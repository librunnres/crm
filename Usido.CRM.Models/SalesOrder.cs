/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SalesOrder
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售主表
    /// </summary>
    [Table("SalesOrder")]
    public class SalesOrder
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 会员id
        /// </summary>
        public int? MemberId { get; set; }

        /// <summary>
        /// 业务员Id
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 订单类型 0默认是业务员打的销售订单 1表示处方转的订单  2表示理疗服务
        /// </summary>
        [Required]
        public int OrderType { get; set; }

        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? ShipTime { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Required]
        public int State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}