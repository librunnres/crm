/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：实物明细账
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：StockProductDetails
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 实物明细账
    /// </summary>
    [Table("StockProductDetails")]
    public class StockProductDetails
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 单据号
        /// </summary>
        public string InvoiceCode { get; set; }

        /// <summary>
        /// 收发类型 采购入库 采购退货出库 销售出库 销售退货入库 库存盘盈入库 库存盘亏出库
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 期初数量
        /// </summary>
        [Required]
        public decimal PrevQuantity { get; set; }

        /// <summary>
        /// 期初单价
        /// </summary>
        [Required]
        public decimal PrevCostPrice { get; set; }

        /// <summary>
        /// 入库数量
        /// </summary>
        public decimal? PutQuantity { get; set; }

        /// <summary>
        /// 入库单价
        /// </summary>
        public decimal? PutCostPrice { get; set; }

        /// <summary>
        /// 出库数量
        /// </summary>
        public decimal? ShipmentsQuantity { get; set; }

        /// <summary>
        /// 出库单价
        /// </summary>
        public decimal? ShipmentsCostPrice { get; set; }

        /// <summary>
        /// 结存数量
        /// </summary>
        [Required]
        public decimal BalancesQuantity { get; set; }

        /// <summary>
        /// 结存单价
        /// </summary>
        [Required]
        public decimal BalancesCostPrice { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        public DateTime InDate { get; set; }

        /// <summary>
        /// 库存金额
        /// </summary>
        [Required]
        public decimal StockPrice { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }
    }
}