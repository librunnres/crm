﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usido.CRM.Models
{
    [Table("MoneyFlow")]
    public class MoneyFlow
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 会员Id
        /// </summary>
        public int MemberId { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public int CreateUserId { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string MemberCard { get; set; }

        /// <summary>
        /// 类别  理疗还是普通会员
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal Moneys { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>

        public DateTime? CreateTime { get; set; }

    }
}
