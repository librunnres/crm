/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：采购明细表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SupplyInfo
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 采购明细表
    /// </summary>
    [Table("SupplyInfo")]
    public class SupplyInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public decimal Quantity { get; set; }

        /// <summary>
        /// 有效期至
        /// </summary>
        [Required]
        public DateTime? DateExpiry { get; set; }

        /// <summary>
        /// 采购主表id
        /// </summary>
        public int? OrderId { get; set; }
    }
}