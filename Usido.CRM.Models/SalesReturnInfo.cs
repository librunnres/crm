/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售退货明细表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SalesReturnInfo
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售退货明细表
    /// </summary>
    [Table("SalesReturnInfo")]
    public class SalesReturnInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 原始销售订单明细id
        /// </summary>
        public int? OriginalSalesInforId { get; set; }

        /// <summary>
        /// 销售退货主表id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 药品到期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }
    }
}