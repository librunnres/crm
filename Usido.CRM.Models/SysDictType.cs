/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：数据字典类型
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysDictType
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 数据字典类型
    /// </summary>
    [Table("SysDictType")]
    public class SysDictType
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 字典类型名称
        /// </summary>
        public string TypeName {get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {get;set;}
    }
}