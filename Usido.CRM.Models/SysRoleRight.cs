/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：角色权限
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysRoleRight
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 角色权限
    /// </summary>
    [Table("SysRoleRight")]
    public class SysRoleRight
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 角色id
        /// </summary>
        [Required]
        public int RoleId {get;set;}

        /// <summary>
        /// 菜单Id
        /// </summary>
        [Required]
        public int MenuId {get;set;}
    }
}