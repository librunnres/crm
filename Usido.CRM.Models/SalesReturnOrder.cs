/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售退货主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SalesReturnOrder
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售退货主表
    /// </summary>
    [Table("SalesReturnOrder")]
    public class SalesReturnOrder
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 业务员Id
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTime? PutTime { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 原始销售订单号
        /// </summary>
        public int? OriginalSalesOrderId { get; set; }
    }
}