/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：角色表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysRole
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 角色表
    /// </summary>
    [Table("SysRole")]
    public class SysRole
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 名称
        /// </summary>
        public string RoleName {get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public bool State {get;set;}
    }
}