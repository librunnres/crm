/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：库存盘点明细表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-11 18:00:21
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：StockInventoryDetail
*└──────────────────────────────────────────────────────────────┘
*/

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-11 18:00:21
    /// 库存盘点明细表
    /// </summary>
    [Table("StockInventoryDetail")]
    public class StockInventoryDetail
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 主表id
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 库存数
        /// </summary>
        public decimal? StockQuantity { get; set; }

        /// <summary>
        /// 实盘数
        /// </summary>
        public decimal? ActualQuantity { get; set; }

        /// <summary>
        /// 盘盈数量
        /// </summary>
        public decimal? InventorySurplusQuantity { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public DateTime? DateExpiry { get; set; }
    }
}