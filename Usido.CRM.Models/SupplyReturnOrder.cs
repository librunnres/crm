/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：采购退货主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SupplyReturnOrder
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 采购退货主表
    /// </summary>
    [Table("SupplyReturnOrder")]
    public class SupplyReturnOrder
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        [Required]
        public int CreateUserId { get; set; }

        /// <summary>
        /// 出库人
        /// </summary>
        public int? PutUserId { get; set; }

        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? PutTime { get; set; }

        /// <summary>
        /// 状态 0未出库 1已出库
        /// </summary>
        [Required]
        public int State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 原始采购订单号
        /// </summary>
        [Required]
        public int SupplyOrderId { get; set; }
    }
}