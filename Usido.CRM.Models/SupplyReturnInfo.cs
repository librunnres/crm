/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：采购退货明细表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SupplyReturnInfo
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 采购退货明细表
    /// </summary>
    [Table("SupplyReturnInfo")]
    public class SupplyReturnInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public decimal Quantity { get; set; }

        /// <summary>
        /// 原始采购订单号
        /// </summary>
        [Required]
        public int OriginalSupplyInforId { get; set; }

        /// <summary>
        /// 采购主表id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 有效期至
        /// </summary>
        [Required]
        public DateTime? DateExpiry { get; set; }
    }
}