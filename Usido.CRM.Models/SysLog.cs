/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：操作日志表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysLog
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 操作日志表
    /// </summary>
    [Table("SysLog")]
    public class SysLog
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId {get;set;}

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime? CreateTime {get;set;}

        /// <summary>
        /// 操作内容
        /// </summary>
        public string OperContent {get;set;}

        /// <summary>
        /// 操作ip
        /// </summary>
        public string OperIp {get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {get;set;}
    }
}