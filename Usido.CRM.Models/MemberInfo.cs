/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：会员信息
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：MemberInfo
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 会员信息
    /// </summary>
    [Table("MemberInfo")]
    public class MemberInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IdNumber { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Required]
        public bool Sex { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? Birthdate { get; set; }

        /// <summary>
        /// 会员卡号
        /// </summary>
        public string MemberCard { get; set; }

        /// <summary>
        /// 理疗卡号
        /// </summary>
        public string LLCard { get; set; }

        
        /// <summary>
        /// 普通会员账户余额
        /// </summary>
        public decimal? AccountMoney { get; set; }

        /// <summary>
        /// 理疗账户余额
        /// </summary>
        public decimal? LLAccountMoney { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
    }
}