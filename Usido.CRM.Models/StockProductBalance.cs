/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：实物结存表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：StockProductBalance
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 实物结存表
    /// </summary>
    [Table("StockProductBalance")]
    public class StockProductBalance
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 结存数量
        /// </summary>
        [Required]
        public decimal StockQuantity { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        [Required]
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Required]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 预留库存数
        /// </summary>
        public decimal? ReservedStock { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }
    }
}