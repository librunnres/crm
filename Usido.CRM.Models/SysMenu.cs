/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：菜单信息
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysMenu
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 菜单信息
    /// </summary>
    [Table("SysMenu")]
    public class SysMenu
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName {get;set;}

        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon {get;set;}

        /// <summary>
        /// 上级菜单Id
        /// </summary>
        [Required]
        public int ParentId {get;set;}

        /// <summary>
        /// 状态 正常禁用
        /// </summary>
        [Required]
        public bool State {get;set;}

        /// <summary>
        /// 排序字段
        /// </summary>
        [Required]
        public int OrderBy {get;set;}
    }
}