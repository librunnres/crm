/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：数据字典信息
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysDict
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 数据字典信息
    /// </summary>
    [Table("SysDict")]
    public class SysDict
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 字典名称
        /// </summary>
        public string DictName {get;set;}

        /// <summary>
        /// 所属类型
        /// </summary>
        [Required]
        public int DictTypeId {get;set;}

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark {get;set;}

        /// <summary>
        /// 排序字段
        /// </summary>
        [Required]
        public int OrderBy {get;set;}

        /// <summary>
        /// 状态 正常 禁用
        /// </summary>
        [Required]
        public bool State {get;set;}
    }
}