/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售明细表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-06 14:31:59
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SalesInfo
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售明细表
    /// </summary>
    [Table("SalesInfo")]
    public class SalesInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 销售主表Id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 药品到期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }
    }
}