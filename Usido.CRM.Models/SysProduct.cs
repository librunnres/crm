/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：商品表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysProduct
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 商品表
    /// </summary>
    [Table("SysProduct")]
    public class SysProduct
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName {get;set;}

        /// <summary>
        /// 商品简码
        /// </summary>
        public string ProductCode {get;set;}

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification {get;set;}

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit {get;set;}

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise {get;set;}

        /// <summary>
        /// 添加时间
        /// </summary>
        [Required]
        public DateTime CreateTime {get;set;}

        /// <summary>
        /// 添加人
        /// </summary>
        [Required]
        public int CreateUserId {get;set;}

        /// <summary>
        /// 最后一次修改时间
        /// </summary>
        public DateTime? LastUpdateTime {get;set;}

        /// <summary>
        /// 修改人
        /// </summary>
        public int? LastUpdateUserId {get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public bool State {get;set;}

        /// <summary>
        /// 批注问号
        /// </summary>

        public string Notation { get; set; }

        /// <summary>
        /// 零售价
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// 会员折扣
        /// </summary>
        public decimal MemberDiscount { get; set; }

        /// <summary>
        /// 条码
        /// </summary>
        public string CardNum { get; set; }
    }
}