/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：供应商
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Usido.CRM.Models
*│　类    名：SysSupplier
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Usido.CRM.Models
{
    /// <summary>
    /// 李宝
    /// 2019-01-05 10:49:57
    /// 供应商
    /// </summary>
    [Table("SysSupplier")]
    public class SysSupplier
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id {get;set;}

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CompanyName {get;set;}

        /// <summary>
        /// 企业简码
        /// </summary>
        public string CompanyCode {get;set;}

        /// <summary>
        /// 企业法人
        /// </summary>
        public string LegalPerson {get;set;}

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts {get;set;}

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone {get;set;}

        /// <summary>
        /// 企业地址
        /// </summary>
        public string CompanyAddress {get;set;}

        /// <summary>
        /// 添加时间
        /// </summary>
        [Required]
        public DateTime CreateTime {get;set;}

        /// <summary>
        /// 添加人
        /// </summary>
        [Required]
        public int CreateUserId {get;set;}

        /// <summary>
        /// 最后一次修改时间
        /// </summary>
        public DateTime? LastUpdateTime {get;set;}

        /// <summary>
        /// 修改人
        /// </summary>
        public int? LastUpdateUserId {get;set;}

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public bool State {get;set;}
    }
}