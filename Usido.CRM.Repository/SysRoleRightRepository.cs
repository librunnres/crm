/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：角色权限接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysRoleRightRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;
using Dapper;

namespace Usido.CRM.Repository
{
    public class SysRoleRightRepository:BaseRepository<SysRoleRight,int>, ISysRoleRightRepository
    {
        public SysRoleRightRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        /// <summary>
        /// 删除角色权限
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public int DelRole(int RoleId)
        {
            string sql = @"delete from [dbo].[SysRoleRight] where [RoleId]=@RoleId";

            return _dbConnection.Execute(sql, new {RoleId = RoleId });
        }
    }
}