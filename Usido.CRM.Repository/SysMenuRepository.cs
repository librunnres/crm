/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：菜单信息接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysMenuRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Usido.CRM.Repository
{
    public class SysMenuRepository : BaseRepository<SysMenu, int>, ISysMenuRepository
    {
        private readonly ISysRoleRightRepository roleRightRepository = new SysRoleRightRepository();

        public SysMenuRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public IEnumerable<SysMenu> GetMenuListByRoleId(int roleId)
        {
            var roleRightList = roleRightRepository.GetList(new { RoleId = roleId });
            var ids = new List<int>();
            ids.Add(0);
            foreach (var item in roleRightList)
            {
                ids.Add(item.MenuId);
            }
            string idString = string.Join(",", ids);
            var list = _dbConnection.Query<SysMenu>($"SELECT * FROM dbo.SysMenu WHERE Id IN ({idString})");

            return list;
        }
    }
}