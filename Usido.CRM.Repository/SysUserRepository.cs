using Dapper;
using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：用户表接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysUserRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;

namespace Usido.CRM.Repository
{
    public class SysUserRepository : BaseRepository<SysUser, int>, ISysUserRepository
    {
        public SysUserRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public IEnumerable<SysUserDto> GetUserList(string where)
        {
            string sql = @"select a.*, (case when a.State = '1' then '可用' else '停用' end)State,c.[RoleName],b.[DeptName],d.[dictname] from [dbo].[SysUser] as a
inner join [dbo].[SysDepartment] as b on a.[DeptId] = b.Id
inner join [dbo].[SysRole] as c on a.[RoleId] = c.Id
inner join[dbo].[SysDict] as d on a.[EqucationId] = d.Id ";

            if (!where.IsNullOrEmpty())
            {
                sql += $" WHERE {where}";
            }
            var list = _dbConnection.Query<SysUserDto>(sql);

            return list;
        }

        public bool UpdatePassword(int userId, string password)
        {
            string sql = @"UPDATE dbo.SysUser SET Password = @Password WHERE Id = @Id";

            return _dbConnection.Execute(sql, new { Password = password, Id = userId }) > 0;
        }
    }
}