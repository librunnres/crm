using Newtonsoft.Json;
using System;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：操作日志表接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysLogRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.Core.ToolHelper;
using Usido.CRM.IRepository;
using Usido.CRM.Models;

namespace Usido.CRM.Repository
{
    public class SysLogRepository : BaseRepository<SysLog, int>, ISysLogRepository
    {
        public SysLogRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public void AddLog(int userId, string content,object model=null, string remark = "")
        {
            remark=remark + JsonConvert.SerializeObject(model);
            var log = new SysLog()
            {
                CreateTime = DateTime.Now,
                OperContent = content,
                Remark = remark,
                UserId = userId,
                OperIp = Tools.GetIp()
            };

            Insert(log);
        }
    }
}