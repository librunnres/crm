/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：库存盘点明细表接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:56
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： StockInventoryDetailRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;

namespace Usido.CRM.Repository
{
    public class StockInventoryDetailRepository:BaseRepository<StockInventoryDetail,int>, IStockInventoryDetailRepository
    {
        public StockInventoryDetailRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }
    }
}