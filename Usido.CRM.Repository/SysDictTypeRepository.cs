/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：数据字典类型接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysDictTypeRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;

namespace Usido.CRM.Repository
{
    public class SysDictTypeRepository:BaseRepository<SysDictType,int>, ISysDictTypeRepository
    {
        public SysDictTypeRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }
    }
}