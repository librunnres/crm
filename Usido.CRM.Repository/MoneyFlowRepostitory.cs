﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;

namespace Usido.CRM.Repository
{
    public class MoneyFlowRepostitory: BaseRepository<MoneyFlow, int>, IMoneyFlowRepository
    {
        public MoneyFlowRepostitory()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public double GetMoneyFlowList(string where)
        {
            string sql = @"SELECT sum([Moneys]) FROM   [dbo].[MoneyFlow]";

            if (!where.IsNullOrEmpty())
            {
                sql += $"{where}";
            }
            var list = _dbConnection.ExecuteScalar<double>(sql);

            return list;
        }
    }
   
}
