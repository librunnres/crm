/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售明细表接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SalesInfoRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;

namespace Usido.CRM.Repository
{
    public class SalesInfoRepository:BaseRepository<SalesInfo,int>, ISalesInfoRepository
    {
        public SalesInfoRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }
    }
}