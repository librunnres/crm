/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：实物明细账接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： StockProductDetailsRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;
using Dapper;
using Usido.CRM.Core.Extensions;

namespace Usido.CRM.Repository
{
    public class StockProductDetailsRepository:BaseRepository<StockProductDetails,int>, IStockProductDetailsRepository
    {
        public StockProductDetailsRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public IEnumerable<StockProductDetailsDto> GetAllList(string where)
        {
            string sql = @"
SELECT * FROM (
	SELECT A.*,B.ProductName,B.ProductCode,B.Specification,B.MarketingUnit,B.ManufacturingEnterprise FROM dbo.StockProductDetails AS A
	INNER JOIN dbo.SysProduct AS B ON A.ProductId = B.Id
)T ";
            if (!where.IsNullOrEmpty())
            {
                sql += where;
            }

            return _dbConnection.Query<StockProductDetailsDto>(sql);
        }
    }
}