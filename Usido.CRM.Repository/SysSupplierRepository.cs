/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：供应商接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： SysSupplierRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;
using Usido.CRM.Dto;
using Usido.CRM.Core.Extensions;
using Dapper;

namespace Usido.CRM.Repository
{
    public class SysSupplierRepository:BaseRepository<SysSupplier,int>, ISysSupplierRepository
    {
        public SysSupplierRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public IEnumerable<SysSupplierDto> GetSupplierList(string where)
        {
            string sql = @"select a.*,b.UserName from [dbo].SysSupplier as a
inner join [dbo].SysUser as b on a.CreateUserId = b.Id";

            if (!where.IsNullOrEmpty())
            {
                sql += $" WHERE {where}";
            }
            var list = _dbConnection.Query<SysSupplierDto>(sql);

            return list;
        }
    }
}