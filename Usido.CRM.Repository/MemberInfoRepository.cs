/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：会员信息接口实现
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:56
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.Repository
*│　类    名： MemberInfoRepository
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Options;
using Usido.CRM.Core.Repository;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;
using Usido.CRM.Core.Extensions;
using Dapper;
using Usido.CRM.Dto;

namespace Usido.CRM.Repository
{
    public class MemberInfoRepository : BaseRepository<MemberInfo, int>, IMemberInfoRepository
    {
        public MemberInfoRepository()
        {
            _dbConnection = ConnectionFactory.CreateConnection(DbOptionData.DbType, DbOptionData.ConnectionString);
        }

        public IEnumerable<MemberInfoDto> GetMemberInfoList(string where)
        {
            string sql = @"SELECT Id, UserName, Phone, IdNumber,  (case when Sex = '0' then '女' else '男' end) Sex, Birthdate, MemberCard, AccountMoney,LLCard,LLAccountMoney, CreateTime FROM dbo.MemberInfo ";

            if (!where.IsNullOrEmpty())
            {
                sql += $" WHERE {where}";
            }
            var list = _dbConnection.Query<MemberInfoDto>(sql);

            return list;
        }

        public decimal GetMoney(string where)
        {
            string sql = @"SELECT ISNULL(-SUM(Moneys),0) FROM dbo.MoneyFlow WHERE Type NOT LIKE '%充值%' ";
            if (!where.IsNullOrEmpty())
            {
                sql += where;
            }

            return _dbConnection.ExecuteScalar<decimal>(sql);
        }

        public int UpdateAccountMon(string id, string type, decimal money)
        {
            string sql = "";
            if (type == "0")
            {
                //普通会员充值
                sql = @" Update dbo.MemberInfo set AccountMoney+=" + money + " where Id=" + id + " ";
            }
            else
            {
                //理疗充值
                sql = @" Update dbo.MemberInfo set LLAccountMoney+=" + money + " where Id=" + id + " ";
            }
            return _dbConnection.Execute(sql);
        }
    }
}