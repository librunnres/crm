﻿using System;
using System.Collections;
using System.Reflection;
using System.Windows.Forms;
using Usido.CRM.App.Forms;
using Usido.CRM.Models;

namespace Usido.CRM.App
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            bool createNew;
            using (System.Threading.Mutex mutex = new System.Threading.Mutex(true, Application.ProductName, out createNew))
            {
                if (createNew)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Frm_Logincs login = new Frm_Logincs();
                    login.ShowDialog();
                    if (login.DialogResult.Equals(DialogResult.OK))
                    {
                        login.Close();
                        string[] info = login.Tag.ToString().Split('|'); 
                        Application.Run(new Form1(info[0], info[1]));
                    } 
                    
                }
                else
                {
                    MessageBox.Show("应用程序已经在运行中...");
                    System.Threading.Thread.Sleep(1000);
                    System.Environment.Exit(1);
                }
            }
        }
    }
}