﻿using System.Windows.Forms;
using Usido.CRM.Core.CodeGenerator;
using Usido.CRM.Core.Models;
using Usido.CRM.Core.Options;

namespace Usido.CRM.App
{
    public partial class FrmCodeGenerate : Form
    {
        public FrmCodeGenerate()
        {
            InitializeComponent();

            var option = new CodeGenerateOption()
            {
                ConnectionString = "Data Source=123.207.60.155;Initial Catalog=UsidoCRM;User ID=sa;Password=radial_soft1;Persist Security Info=True;Max Pool Size=50;Min Pool Size=0;Connection Lifetime=300;",
                DbType = DatabaseType.SqlServer.ToString(),
                Author = "李宝",
                OutputPath = @"E:\Code",
                ModelsNamespace = "Usido.CRM.Models",
                IRepositoryNamespace = "Usido.CRM.IRepository",
                RepositoryNamespace = "Usido.CRM.Repository",
                ProjectNamespace = "Usido.CRM",
                ProjectName = "Usido.CRM",
            };

            CodeGenerator codeGenerator = new CodeGenerator(option);

            codeGenerator.GenerateTemplateCodesFromDatabase();

            MessageBox.Show("代码生成成功，路径是:" + option.OutputPath);
        }

        private void FrmCodeGenerate_Load(object sender, System.EventArgs e)
        {

        }
    }
}