﻿using log4net;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Usido.CRM.App.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.ToolHelper;
using Usido.CRM.IRepository;
using Usido.CRM.Repository;

namespace Usido.CRM.App
{
    public partial class Form1 : Form
    {
        private string UserId;
        private string UserName;
        public static readonly ILog log = LogManager.GetLogger(nameof(Form1));
        private readonly ISysUserRepository userReposioty = new SysUserRepository();
        private readonly ISysMenuRepository MenuS = new SysMenuRepository();
        public Form1()
        {
            InitializeComponent();
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        public Form1(string v1, string v2)
        {
            InitializeComponent();
            this.UserId = v1;
            this.UserName = v2;
        }

        public void CloseOtherChildrenForm()
        {
            for (int i = 0; i < this.MdiChildren.Length; i++)
            {
                this.MdiChildren[i].Close();
            }
        }

        private void 设置员工ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            YG_SetFrom dm = new YG_SetFrom(UserId);
            dm.FormBorderStyle = FormBorderStyle.Fixed3D;
            dm.WindowState = FormWindowState.Maximized;
            dm.MdiParent = this;
            dm.Show();
        }

        private void 设置药品ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品设置界面 YP = new 药品设置界面(UserId);
            YP.WindowState = FormWindowState.Maximized;
            YP.MdiParent = this;
            YP.Show();
        }

        private void 设置药品价格ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品价格设置界面 YPJG = new 药品价格设置界面(UserId);
            YPJG.WindowState = FormWindowState.Maximized;
            YPJG.MdiParent = this;
            YPJG.Show();
        }

        private void 药品查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品查询界面 YPCX = new 药品查询界面(UserId);
            YPCX.WindowState = FormWindowState.Maximized;
            YPCX.MdiParent = this;
            YPCX.Show();
        }

        private void 设置客户或供应商ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            客户设置界面 KHSZ = new 客户设置界面(UserId);
            KHSZ.WindowState = FormWindowState.Maximized;
            KHSZ.MdiParent = this;
            KHSZ.Show();
        }

        private void 客户查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            客户查询界面 KHCX = new 客户查询界面();
            KHCX.WindowState = FormWindowState.Maximized;
            KHCX.MdiParent = this;
            KHCX.Show();
        }

        private void 采购入库ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            采购入库管理 CGRK = new 采购入库管理(UserId, UserName);
            //CGRK.WindowState = FormWindowState.Maximized;
            CGRK.MdiParent = this;
            CGRK.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var user = userReposioty.Get(UserId.ToInt());
            Lab_Username.Text = $"当前用户：{UserName}";
            lab_date.Text = $"当前时间：{DateTime.Now.ToString("yyyy年MM月dd日  ddd")}";

            //int Role_Id = user.RoleId;
            //var MenuInfo = MenuS.GetMenuListByRoleId(Role_Id);
            //foreach (ToolStripItem item in toolStrip1.Items)
            //{
            //    if (item is ToolStripDropDownButton)
            //    {
            //        foreach (ToolStripMenuItem menu in ((ToolStripDropDownButton)item).DropDownItems)
            //        {
            //            if (MenuInfo.ToList().Any(m => m.MenuName == menu.Text))
            //            {
            //                menu.Visible = true;
            //            }
            //            else
            //            {
            //                menu.Visible = false;
            //            }
            //        }

            //        if (MenuInfo.ToList().Any(m => m.MenuName == item.Text))
            //        {
            //            item.Visible = true;
            //        }
            //        else
            //        {
            //            item.Visible = false;
            //        }
            //    }
            //}
        }

        private void 缺货品种查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            缺货品种查询 QHPZ = new 缺货品种查询();
            QHPZ.WindowState = FormWindowState.Maximized;
            QHPZ.MdiParent = this;
            QHPZ.Show();
        }

        private void 设置药品预留数ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品预留数界面 ypyy = new 药品预留数界面();
            ypyy.WindowState = FormWindowState.Maximized;
            ypyy.MdiParent = this;
            ypyy.Show();
        }

        private void 采购退货ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            采购退货管理 cgth = new 采购退货管理(UserId, UserName);
            //cgth.WindowState= FormWindowState.Maximized;
            cgth.MdiParent = this;
            cgth.Show();
        }

        private void 药品采购综合查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            采购统计 cgth = new 采购统计(UserId, UserName);
            cgth.WindowState = FormWindowState.Maximized;
            cgth.MdiParent = this;
            cgth.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            FrmAbout About = new FrmAbout();
            //About.WindowState = FormWindowState.Maximized;
            About.MdiParent = this;
            About.Show();
        }

        private void 设置用户权限ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            //权限控制 qx = new 权限控制();
            //qx.WindowState = FormWindowState.Maximized;
            //qx.MdiParent = this;
            //qx.Show();
            MessageBox.Show("此功能暂未启用");
        }

        private void 注销登陆ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart(); //重启当前程序
        }

        private void 更改个人密码ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            UpdateUserPassWord Upw = new UpdateUserPassWord(UserId);
            //Upw.WindowState = FormWindowState.Maximized;
            Upw.MdiParent = this;
            Upw.Show();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Frm_Logincs Frm = new Frm_Logincs();
            //Frm.Visible = true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 会员设置ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            会员设置界面 HY = new 会员设置界面(UserId);
            HY.WindowState = FormWindowState.Maximized;
            HY.MdiParent = this;
            HY.Show();
        }

        private void 药品销售制单ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品销售管理 ypxs = new 药品销售管理(UserId, UserName);
            ypxs.MdiParent = this;
            ypxs.WindowState = FormWindowState.Maximized;
            ypxs.Show();
        }

        private void 销售退货制单ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            药品退货管理 ypth = new 药品退货管理(UserId, UserName);
            ypth.WindowState = FormWindowState.Maximized;
            ypth.MdiParent = this;
            ypth.Show();
        }

        private void 库存查询ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            库存查询 kccx = new 库存查询(UserId, UserName);
            kccx.WindowState = FormWindowState.Maximized;
            kccx.MdiParent = this;
            kccx.Show();
        }

        private void 电子处方ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            电子处方 dzcf = new 电子处方(UserId, UserName);
            //dzcf.WindowState = FormWindowState.Maximized;
            dzcf.MdiParent = this;
            dzcf.Show();

        }

        private void 财务报表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            财务报表 caiwu = new 财务报表();
            caiwu.WindowState = FormWindowState.Maximized;
            caiwu.MdiParent = this;
            caiwu.Show();
        }

        private void 药品销售综合查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            销售统计 xstj = new 销售统计(UserId, UserName);
            xstj.MdiParent = this;
            xstj.WindowState = FormWindowState.Maximized;
            xstj.Show();
        }

        private void 理疗服务销售管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            理疗服务销售管理 llfwxsgl = new 理疗服务销售管理(UserId, UserName);
            llfwxsgl.MdiParent = this;
            llfwxsgl.WindowState = FormWindowState.Maximized;
            llfwxsgl.Show();
        }

        private void 盘点制单ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            库存盘点管理 kucunpandian = new 库存盘点管理(UserId, UserName);
            kucunpandian.MdiParent = this;
            kucunpandian.WindowState = FormWindowState.Maximized;
            kucunpandian.Show();
        }

        private void 审核处方ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseOtherChildrenForm();
            实物明细账 kucunpandian = new 实物明细账(UserId, UserName);
            kucunpandian.MdiParent = this;
            kucunpandian.WindowState = FormWindowState.Maximized;
            kucunpandian.Show();
        }

        //private bool ShowChildrenForm(string p_ChildrenFormText)
        //{
        //    int i;
        //    for (i = 0; i < this.MdiChildren.Length; i++)
        //    {
        //        if (this.MdiChildren[i].Text == p_ChildrenFormText)
        //        {
        //            this.MdiChildren[i].Activate();
        //            return true;
        //        }
        //    }
        //    return false;
        //}
    }
}