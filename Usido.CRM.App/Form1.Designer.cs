﻿namespace Usido.CRM.App
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.设置员工ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.设置用户权限ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.注销登陆ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.更改个人密码ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton8 = new System.Windows.Forms.ToolStripDropDownButton();
            this.设置药品ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.设置药品价格ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.药品查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton7 = new System.Windows.Forms.ToolStripDropDownButton();
            this.设置客户或供应商ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.客户查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton6 = new System.Windows.Forms.ToolStripDropDownButton();
            this.采购入库ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.缺货品种查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.采购退货ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.设置药品预留数ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.药品采购综合查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton5 = new System.Windows.Forms.ToolStripDropDownButton();
            this.药品销售制单ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.销售退货制单ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.药品销售综合查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.电子处方ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.审核处方ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.理疗服务销售管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.库存查询ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.盘点制单ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.会员设置ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.财务报表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.Lab_Username = new System.Windows.Forms.ToolStripStatusLabel();
            this.lab_date = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStrip1.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator2.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator3.Visible = false;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator4.Visible = false;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 60);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator6.Visible = false;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 60);
            this.toolStripSeparator7.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AllowItemReorder = true;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.toolStrip1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSeparator5,
            this.toolStripDropDownButton8,
            this.toolStripSeparator4,
            this.toolStripDropDownButton7,
            this.toolStripSeparator2,
            this.toolStripDropDownButton6,
            this.toolStripSeparator6,
            this.toolStripDropDownButton5,
            this.toolStripSeparator1,
            this.toolStripDropDownButton4,
            this.toolStripDropDownButton3,
            this.toolStripSeparator3,
            this.toolStripSeparator7,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1348, 60);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.TabStop = true;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置员工ToolStripMenuItem1,
            this.设置用户权限ToolStripMenuItem1,
            this.注销登陆ToolStripMenuItem1,
            this.更改个人密码ToolStripMenuItem1,
            this.toolStripMenuItem1});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton1.Text = "设置";
            this.toolStripDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 设置员工ToolStripMenuItem1
            // 
            this.设置员工ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置员工ToolStripMenuItem1.Image")));
            this.设置员工ToolStripMenuItem1.Name = "设置员工ToolStripMenuItem1";
            this.设置员工ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.设置员工ToolStripMenuItem1.Text = "设置员工";
            this.设置员工ToolStripMenuItem1.Click += new System.EventHandler(this.设置员工ToolStripMenuItem1_Click);
            // 
            // 设置用户权限ToolStripMenuItem1
            // 
            this.设置用户权限ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置用户权限ToolStripMenuItem1.Image")));
            this.设置用户权限ToolStripMenuItem1.Name = "设置用户权限ToolStripMenuItem1";
            this.设置用户权限ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.设置用户权限ToolStripMenuItem1.Text = "设置用户权限";
            this.设置用户权限ToolStripMenuItem1.Click += new System.EventHandler(this.设置用户权限ToolStripMenuItem1_Click);
            // 
            // 注销登陆ToolStripMenuItem1
            // 
            this.注销登陆ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("注销登陆ToolStripMenuItem1.Image")));
            this.注销登陆ToolStripMenuItem1.Name = "注销登陆ToolStripMenuItem1";
            this.注销登陆ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.注销登陆ToolStripMenuItem1.Text = "注销/登陆";
            this.注销登陆ToolStripMenuItem1.Click += new System.EventHandler(this.注销登陆ToolStripMenuItem1_Click);
            // 
            // 更改个人密码ToolStripMenuItem1
            // 
            this.更改个人密码ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("更改个人密码ToolStripMenuItem1.Image")));
            this.更改个人密码ToolStripMenuItem1.Name = "更改个人密码ToolStripMenuItem1";
            this.更改个人密码ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.更改个人密码ToolStripMenuItem1.Text = "更改个人密码";
            this.更改个人密码ToolStripMenuItem1.Click += new System.EventHandler(this.更改个人密码ToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.toolStripMenuItem1.Text = "关于软件";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton8
            // 
            this.toolStripDropDownButton8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置药品ToolStripMenuItem1,
            this.设置药品价格ToolStripMenuItem1,
            this.药品查询ToolStripMenuItem1});
            this.toolStripDropDownButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton8.Image")));
            this.toolStripDropDownButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton8.Name = "toolStripDropDownButton8";
            this.toolStripDropDownButton8.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton8.Text = "药品";
            this.toolStripDropDownButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 设置药品ToolStripMenuItem1
            // 
            this.设置药品ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置药品ToolStripMenuItem1.Image")));
            this.设置药品ToolStripMenuItem1.Name = "设置药品ToolStripMenuItem1";
            this.设置药品ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.设置药品ToolStripMenuItem1.Text = "设置药品";
            this.设置药品ToolStripMenuItem1.Click += new System.EventHandler(this.设置药品ToolStripMenuItem1_Click);
            // 
            // 设置药品价格ToolStripMenuItem1
            // 
            this.设置药品价格ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置药品价格ToolStripMenuItem1.Image")));
            this.设置药品价格ToolStripMenuItem1.Name = "设置药品价格ToolStripMenuItem1";
            this.设置药品价格ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.设置药品价格ToolStripMenuItem1.Text = "设置药品价格";
            this.设置药品价格ToolStripMenuItem1.Click += new System.EventHandler(this.设置药品价格ToolStripMenuItem1_Click);
            // 
            // 药品查询ToolStripMenuItem1
            // 
            this.药品查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("药品查询ToolStripMenuItem1.Image")));
            this.药品查询ToolStripMenuItem1.Name = "药品查询ToolStripMenuItem1";
            this.药品查询ToolStripMenuItem1.Size = new System.Drawing.Size(192, 38);
            this.药品查询ToolStripMenuItem1.Text = "药品查询";
            this.药品查询ToolStripMenuItem1.Click += new System.EventHandler(this.药品查询ToolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton7
            // 
            this.toolStripDropDownButton7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.设置客户或供应商ToolStripMenuItem1,
            this.客户查询ToolStripMenuItem1});
            this.toolStripDropDownButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton7.Image")));
            this.toolStripDropDownButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton7.Name = "toolStripDropDownButton7";
            this.toolStripDropDownButton7.Size = new System.Drawing.Size(71, 57);
            this.toolStripDropDownButton7.Text = "供应商";
            this.toolStripDropDownButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 设置客户或供应商ToolStripMenuItem1
            // 
            this.设置客户或供应商ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置客户或供应商ToolStripMenuItem1.Image")));
            this.设置客户或供应商ToolStripMenuItem1.Name = "设置客户或供应商ToolStripMenuItem1";
            this.设置客户或供应商ToolStripMenuItem1.Size = new System.Drawing.Size(176, 38);
            this.设置客户或供应商ToolStripMenuItem1.Text = "供应商";
            this.设置客户或供应商ToolStripMenuItem1.Click += new System.EventHandler(this.设置客户或供应商ToolStripMenuItem1_Click);
            // 
            // 客户查询ToolStripMenuItem1
            // 
            this.客户查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("客户查询ToolStripMenuItem1.Image")));
            this.客户查询ToolStripMenuItem1.Name = "客户查询ToolStripMenuItem1";
            this.客户查询ToolStripMenuItem1.Size = new System.Drawing.Size(176, 38);
            this.客户查询ToolStripMenuItem1.Text = "供应商查询";
            this.客户查询ToolStripMenuItem1.Click += new System.EventHandler(this.客户查询ToolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton6
            // 
            this.toolStripDropDownButton6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.采购入库ToolStripMenuItem1,
            this.缺货品种查询ToolStripMenuItem1,
            this.采购退货ToolStripMenuItem1,
            this.设置药品预留数ToolStripMenuItem1,
            this.药品采购综合查询ToolStripMenuItem1});
            this.toolStripDropDownButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton6.Image")));
            this.toolStripDropDownButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton6.Name = "toolStripDropDownButton6";
            this.toolStripDropDownButton6.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton6.Text = "采购";
            this.toolStripDropDownButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 采购入库ToolStripMenuItem1
            // 
            this.采购入库ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("采购入库ToolStripMenuItem1.Image")));
            this.采购入库ToolStripMenuItem1.Name = "采购入库ToolStripMenuItem1";
            this.采购入库ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.采购入库ToolStripMenuItem1.Text = "采购入库";
            this.采购入库ToolStripMenuItem1.Click += new System.EventHandler(this.采购入库ToolStripMenuItem1_Click);
            // 
            // 缺货品种查询ToolStripMenuItem1
            // 
            this.缺货品种查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("缺货品种查询ToolStripMenuItem1.Image")));
            this.缺货品种查询ToolStripMenuItem1.Name = "缺货品种查询ToolStripMenuItem1";
            this.缺货品种查询ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.缺货品种查询ToolStripMenuItem1.Text = "缺货品种查询";
            this.缺货品种查询ToolStripMenuItem1.Visible = false;
            this.缺货品种查询ToolStripMenuItem1.Click += new System.EventHandler(this.缺货品种查询ToolStripMenuItem1_Click);
            // 
            // 采购退货ToolStripMenuItem1
            // 
            this.采购退货ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("采购退货ToolStripMenuItem1.Image")));
            this.采购退货ToolStripMenuItem1.Name = "采购退货ToolStripMenuItem1";
            this.采购退货ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.采购退货ToolStripMenuItem1.Text = "采购退货";
            this.采购退货ToolStripMenuItem1.Click += new System.EventHandler(this.采购退货ToolStripMenuItem1_Click);
            // 
            // 设置药品预留数ToolStripMenuItem1
            // 
            this.设置药品预留数ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("设置药品预留数ToolStripMenuItem1.Image")));
            this.设置药品预留数ToolStripMenuItem1.Name = "设置药品预留数ToolStripMenuItem1";
            this.设置药品预留数ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.设置药品预留数ToolStripMenuItem1.Text = "设置药品预留数";
            this.设置药品预留数ToolStripMenuItem1.Click += new System.EventHandler(this.设置药品预留数ToolStripMenuItem1_Click);
            // 
            // 药品采购综合查询ToolStripMenuItem1
            // 
            this.药品采购综合查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("药品采购综合查询ToolStripMenuItem1.Image")));
            this.药品采购综合查询ToolStripMenuItem1.Name = "药品采购综合查询ToolStripMenuItem1";
            this.药品采购综合查询ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.药品采购综合查询ToolStripMenuItem1.Text = "药品采购综合查询";
            this.药品采购综合查询ToolStripMenuItem1.Click += new System.EventHandler(this.药品采购综合查询ToolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton5
            // 
            this.toolStripDropDownButton5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.药品销售制单ToolStripMenuItem1,
            this.销售退货制单ToolStripMenuItem1,
            this.药品销售综合查询ToolStripMenuItem1,
            this.电子处方ToolStripMenuItem1,
            this.审核处方ToolStripMenuItem1,
            this.理疗服务销售管理ToolStripMenuItem});
            this.toolStripDropDownButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton5.Image")));
            this.toolStripDropDownButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton5.Name = "toolStripDropDownButton5";
            this.toolStripDropDownButton5.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton5.Text = "销售";
            this.toolStripDropDownButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 药品销售制单ToolStripMenuItem1
            // 
            this.药品销售制单ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("药品销售制单ToolStripMenuItem1.Image")));
            this.药品销售制单ToolStripMenuItem1.Name = "药品销售制单ToolStripMenuItem1";
            this.药品销售制单ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.药品销售制单ToolStripMenuItem1.Text = "药品销售制单";
            this.药品销售制单ToolStripMenuItem1.Click += new System.EventHandler(this.药品销售制单ToolStripMenuItem1_Click);
            // 
            // 销售退货制单ToolStripMenuItem1
            // 
            this.销售退货制单ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("销售退货制单ToolStripMenuItem1.Image")));
            this.销售退货制单ToolStripMenuItem1.Name = "销售退货制单ToolStripMenuItem1";
            this.销售退货制单ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.销售退货制单ToolStripMenuItem1.Text = "销售退货制单";
            this.销售退货制单ToolStripMenuItem1.Click += new System.EventHandler(this.销售退货制单ToolStripMenuItem1_Click);
            // 
            // 药品销售综合查询ToolStripMenuItem1
            // 
            this.药品销售综合查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("药品销售综合查询ToolStripMenuItem1.Image")));
            this.药品销售综合查询ToolStripMenuItem1.Name = "药品销售综合查询ToolStripMenuItem1";
            this.药品销售综合查询ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.药品销售综合查询ToolStripMenuItem1.Text = "药品销售综合查询";
            this.药品销售综合查询ToolStripMenuItem1.Click += new System.EventHandler(this.药品销售综合查询ToolStripMenuItem_Click);
            // 
            // 电子处方ToolStripMenuItem1
            // 
            this.电子处方ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("电子处方ToolStripMenuItem1.Image")));
            this.电子处方ToolStripMenuItem1.Name = "电子处方ToolStripMenuItem1";
            this.电子处方ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.电子处方ToolStripMenuItem1.Text = "电子处方";
            this.电子处方ToolStripMenuItem1.Click += new System.EventHandler(this.电子处方ToolStripMenuItem1_Click);
            // 
            // 审核处方ToolStripMenuItem1
            // 
            this.审核处方ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("审核处方ToolStripMenuItem1.Image")));
            this.审核处方ToolStripMenuItem1.Name = "审核处方ToolStripMenuItem1";
            this.审核处方ToolStripMenuItem1.Size = new System.Drawing.Size(224, 38);
            this.审核处方ToolStripMenuItem1.Text = "审核处方";
            this.审核处方ToolStripMenuItem1.Click += new System.EventHandler(this.审核处方ToolStripMenuItem1_Click);
            // 
            // 理疗服务销售管理ToolStripMenuItem
            // 
            this.理疗服务销售管理ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("理疗服务销售管理ToolStripMenuItem.Image")));
            this.理疗服务销售管理ToolStripMenuItem.Name = "理疗服务销售管理ToolStripMenuItem";
            this.理疗服务销售管理ToolStripMenuItem.Size = new System.Drawing.Size(224, 38);
            this.理疗服务销售管理ToolStripMenuItem.Text = "理疗服务销售管理";
            this.理疗服务销售管理ToolStripMenuItem.Click += new System.EventHandler(this.理疗服务销售管理ToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.库存查询ToolStripMenuItem1,
            this.盘点制单ToolStripMenuItem1});
            this.toolStripDropDownButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton4.Image")));
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton4.Text = "仓库";
            this.toolStripDropDownButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 库存查询ToolStripMenuItem1
            // 
            this.库存查询ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("库存查询ToolStripMenuItem1.Image")));
            this.库存查询ToolStripMenuItem1.Name = "库存查询ToolStripMenuItem1";
            this.库存查询ToolStripMenuItem1.Size = new System.Drawing.Size(160, 38);
            this.库存查询ToolStripMenuItem1.Text = "库存查询";
            this.库存查询ToolStripMenuItem1.Click += new System.EventHandler(this.库存查询ToolStripMenuItem1_Click);
            // 
            // 盘点制单ToolStripMenuItem1
            // 
            this.盘点制单ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("盘点制单ToolStripMenuItem1.Image")));
            this.盘点制单ToolStripMenuItem1.Name = "盘点制单ToolStripMenuItem1";
            this.盘点制单ToolStripMenuItem1.Size = new System.Drawing.Size(160, 38);
            this.盘点制单ToolStripMenuItem1.Text = "盘点制单";
            this.盘点制单ToolStripMenuItem1.Click += new System.EventHandler(this.盘点制单ToolStripMenuItem1_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.会员设置ToolStripMenuItem1,
            this.财务报表ToolStripMenuItem});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(55, 57);
            this.toolStripDropDownButton3.Text = "财务";
            this.toolStripDropDownButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // 会员设置ToolStripMenuItem1
            // 
            this.会员设置ToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("会员设置ToolStripMenuItem1.Image")));
            this.会员设置ToolStripMenuItem1.Name = "会员设置ToolStripMenuItem1";
            this.会员设置ToolStripMenuItem1.Size = new System.Drawing.Size(160, 38);
            this.会员设置ToolStripMenuItem1.Text = "会员设置";
            this.会员设置ToolStripMenuItem1.Click += new System.EventHandler(this.会员设置ToolStripMenuItem1_Click);
            // 
            // 财务报表ToolStripMenuItem
            // 
            this.财务报表ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("财务报表ToolStripMenuItem.Image")));
            this.财务报表ToolStripMenuItem.Name = "财务报表ToolStripMenuItem";
            this.财务报表ToolStripMenuItem.Size = new System.Drawing.Size(160, 38);
            this.财务报表ToolStripMenuItem.Text = "财务报表";
            this.财务报表ToolStripMenuItem.Click += new System.EventHandler(this.财务报表ToolStripMenuItem_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(46, 57);
            this.toolStripButton1.Text = "退出";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(152, 20);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(79, 20);
            this.toolStripStatusLabel2.Text = "当前时间：";
            // 
            // statusStrip2
            // 
            this.statusStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip2.Location = new System.Drawing.Point(0, 676);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1348, 25);
            this.statusStrip2.TabIndex = 22;
            this.statusStrip2.Text = "statusStrip2";
            this.statusStrip2.Visible = false;
            // 
            // Lab_Username
            // 
            this.Lab_Username.Name = "Lab_Username";
            this.Lab_Username.Size = new System.Drawing.Size(152, 20);
            this.Lab_Username.Text = "toolStripStatusLabel1";
            // 
            // lab_date
            // 
            this.lab_date.Name = "lab_date";
            this.lab_date.Size = new System.Drawing.Size(79, 20);
            this.lab_date.Text = "当前时间：";
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.statusStrip1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Lab_Username,
            this.lab_date});
            this.statusStrip1.Location = new System.Drawing.Point(0, 701);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1348, 25);
            this.statusStrip1.TabIndex = 21;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScrollMinSize = new System.Drawing.Size(1300, 726);
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1348, 711);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MaximumSize = new System.Drawing.Size(1364, 750);
            this.MinimumSize = new System.Drawing.Size(1364, 726);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        public System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton8;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton7;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton6;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton5;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem 设置员工ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置用户权限ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 注销登陆ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 更改个人密码ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置药品ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置药品价格ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 药品查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置客户或供应商ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 客户查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 采购入库ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 缺货品种查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 采购退货ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 设置药品预留数ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 药品采购综合查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 药品销售制单ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 销售退货制单ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 药品销售综合查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 电子处方ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 审核处方ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 库存查询ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 盘点制单ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 会员设置ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 财务报表ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 理疗服务销售管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel Lab_Username;
        private System.Windows.Forms.ToolStripStatusLabel lab_date;
        private System.Windows.Forms.StatusStrip statusStrip1;
    }
}

