﻿namespace Usido.CRM.App.Forms
{
    partial class 电子处方
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.txtPatientNameQuery = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dgvSalesOrder = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.订单编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.订单状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.出库时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.订单备注 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvProductList = new System.Windows.Forms.DataGridView();
            this.dgv2编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2药品名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.过期时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2零售价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2库存 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2药品简码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2会员折扣 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProductQuery = new System.Windows.Forms.TextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtAge = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.cboxGender = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPatientName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.lblOrderCode = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOrderState = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvSalesOrderInfo = new System.Windows.Forms.DataGridView();
            this.药品编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.库存 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.零售价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品有效期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Lab_HJcount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.lblCreateTime = new System.Windows.Forms.ToolStripLabel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrder)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductList)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel16.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrderInfo)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.30827F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1328, 598);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1318, 588);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1318, 588);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Wheat;
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1310, 552);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.tableLayoutPanel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1304, 546);
            this.panel5.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.panel11, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.509158F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.49084F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1304, 546);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.txtPatientNameQuery);
            this.panel11.Controls.Add(this.label10);
            this.panel11.Controls.Add(this.label9);
            this.panel11.Controls.Add(this.dtpEnd);
            this.panel11.Controls.Add(this.dtpStart);
            this.panel11.Controls.Add(this.btnClose);
            this.panel11.Controls.Add(this.btnQuery);
            this.panel11.Controls.Add(this.btnAdd);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1298, 35);
            this.panel11.TabIndex = 1;
            // 
            // txtPatientNameQuery
            // 
            this.txtPatientNameQuery.Location = new System.Drawing.Point(87, 4);
            this.txtPatientNameQuery.Name = "txtPatientNameQuery";
            this.txtPatientNameQuery.Size = new System.Drawing.Size(151, 26);
            this.txtPatientNameQuery.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(246, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 14;
            this.label10.Text = "时间范围：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 20);
            this.label9.TabIndex = 14;
            this.label9.Text = "患者姓名：";
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "yyyy年MM月dd日";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(491, 3);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(144, 26);
            this.dtpEnd.TabIndex = 13;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "yyyy年MM月dd日";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(329, 3);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(145, 26);
            this.dtpStart.TabIndex = 12;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(847, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 29);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(665, 2);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 29);
            this.btnQuery.TabIndex = 0;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(758, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 29);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "新增";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.dgvSalesOrder);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 44);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1298, 499);
            this.panel7.TabIndex = 0;
            // 
            // dgvSalesOrder
            // 
            this.dgvSalesOrder.AllowUserToAddRows = false;
            this.dgvSalesOrder.AllowUserToDeleteRows = false;
            this.dgvSalesOrder.AllowUserToResizeColumns = false;
            this.dgvSalesOrder.AllowUserToResizeRows = false;
            this.dgvSalesOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalesOrder.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvSalesOrder.ColumnHeadersHeight = 26;
            this.dgvSalesOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSalesOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.订单编号,
            this.制单人,
            this.制单时间,
            this.订单状态,
            this.出库时间,
            this.订单备注,
            this.会员编号});
            this.dgvSalesOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalesOrder.Location = new System.Drawing.Point(0, 0);
            this.dgvSalesOrder.Name = "dgvSalesOrder";
            this.dgvSalesOrder.RowHeadersWidth = 25;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvSalesOrder.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvSalesOrder.RowTemplate.Height = 23;
            this.dgvSalesOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalesOrder.Size = new System.Drawing.Size(1298, 499);
            this.dgvSalesOrder.TabIndex = 5;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "id";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Red;
            this.Id.DefaultCellStyle = dataGridViewCellStyle1;
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // 订单编号
            // 
            this.订单编号.DataPropertyName = "OrderCode";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Red;
            this.订单编号.DefaultCellStyle = dataGridViewCellStyle2;
            this.订单编号.HeaderText = "订单编号";
            this.订单编号.Name = "订单编号";
            this.订单编号.ReadOnly = true;
            // 
            // 制单人
            // 
            this.制单人.DataPropertyName = "UserId";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Red;
            this.制单人.DefaultCellStyle = dataGridViewCellStyle3;
            this.制单人.HeaderText = "制单人";
            this.制单人.Name = "制单人";
            this.制单人.ReadOnly = true;
            // 
            // 制单时间
            // 
            this.制单时间.DataPropertyName = "CreateTime";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Red;
            this.制单时间.DefaultCellStyle = dataGridViewCellStyle4;
            this.制单时间.HeaderText = "制单时间";
            this.制单时间.Name = "制单时间";
            this.制单时间.ReadOnly = true;
            // 
            // 订单状态
            // 
            this.订单状态.DataPropertyName = "State";
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Red;
            this.订单状态.DefaultCellStyle = dataGridViewCellStyle5;
            this.订单状态.HeaderText = "订单状态";
            this.订单状态.Name = "订单状态";
            this.订单状态.ReadOnly = true;
            // 
            // 出库时间
            // 
            this.出库时间.DataPropertyName = "ShipTime";
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Red;
            this.出库时间.DefaultCellStyle = dataGridViewCellStyle6;
            this.出库时间.HeaderText = "出库时间";
            this.出库时间.Name = "出库时间";
            this.出库时间.ReadOnly = true;
            // 
            // 订单备注
            // 
            this.订单备注.DataPropertyName = "Remark";
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Red;
            this.订单备注.DefaultCellStyle = dataGridViewCellStyle7;
            this.订单备注.HeaderText = "订单备注";
            this.订单备注.Name = "订单备注";
            this.订单备注.ReadOnly = true;
            // 
            // 会员编号
            // 
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Red;
            this.会员编号.DefaultCellStyle = dataGridViewCellStyle8;
            this.会员编号.HeaderText = "会员编号";
            this.会员编号.Name = "会员编号";
            this.会员编号.ReadOnly = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1310, 552);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "明细";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1310, 552);
            this.panel3.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel16, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.78707F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.21293F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1310, 552);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.panel9, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.panel10, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 344);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1302, 204);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.groupBox1);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(155, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1143, 196);
            this.panel9.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.dgvProductList);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1143, 196);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "药品列表";
            // 
            // dgvProductList
            // 
            this.dgvProductList.AllowUserToAddRows = false;
            this.dgvProductList.AllowUserToDeleteRows = false;
            this.dgvProductList.AllowUserToResizeColumns = false;
            this.dgvProductList.AllowUserToResizeRows = false;
            this.dgvProductList.BackgroundColor = System.Drawing.Color.Silver;
            this.dgvProductList.ColumnHeadersHeight = 26;
            this.dgvProductList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProductList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv2编号,
            this.dgv2药品名称,
            this.批号,
            this.过期时间,
            this.dgv2单位,
            this.dgv2规格,
            this.dgv2零售价,
            this.dgv2库存,
            this.dgv2生产企业,
            this.dgv2药品简码,
            this.dgv2会员折扣});
            this.dgvProductList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProductList.Location = new System.Drawing.Point(3, 22);
            this.dgvProductList.Name = "dgvProductList";
            this.dgvProductList.RowHeadersVisible = false;
            this.dgvProductList.RowHeadersWidth = 25;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.Silver;
            this.dgvProductList.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvProductList.RowTemplate.Height = 23;
            this.dgvProductList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductList.Size = new System.Drawing.Size(1137, 171);
            this.dgvProductList.TabIndex = 2;
            this.dgvProductList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvProductList_KeyDown);
            // 
            // dgv2编号
            // 
            this.dgv2编号.DataPropertyName = "ProductID";
            this.dgv2编号.HeaderText = "编号";
            this.dgv2编号.Name = "dgv2编号";
            this.dgv2编号.ReadOnly = true;
            this.dgv2编号.Width = 80;
            // 
            // dgv2药品名称
            // 
            this.dgv2药品名称.DataPropertyName = "ProductName";
            this.dgv2药品名称.HeaderText = "药品名称";
            this.dgv2药品名称.Name = "dgv2药品名称";
            this.dgv2药品名称.ReadOnly = true;
            this.dgv2药品名称.Width = 140;
            // 
            // 批号
            // 
            this.批号.DataPropertyName = "BatchNumber";
            this.批号.HeaderText = "批号";
            this.批号.Name = "批号";
            this.批号.ReadOnly = true;
            // 
            // 过期时间
            // 
            this.过期时间.DataPropertyName = "DateExpiry";
            this.过期时间.HeaderText = "过期时间";
            this.过期时间.Name = "过期时间";
            this.过期时间.ReadOnly = true;
            // 
            // dgv2单位
            // 
            this.dgv2单位.DataPropertyName = "MarketingUnit";
            this.dgv2单位.HeaderText = "单位";
            this.dgv2单位.Name = "dgv2单位";
            this.dgv2单位.ReadOnly = true;
            // 
            // dgv2规格
            // 
            this.dgv2规格.DataPropertyName = "Specification";
            this.dgv2规格.HeaderText = "规格";
            this.dgv2规格.Name = "dgv2规格";
            this.dgv2规格.ReadOnly = true;
            this.dgv2规格.Width = 71;
            // 
            // dgv2零售价
            // 
            this.dgv2零售价.DataPropertyName = "RetailPrice";
            this.dgv2零售价.HeaderText = "零售价";
            this.dgv2零售价.Name = "dgv2零售价";
            this.dgv2零售价.ReadOnly = true;
            this.dgv2零售价.Width = 140;
            // 
            // dgv2库存
            // 
            this.dgv2库存.DataPropertyName = "AvailableQuantity";
            this.dgv2库存.HeaderText = "库存";
            this.dgv2库存.Name = "dgv2库存";
            // 
            // dgv2生产企业
            // 
            this.dgv2生产企业.DataPropertyName = "ManufacturingEnterprise";
            this.dgv2生产企业.HeaderText = "生产企业";
            this.dgv2生产企业.Name = "dgv2生产企业";
            // 
            // dgv2药品简码
            // 
            this.dgv2药品简码.DataPropertyName = "ProductCode";
            this.dgv2药品简码.HeaderText = "药品简码";
            this.dgv2药品简码.Name = "dgv2药品简码";
            // 
            // dgv2会员折扣
            // 
            this.dgv2会员折扣.DataPropertyName = "MemberDiscount";
            this.dgv2会员折扣.HeaderText = "会员折扣";
            this.dgv2会员折扣.Name = "dgv2会员折扣";
            this.dgv2会员折扣.ReadOnly = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label1);
            this.panel10.Controls.Add(this.txtProductQuery);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(4, 4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(144, 196);
            this.panel10.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "药品检索：";
            // 
            // txtProductQuery
            // 
            this.txtProductQuery.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtProductQuery.Location = new System.Drawing.Point(3, 30);
            this.txtProductQuery.Name = "txtProductQuery";
            this.txtProductQuery.Size = new System.Drawing.Size(138, 26);
            this.txtProductQuery.TabIndex = 0;
            this.txtProductQuery.TextChanged += new System.EventHandler(this.txtProductQuery_TextChanged);
            this.txtProductQuery.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProductQuery_KeyDown);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.groupBox2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(4, 4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1302, 333);
            this.panel16.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1302, 333);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "电子处方明细：";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel4);
            this.panel4.Controls.Add(this.toolStrip1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 22);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1296, 308);
            this.panel4.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1296, 283);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.txtAge);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.cboxGender);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.txtRemark);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.txtPatientName);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.lblId);
            this.panel8.Controls.Add(this.lblOrderCode);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.lblOrderState);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(254, 277);
            this.panel8.TabIndex = 2;
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(199, 88);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(52, 26);
            this.txtAge.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(146, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 21);
            this.label8.TabIndex = 12;
            this.label8.Text = "年龄:";
            // 
            // cboxGender
            // 
            this.cboxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxGender.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cboxGender.FormattingEnabled = true;
            this.cboxGender.Items.AddRange(new object[] {
            "男",
            "女"});
            this.cboxGender.Location = new System.Drawing.Point(86, 89);
            this.cboxGender.Name = "cboxGender";
            this.cboxGender.Size = new System.Drawing.Size(51, 25);
            this.cboxGender.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(11, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 21);
            this.label5.TabIndex = 10;
            this.label5.Text = "性别:";
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(3, 141);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(241, 136);
            this.txtRemark.TabIndex = 9;
            this.txtRemark.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(3, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 21);
            this.label7.TabIndex = 8;
            this.label7.Text = "门诊诊断:";
            // 
            // txtPatientName
            // 
            this.txtPatientName.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtPatientName.Location = new System.Drawing.Point(86, 57);
            this.txtPatientName.Name = "txtPatientName";
            this.txtPatientName.Size = new System.Drawing.Size(165, 26);
            this.txtPatientName.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(3, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 21);
            this.label6.TabIndex = 6;
            this.label6.Text = "患者姓名:";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblId.Location = new System.Drawing.Point(214, 28);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(28, 21);
            this.lblId.TabIndex = 5;
            this.lblId.Text = "00";
            // 
            // lblOrderCode
            // 
            this.lblOrderCode.AutoSize = true;
            this.lblOrderCode.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblOrderCode.Location = new System.Drawing.Point(82, 3);
            this.lblOrderCode.Name = "lblOrderCode";
            this.lblOrderCode.Size = new System.Drawing.Size(0, 17);
            this.lblOrderCode.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(143, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "主单ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(3, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "处方状态:";
            // 
            // lblOrderState
            // 
            this.lblOrderState.AutoSize = true;
            this.lblOrderState.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblOrderState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblOrderState.Location = new System.Drawing.Point(82, 28);
            this.lblOrderState.Name = "lblOrderState";
            this.lblOrderState.Size = new System.Drawing.Size(42, 21);
            this.lblOrderState.TabIndex = 3;
            this.lblOrderState.Text = "制单";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 21);
            this.label2.TabIndex = 0;
            this.label2.Text = "处方编号:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvSalesOrderInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(263, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 277);
            this.panel1.TabIndex = 3;
            // 
            // dgvSalesOrderInfo
            // 
            this.dgvSalesOrderInfo.AllowUserToAddRows = false;
            this.dgvSalesOrderInfo.AllowUserToDeleteRows = false;
            this.dgvSalesOrderInfo.AllowUserToResizeColumns = false;
            this.dgvSalesOrderInfo.AllowUserToResizeRows = false;
            this.dgvSalesOrderInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalesOrderInfo.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvSalesOrderInfo.ColumnHeadersHeight = 26;
            this.dgvSalesOrderInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSalesOrderInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.药品编码,
            this.药品名称,
            this.规格,
            this.单位,
            this.库存,
            this.生产企业,
            this.零售价,
            this.会员价,
            this.数量,
            this.金额,
            this.会员金额,
            this.药品批号,
            this.药品有效期});
            this.dgvSalesOrderInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalesOrderInfo.Location = new System.Drawing.Point(0, 0);
            this.dgvSalesOrderInfo.Name = "dgvSalesOrderInfo";
            this.dgvSalesOrderInfo.RowHeadersWidth = 25;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvSalesOrderInfo.RowsDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvSalesOrderInfo.RowTemplate.Height = 23;
            this.dgvSalesOrderInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSalesOrderInfo.Size = new System.Drawing.Size(1030, 277);
            this.dgvSalesOrderInfo.TabIndex = 9;
            // 
            // 药品编码
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Blue;
            this.药品编码.DefaultCellStyle = dataGridViewCellStyle11;
            this.药品编码.HeaderText = "药品编码";
            this.药品编码.Name = "药品编码";
            this.药品编码.ReadOnly = true;
            this.药品编码.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 药品名称
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.药品名称.DefaultCellStyle = dataGridViewCellStyle12;
            this.药品名称.HeaderText = "药品名称";
            this.药品名称.Name = "药品名称";
            this.药品名称.ReadOnly = true;
            this.药品名称.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 规格
            // 
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.规格.DefaultCellStyle = dataGridViewCellStyle13;
            this.规格.HeaderText = "规格";
            this.规格.Name = "规格";
            this.规格.ReadOnly = true;
            this.规格.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 单位
            // 
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            this.单位.DefaultCellStyle = dataGridViewCellStyle14;
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            this.单位.ReadOnly = true;
            this.单位.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 库存
            // 
            this.库存.DataPropertyName = "AvailableQuantity";
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Blue;
            this.库存.DefaultCellStyle = dataGridViewCellStyle15;
            this.库存.HeaderText = "库存";
            this.库存.Name = "库存";
            this.库存.ReadOnly = true;
            this.库存.Visible = false;
            // 
            // 生产企业
            // 
            this.生产企业.DataPropertyName = "ManufacturingEnterprise";
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.Blue;
            this.生产企业.DefaultCellStyle = dataGridViewCellStyle16;
            this.生产企业.HeaderText = "生产企业";
            this.生产企业.Name = "生产企业";
            this.生产企业.ReadOnly = true;
            // 
            // 零售价
            // 
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Blue;
            this.零售价.DefaultCellStyle = dataGridViewCellStyle17;
            this.零售价.HeaderText = "零售价";
            this.零售价.Name = "零售价";
            this.零售价.ReadOnly = true;
            this.零售价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 会员价
            // 
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.Blue;
            this.会员价.DefaultCellStyle = dataGridViewCellStyle18;
            this.会员价.HeaderText = "会员价";
            this.会员价.Name = "会员价";
            this.会员价.ReadOnly = true;
            this.会员价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 数量
            // 
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.Blue;
            this.数量.DefaultCellStyle = dataGridViewCellStyle19;
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.ReadOnly = true;
            this.数量.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 金额
            // 
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Red;
            this.金额.DefaultCellStyle = dataGridViewCellStyle20;
            this.金额.HeaderText = "金额";
            this.金额.Name = "金额";
            this.金额.ReadOnly = true;
            this.金额.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 会员金额
            // 
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Red;
            this.会员金额.DefaultCellStyle = dataGridViewCellStyle21;
            this.会员金额.HeaderText = "会员金额";
            this.会员金额.Name = "会员金额";
            this.会员金额.ReadOnly = true;
            // 
            // 药品批号
            // 
            this.药品批号.DataPropertyName = "BatchNumber";
            this.药品批号.HeaderText = "药品批号";
            this.药品批号.Name = "药品批号";
            this.药品批号.ReadOnly = true;
            // 
            // 药品有效期
            // 
            this.药品有效期.DataPropertyName = "DateExpiry";
            this.药品有效期.HeaderText = "药品有效期";
            this.药品有效期.Name = "药品有效期";
            this.药品有效期.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Lab_HJcount,
            this.toolStripLabel2,
            this.toolStripLabel3,
            this.btnSave,
            this.btnDelete,
            this.lblCreateTime});
            this.toolStrip1.Location = new System.Drawing.Point(0, 283);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1296, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Lab_HJcount
            // 
            this.Lab_HJcount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Lab_HJcount.Name = "Lab_HJcount";
            this.Lab_HJcount.Size = new System.Drawing.Size(39, 22);
            this.Lab_HJcount.Text = "共1笔";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Margin = new System.Windows.Forms.Padding(50, 1, 0, 2);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(0, 22);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Margin = new System.Windows.Forms.Padding(50, 1, 0, 2);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(68, 22);
            this.toolStripLabel3.Text = "医生:libao ";
            // 
            // btnSave
            // 
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(36, 22);
            this.btnSave.Text = "保存";
            // 
            // btnDelete
            // 
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(36, 22);
            this.btnDelete.Text = "删除";
            // 
            // lblCreateTime
            // 
            this.lblCreateTime.Margin = new System.Windows.Forms.Padding(100, 1, 0, 2);
            this.lblCreateTime.Name = "lblCreateTime";
            this.lblCreateTime.Size = new System.Drawing.Size(186, 22);
            this.lblCreateTime.Text = "制表时间：2018-01-01 12:23:22";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 32);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1310, 552);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "退出";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // 电子处方
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1328, 598);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(1344, 637);
            this.MinimumSize = new System.Drawing.Size(1344, 637);
            this.Name = "电子处方";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "电子处方";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrder)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductList)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAge)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalesOrderInfo)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.DataGridView dgvSalesOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 出库时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单备注;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员编号;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvProductList;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2药品名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 过期时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2零售价;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2库存;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2药品简码;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2会员折扣;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProductQuery;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RichTextBox txtRemark;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPatientName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblOrderCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOrderState;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvSalesOrderInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 库存;
        private System.Windows.Forms.DataGridViewTextBoxColumn 生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn 零售价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品有效期;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboxGender;
        private System.Windows.Forms.NumericUpDown txtAge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtPatientNameQuery;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.ToolStripLabel Lab_HJcount;
        private System.Windows.Forms.ToolStripLabel lblCreateTime;
    }
}