﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.ToolHelper;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 实物明细账 : Form
    {
        private readonly IStockProductDetailsRepository productDetailsRepository = new StockProductDetailsRepository();

        public 实物明细账()
        {
            InitializeComponent();
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        public string UserId { get; set; }
        public string UserName { get; set; }

        public 实物明细账(string userId, string userName)
        {
            this.UserId = userId;
            this.UserName = userName;
            InitializeComponent();

            BindData();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            StringBuilder where = new StringBuilder(" where 1=1 ");
            string code = txtProduct.Text.Trim();
            string order = txtOrder.Text.Trim();
                 
            string beginDate = dtpStart.Text;
            string endDate = dtpEnd.Text;
            if (!code.IsNullOrEmpty())
            {
                where.Append($" and (T.ProductName LIKE '%{code}%' OR T.ProductCode LIKE '%{code}%' OR T.ProductId LIKE '%{code}%')");
            }
            if (!order.IsNullOrEmpty())
            {
                where.Append($" and T.InvoiceCode LIKE '%{order}%'");
            }
            if (!beginDate.IsNullOrEmpty())
            {
                where.Append($" and CONVERT(VARCHAR,InDate,23) >= '{DateTime.Parse(beginDate).ToString("yyyy-MM-dd")}'");
            }
            if (!endDate.IsNullOrEmpty())
            {
                where.Append($" and CONVERT(VARCHAR,InDate,23) <= '{DateTime.Parse(endDate).ToString("yyyy-MM-dd")}'");
            }

            var list = productDetailsRepository.GetAllList(where.ToString());

            dgvData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = list;
        }

        private void 销售统计_Load(object sender, EventArgs e)
        {
            BindData();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            BindData();
            var path = ExcelHelperForCs.ExportToExcel(dgvData, "实物明细报表");
            MessageBox.Show($"导出成功，文件路径:{path}");
        }
    }
}