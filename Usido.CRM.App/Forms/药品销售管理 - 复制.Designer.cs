﻿namespace Usido.CRM.App.Forms
{
    partial class 药品销售管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(药品销售管理));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripAdd = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripBC_RK = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripEdit = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripCancel = new System.Windows.Forms.ToolStripDropDownButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvSupplyList = new System.Windows.Forms.DataGridView();
            this.订单编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.销售单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.添加时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.添加人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Panel_RK = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Lab_ZDR = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Lab_ZDSS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dgv2编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2药品名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.过期时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2零售价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2库存 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2药品简码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2会员折扣 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContextM_DelRK = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.Lab_HJcount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.药品编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.库存 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.零售价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtZhaiYao = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Lab_DJZT = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Lab_DJBH = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_RK.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.ContextM_DelRK.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAdd,
            this.toolStripBC_RK,
            this.toolStripEdit,
            this.toolStripCancel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1121, 26);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripAdd
            // 
            this.toolStripAdd.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAdd.Image")));
            this.toolStripAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAdd.Name = "toolStripAdd";
            this.toolStripAdd.Size = new System.Drawing.Size(66, 24);
            this.toolStripAdd.Text = "新增";
            this.toolStripAdd.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripAdd.Click += new System.EventHandler(this.toolStripAdd_Click);
            // 
            // toolStripBC_RK
            // 
            this.toolStripBC_RK.Enabled = false;
            this.toolStripBC_RK.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripBC_RK.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBC_RK.Image")));
            this.toolStripBC_RK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBC_RK.Name = "toolStripBC_RK";
            this.toolStripBC_RK.Size = new System.Drawing.Size(66, 24);
            this.toolStripBC_RK.Text = "保存";
            this.toolStripBC_RK.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripBC_RK.Click += new System.EventHandler(this.toolStripDelete_Click);
            // 
            // toolStripEdit
            // 
            this.toolStripEdit.Enabled = false;
            this.toolStripEdit.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripEdit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEdit.Image")));
            this.toolStripEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripEdit.Name = "toolStripEdit";
            this.toolStripEdit.Size = new System.Drawing.Size(66, 24);
            this.toolStripEdit.Text = "编辑";
            this.toolStripEdit.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripEdit.Click += new System.EventHandler(this.toolStripEdit_Click);
            // 
            // toolStripCancel
            // 
            this.toolStripCancel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCancel.Image")));
            this.toolStripCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCancel.Name = "toolStripCancel";
            this.toolStripCancel.Size = new System.Drawing.Size(66, 24);
            this.toolStripCancel.Text = "取消";
            this.toolStripCancel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripCancel.Click += new System.EventHandler(this.toolStripCancel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.30827F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1121, 535);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1111, 525);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1111, 525);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Wheat;
            this.tabPage1.Controls.Add(this.dgvSupplyList);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1103, 492);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            // 
            // dgvSupplyList
            // 
            this.dgvSupplyList.AllowUserToResizeColumns = false;
            this.dgvSupplyList.AllowUserToResizeRows = false;
            this.dgvSupplyList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSupplyList.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvSupplyList.ColumnHeadersHeight = 26;
            this.dgvSupplyList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSupplyList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.订单编号,
            this.供应商,
            this.药品规格,
            this.销售单位,
            this.dataGridViewTextBoxColumn1,
            this.添加时间,
            this.添加人,
            this.Id});
            this.dgvSupplyList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSupplyList.Location = new System.Drawing.Point(3, 3);
            this.dgvSupplyList.Name = "dgvSupplyList";
            this.dgvSupplyList.RowHeadersWidth = 25;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvSupplyList.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.dgvSupplyList.RowTemplate.Height = 23;
            this.dgvSupplyList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplyList.Size = new System.Drawing.Size(1097, 486);
            this.dgvSupplyList.TabIndex = 3;
            this.dgvSupplyList.DoubleClick += new System.EventHandler(this.dgvSupplyList_DoubleClick);
            // 
            // 订单编号
            // 
            this.订单编号.DataPropertyName = "OrderCode";
            this.订单编号.HeaderText = "订单编号";
            this.订单编号.Name = "订单编号";
            // 
            // 供应商
            // 
            this.供应商.DataPropertyName = "SupplierName";
            this.供应商.HeaderText = "供应商";
            this.供应商.Name = "供应商";
            this.供应商.ReadOnly = true;
            // 
            // 药品规格
            // 
            this.药品规格.DataPropertyName = "CreateTime";
            this.药品规格.HeaderText = "制单时间";
            this.药品规格.Name = "药品规格";
            this.药品规格.ReadOnly = true;
            // 
            // 销售单位
            // 
            this.销售单位.DataPropertyName = "CreateUserName";
            this.销售单位.HeaderText = "制单人";
            this.销售单位.Name = "销售单位";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "State";
            this.dataGridViewTextBoxColumn1.HeaderText = "订单状态";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // 添加时间
            // 
            this.添加时间.DataPropertyName = "PutUserName";
            this.添加时间.HeaderText = "入库人";
            this.添加时间.Name = "添加时间";
            // 
            // 添加人
            // 
            this.添加人.DataPropertyName = "PutTime";
            this.添加人.HeaderText = "入库时间";
            this.添加人.Name = "添加人";
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1103, 492);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "明细";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Panel_RK);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1103, 492);
            this.panel3.TabIndex = 0;
            // 
            // Panel_RK
            // 
            this.Panel_RK.BackColor = System.Drawing.Color.SkyBlue;
            this.Panel_RK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_RK.Controls.Add(this.tableLayoutPanel2);
            this.Panel_RK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_RK.Location = new System.Drawing.Point(0, 0);
            this.Panel_RK.Name = "Panel_RK";
            this.Panel_RK.Size = new System.Drawing.Size(1103, 492);
            this.Panel_RK.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel7, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 388F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1101, 490);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtZhaiYao);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.Lab_ZDR);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.Lab_ZDSS);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 463);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1095, 24);
            this.panel7.TabIndex = 3;
            // 
            // Lab_ZDR
            // 
            this.Lab_ZDR.AutoSize = true;
            this.Lab_ZDR.ForeColor = System.Drawing.Color.Black;
            this.Lab_ZDR.Location = new System.Drawing.Point(68, 0);
            this.Lab_ZDR.Name = "Lab_ZDR";
            this.Lab_ZDR.Size = new System.Drawing.Size(37, 20);
            this.Lab_ZDR.TabIndex = 7;
            this.Lab_ZDR.Text = "李宝";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "制单人：";
            // 
            // Lab_ZDSS
            // 
            this.Lab_ZDSS.AutoSize = true;
            this.Lab_ZDSS.ForeColor = System.Drawing.Color.Black;
            this.Lab_ZDSS.Location = new System.Drawing.Point(774, 1);
            this.Lab_ZDSS.Name = "Lab_ZDSS";
            this.Lab_ZDSS.Size = new System.Drawing.Size(157, 20);
            this.Lab_ZDSS.TabIndex = 5;
            this.Lab_ZDSS.Text = "2019年1月4日 16:44:22";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(700, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "制单时间：";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Controls.Add(this.statusStrip2);
            this.panel6.Controls.Add(this.dataGridView1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 75);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1095, 382);
            this.panel6.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(258, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(837, 235);
            this.panel1.TabIndex = 8;
            this.panel1.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.dataGridView2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(835, 233);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "药品列表";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.Silver;
            this.dataGridView2.ColumnHeadersHeight = 26;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv2编号,
            this.dgv2药品名称,
            this.批号,
            this.过期时间,
            this.dgv2单位,
            this.dgv2规格,
            this.dgv2零售价,
            this.dgv2库存,
            this.dgv2生产企业,
            this.dgv2药品简码,
            this.dgv2会员折扣});
            this.dataGridView2.ContextMenuStrip = this.ContextM_DelRK;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 22);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 25;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.Silver;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(829, 208);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.DoubleClick += new System.EventHandler(this.dataGridView2_DoubleClick);
            this.dataGridView2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyDown);
            // 
            // dgv2编号
            // 
            this.dgv2编号.DataPropertyName = "ProductID";
            this.dgv2编号.HeaderText = "编号";
            this.dgv2编号.Name = "dgv2编号";
            this.dgv2编号.ReadOnly = true;
            this.dgv2编号.Width = 80;
            // 
            // dgv2药品名称
            // 
            this.dgv2药品名称.DataPropertyName = "ProductName";
            this.dgv2药品名称.HeaderText = "药品名称";
            this.dgv2药品名称.Name = "dgv2药品名称";
            this.dgv2药品名称.ReadOnly = true;
            this.dgv2药品名称.Width = 140;
            // 
            // 批号
            // 
            this.批号.DataPropertyName = "BatchNumber";
            this.批号.HeaderText = "批号";
            this.批号.Name = "批号";
            this.批号.ReadOnly = true;
            // 
            // 过期时间
            // 
            this.过期时间.DataPropertyName = "DateExpiry";
            this.过期时间.HeaderText = "过期时间";
            this.过期时间.Name = "过期时间";
            this.过期时间.ReadOnly = true;
            // 
            // dgv2单位
            // 
            this.dgv2单位.DataPropertyName = "MarketingUnit";
            this.dgv2单位.HeaderText = "单位";
            this.dgv2单位.Name = "dgv2单位";
            this.dgv2单位.ReadOnly = true;
            // 
            // dgv2规格
            // 
            this.dgv2规格.DataPropertyName = "Specification";
            this.dgv2规格.HeaderText = "规格";
            this.dgv2规格.Name = "dgv2规格";
            this.dgv2规格.ReadOnly = true;
            this.dgv2规格.Width = 71;
            // 
            // dgv2零售价
            // 
            this.dgv2零售价.DataPropertyName = "RetailPrice";
            this.dgv2零售价.HeaderText = "零售价";
            this.dgv2零售价.Name = "dgv2零售价";
            this.dgv2零售价.ReadOnly = true;
            this.dgv2零售价.Width = 140;
            // 
            // dgv2库存
            // 
            this.dgv2库存.DataPropertyName = "AvailableQuantity";
            this.dgv2库存.HeaderText = "库存";
            this.dgv2库存.Name = "dgv2库存";
            // 
            // dgv2生产企业
            // 
            this.dgv2生产企业.DataPropertyName = "ManufacturingEnterprise";
            this.dgv2生产企业.HeaderText = "生产企业";
            this.dgv2生产企业.Name = "dgv2生产企业";
            // 
            // dgv2药品简码
            // 
            this.dgv2药品简码.DataPropertyName = "ProductCode";
            this.dgv2药品简码.HeaderText = "药品简码";
            this.dgv2药品简码.Name = "dgv2药品简码";
            // 
            // dgv2会员折扣
            // 
            this.dgv2会员折扣.DataPropertyName = "MemberDiscount";
            this.dgv2会员折扣.HeaderText = "会员折扣";
            this.dgv2会员折扣.Name = "dgv2会员折扣";
            this.dgv2会员折扣.ReadOnly = true;
            // 
            // ContextM_DelRK
            // 
            this.ContextM_DelRK.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.ContextM_DelRK.Name = "contextMenuStrip1";
            this.ContextM_DelRK.Size = new System.Drawing.Size(125, 26);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除ToolStripMenuItem.Text = "删除此条";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Lab_HJcount,
            this.toolStripStatusLabel2});
            this.statusStrip2.Location = new System.Drawing.Point(0, 357);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1095, 25);
            this.statusStrip2.TabIndex = 7;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // Lab_HJcount
            // 
            this.Lab_HJcount.BackColor = System.Drawing.Color.Snow;
            this.Lab_HJcount.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Lab_HJcount.Name = "Lab_HJcount";
            this.Lab_HJcount.Size = new System.Drawing.Size(51, 20);
            this.Lab_HJcount.Text = "合计：";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Snow;
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripStatusLabel2.Margin = new System.Windows.Forms.Padding(600, 3, 0, 2);
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(65, 20);
            this.toolStripStatusLabel2.Text = "总金额：";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridView1.ColumnHeadersHeight = 26;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.药品编码,
            this.药品名称,
            this.规格,
            this.单位,
            this.库存,
            this.生产企业,
            this.零售价,
            this.会员价,
            this.数量,
            this.金额,
            this.会员金额});
            this.dataGridView1.ContextMenuStrip = this.ContextM_DelRK;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 25;
            dataGridViewCellStyle52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1095, 382);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint_1);
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // 药品编码
            // 
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.HotTrack;
            this.药品编码.DefaultCellStyle = dataGridViewCellStyle39;
            this.药品编码.HeaderText = "药品编码";
            this.药品编码.Name = "药品编码";
            this.药品编码.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.药品编码.Width = 90;
            // 
            // 药品名称
            // 
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle47.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black;
            this.药品名称.DefaultCellStyle = dataGridViewCellStyle47;
            this.药品名称.HeaderText = "药品名称";
            this.药品名称.Name = "药品名称";
            this.药品名称.ReadOnly = true;
            this.药品名称.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.药品名称.Width = 140;
            // 
            // 规格
            // 
            dataGridViewCellStyle48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            this.规格.DefaultCellStyle = dataGridViewCellStyle48;
            this.规格.HeaderText = "规格";
            this.规格.Name = "规格";
            this.规格.ReadOnly = true;
            this.规格.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.规格.Width = 140;
            // 
            // 单位
            // 
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            this.单位.DefaultCellStyle = dataGridViewCellStyle49;
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            this.单位.ReadOnly = true;
            this.单位.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.单位.Width = 60;
            // 
            // 库存
            // 
            this.库存.DataPropertyName = "AvailableQuantity";
            this.库存.HeaderText = "库存";
            this.库存.Name = "库存";
            this.库存.ReadOnly = true;
            this.库存.Visible = false;
            // 
            // 生产企业
            // 
            this.生产企业.DataPropertyName = "ManufacturingEnterprise";
            this.生产企业.HeaderText = "生产企业";
            this.生产企业.Name = "生产企业";
            this.生产企业.ReadOnly = true;
            // 
            // 零售价
            // 
            this.零售价.HeaderText = "零售价";
            this.零售价.Name = "零售价";
            this.零售价.ReadOnly = true;
            this.零售价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.零售价.Width = 70;
            // 
            // 会员价
            // 
            this.会员价.HeaderText = "会员价";
            this.会员价.Name = "会员价";
            this.会员价.ReadOnly = true;
            this.会员价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 数量
            // 
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.数量.Width = 70;
            // 
            // 金额
            // 
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Red;
            this.金额.DefaultCellStyle = dataGridViewCellStyle50;
            this.金额.HeaderText = "金额";
            this.金额.Name = "金额";
            this.金额.ReadOnly = true;
            this.金额.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.金额.Width = 70;
            // 
            // 会员金额
            // 
            dataGridViewCellStyle51.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.Red;
            this.会员金额.DefaultCellStyle = dataGridViewCellStyle51;
            this.会员金额.HeaderText = "会员金额";
            this.会员金额.Name = "会员金额";
            this.会员金额.ReadOnly = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.comboBox1);
            this.panel5.Controls.Add(this.txtHY);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.Lab_DJZT);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.Lab_DJBH);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 37);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1095, 32);
            this.panel5.TabIndex = 1;
            // 
            // txtZhaiYao
            // 
            this.txtZhaiYao.Location = new System.Drawing.Point(331, 1);
            this.txtZhaiYao.Name = "txtZhaiYao";
            this.txtZhaiYao.Size = new System.Drawing.Size(242, 26);
            this.txtZhaiYao.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(256, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 20);
            this.label8.TabIndex = 11;
            this.label8.Text = "单据摘要：";
            // 
            // txtHY
            // 
            this.txtHY.Location = new System.Drawing.Point(778, 3);
            this.txtHY.Name = "txtHY";
            this.txtHY.Size = new System.Drawing.Size(128, 26);
            this.txtHY.TabIndex = 10;
            this.txtHY.TextChanged += new System.EventHandler(this.txtHY_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(700, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "会员检索：";
            // 
            // Lab_DJZT
            // 
            this.Lab_DJZT.AutoSize = true;
            this.Lab_DJZT.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.Lab_DJZT.Location = new System.Drawing.Point(345, 5);
            this.Lab_DJZT.Name = "Lab_DJZT";
            this.Lab_DJZT.Size = new System.Drawing.Size(37, 20);
            this.Lab_DJZT.TabIndex = 3;
            this.Lab_DJZT.Text = "制单";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(270, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "单据状态：";
            // 
            // Lab_DJBH
            // 
            this.Lab_DJBH.AutoSize = true;
            this.Lab_DJBH.Location = new System.Drawing.Point(81, 5);
            this.Lab_DJBH.Name = "Lab_DJBH";
            this.Lab_DJBH.Size = new System.Drawing.Size(89, 20);
            this.Lab_DJBH.TabIndex = 1;
            this.Lab_DJBH.Text = "1029010409";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "单据编号：";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.linkLabel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1095, 28);
            this.panel4.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel1.Location = new System.Drawing.Point(366, 2);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(187, 29);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "药品销售制单";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(927, 1);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 28);
            this.comboBox1.TabIndex = 11;
            // 
            // 药品销售管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1121, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "药品销售管理";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "药品销售管理";
            this.Load += new System.EventHandler(this.采购入库管理_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.Panel_RK.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ContextM_DelRK.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripAdd;
        private System.Windows.Forms.ToolStripDropDownButton toolStripBC_RK;
        private System.Windows.Forms.ToolStripDropDownButton toolStripEdit;
        private System.Windows.Forms.ToolStripDropDownButton toolStripCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip ContextM_DelRK;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvSupplyList;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn 销售单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 添加时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 添加人;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.Panel Panel_RK;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label Lab_ZDR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Lab_ZDSS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel Lab_HJcount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtHY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Lab_DJZT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Lab_DJBH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.TextBox txtZhaiYao;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 库存;
        private System.Windows.Forms.DataGridViewTextBoxColumn 生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn 零售价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2药品名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 过期时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2零售价;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2库存;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2药品简码;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2会员折扣;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}