﻿namespace Usido.CRM.App.Forms
{
    partial class 药品退货管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(药品退货管理));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripAdd = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripCancel = new System.Windows.Forms.ToolStripDropDownButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvSupplyList = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退货订单编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退货订单状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退货入库时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lab_xszdID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lab_thbm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.LAB_id = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lab_ddzt = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lab_djbh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.药品编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.销售退货订单明细ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.零售价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.会员金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.药品有效期 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.可退货数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.退货数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Lab_HJcount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.退货 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.全部退货 = new System.Windows.Forms.ToolStripMenuItem();
            this.部分退货 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel16.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.退货.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAdd,
            this.toolStripCancel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1121, 26);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "药品退货管理";
            // 
            // toolStripAdd
            // 
            this.toolStripAdd.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAdd.Name = "toolStripAdd";
            this.toolStripAdd.Size = new System.Drawing.Size(50, 24);
            this.toolStripAdd.Text = "新增";
            this.toolStripAdd.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripAdd.Click += new System.EventHandler(this.toolStripAdd_Click);
            // 
            // toolStripCancel
            // 
            this.toolStripCancel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCancel.Name = "toolStripCancel";
            this.toolStripCancel.Size = new System.Drawing.Size(50, 24);
            this.toolStripCancel.Text = "取消";
            this.toolStripCancel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripCancel.Click += new System.EventHandler(this.toolStripCancel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.30827F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 533F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1121, 535);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1111, 525);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1111, 525);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Wheat;
            this.tabPage1.Controls.Add(this.dgvSupplyList);
            this.tabPage1.Location = new System.Drawing.Point(4, 32);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1103, 489);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            // 
            // dgvSupplyList
            // 
            this.dgvSupplyList.AllowUserToAddRows = false;
            this.dgvSupplyList.AllowUserToDeleteRows = false;
            this.dgvSupplyList.AllowUserToResizeColumns = false;
            this.dgvSupplyList.AllowUserToResizeRows = false;
            this.dgvSupplyList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSupplyList.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvSupplyList.ColumnHeadersHeight = 26;
            this.dgvSupplyList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSupplyList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.退货订单编号,
            this.制单人,
            this.制单时间,
            this.退货订单状态,
            this.退货入库时间});
            this.dgvSupplyList.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvSupplyList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSupplyList.Location = new System.Drawing.Point(3, 3);
            this.dgvSupplyList.Name = "dgvSupplyList";
            this.dgvSupplyList.RowHeadersWidth = 25;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvSupplyList.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSupplyList.RowTemplate.Height = 23;
            this.dgvSupplyList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplyList.Size = new System.Drawing.Size(1097, 483);
            this.dgvSupplyList.TabIndex = 3;
            this.dgvSupplyList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplyList_CellContentDoubleClick);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "id";
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Red;
            this.Id.DefaultCellStyle = dataGridViewCellStyle1;
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // 退货订单编号
            // 
            this.退货订单编号.DataPropertyName = "OrderCode";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Red;
            this.退货订单编号.DefaultCellStyle = dataGridViewCellStyle2;
            this.退货订单编号.HeaderText = "退货订单编号";
            this.退货订单编号.Name = "退货订单编号";
            this.退货订单编号.ReadOnly = true;
            // 
            // 制单人
            // 
            this.制单人.DataPropertyName = "UserName";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Red;
            this.制单人.DefaultCellStyle = dataGridViewCellStyle3;
            this.制单人.HeaderText = "制单人";
            this.制单人.Name = "制单人";
            this.制单人.ReadOnly = true;
            // 
            // 制单时间
            // 
            this.制单时间.DataPropertyName = "CreateTime";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Red;
            this.制单时间.DefaultCellStyle = dataGridViewCellStyle4;
            this.制单时间.HeaderText = "制单时间";
            this.制单时间.Name = "制单时间";
            this.制单时间.ReadOnly = true;
            // 
            // 退货订单状态
            // 
            this.退货订单状态.DataPropertyName = "State";
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Red;
            this.退货订单状态.DefaultCellStyle = dataGridViewCellStyle5;
            this.退货订单状态.HeaderText = "退货订单状态";
            this.退货订单状态.Name = "退货订单状态";
            this.退货订单状态.ReadOnly = true;
            // 
            // 退货入库时间
            // 
            this.退货入库时间.DataPropertyName = "PutTime";
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Red;
            this.退货入库时间.DefaultCellStyle = dataGridViewCellStyle6;
            this.退货入库时间.HeaderText = "退货入库时间";
            this.退货入库时间.Name = "退货入库时间";
            this.退货入库时间.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 32);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1103, 489);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "明细";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1103, 489);
            this.panel3.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel16, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 489F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1103, 489);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.groupBox2);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(3, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1097, 483);
            this.panel16.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1097, 483);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "药品销售退货订单明细：";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel4);
            this.panel4.Controls.Add(this.toolStrip1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 22);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1091, 458);
            this.panel4.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.16859F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.83141F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1091, 433);
            this.tableLayoutPanel4.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lab_xszdID);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.lab_thbm);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.LAB_id);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.lab_ddzt);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.lab_djbh);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1085, 76);
            this.panel5.TabIndex = 0;
            // 
            // lab_xszdID
            // 
            this.lab_xszdID.AutoSize = true;
            this.lab_xszdID.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_xszdID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lab_xszdID.Location = new System.Drawing.Point(1030, 40);
            this.lab_xszdID.Name = "lab_xszdID";
            this.lab_xszdID.Size = new System.Drawing.Size(34, 25);
            this.lab_xszdID.TabIndex = 10;
            this.lab_xszdID.Text = "00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(912, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 19);
            this.label6.TabIndex = 9;
            this.label6.Text = "退货主单ID:";
            // 
            // lab_thbm
            // 
            this.lab_thbm.BackColor = System.Drawing.Color.White;
            this.lab_thbm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lab_thbm.Enabled = false;
            this.lab_thbm.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_thbm.Location = new System.Drawing.Point(147, 43);
            this.lab_thbm.Name = "lab_thbm";
            this.lab_thbm.Size = new System.Drawing.Size(229, 29);
            this.lab_thbm.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(3, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 19);
            this.label1.TabIndex = 7;
            this.label1.Text = "退货单据编号：";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(382, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "检索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LAB_id
            // 
            this.LAB_id.AutoSize = true;
            this.LAB_id.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LAB_id.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.LAB_id.Location = new System.Drawing.Point(1030, 3);
            this.LAB_id.Name = "LAB_id";
            this.LAB_id.Size = new System.Drawing.Size(34, 25);
            this.LAB_id.TabIndex = 5;
            this.LAB_id.Text = "00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(912, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "销售主单ID:";
            // 
            // lab_ddzt
            // 
            this.lab_ddzt.AutoSize = true;
            this.lab_ddzt.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_ddzt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lab_ddzt.Location = new System.Drawing.Point(794, 38);
            this.lab_ddzt.Name = "lab_ddzt";
            this.lab_ddzt.Size = new System.Drawing.Size(50, 25);
            this.lab_ddzt.TabIndex = 3;
            this.lab_ddzt.Text = "制单";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(649, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "退货单据状态：";
            // 
            // lab_djbh
            // 
            this.lab_djbh.BackColor = System.Drawing.Color.White;
            this.lab_djbh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lab_djbh.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_djbh.Location = new System.Drawing.Point(147, 4);
            this.lab_djbh.Name = "lab_djbh";
            this.lab_djbh.Size = new System.Drawing.Size(229, 29);
            this.lab_djbh.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "销售单据编号：";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.dataGridView1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 85);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1085, 345);
            this.panel7.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridView1.ColumnHeadersHeight = 26;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.药品编码,
            this.销售退货订单明细ID,
            this.药品名称,
            this.生产企业,
            this.零售价,
            this.会员价,
            this.数量,
            this.金额,
            this.会员金额,
            this.药品批号,
            this.药品有效期,
            this.可退货数量,
            this.退货数量});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 25;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1085, 345);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint);
            // 
            // 药品编码
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Blue;
            this.药品编码.DefaultCellStyle = dataGridViewCellStyle8;
            this.药品编码.HeaderText = "药品编码";
            this.药品编码.Name = "药品编码";
            this.药品编码.ReadOnly = true;
            this.药品编码.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 销售退货订单明细ID
            // 
            this.销售退货订单明细ID.HeaderText = "销售退货订单明细ID";
            this.销售退货订单明细ID.Name = "销售退货订单明细ID";
            this.销售退货订单明细ID.ReadOnly = true;
            this.销售退货订单明细ID.Visible = false;
            // 
            // 药品名称
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.药品名称.DefaultCellStyle = dataGridViewCellStyle9;
            this.药品名称.HeaderText = "药品名称";
            this.药品名称.Name = "药品名称";
            this.药品名称.ReadOnly = true;
            this.药品名称.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 生产企业
            // 
            this.生产企业.DataPropertyName = "ManufacturingEnterprise";
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.Blue;
            this.生产企业.DefaultCellStyle = dataGridViewCellStyle10;
            this.生产企业.HeaderText = "生产企业";
            this.生产企业.Name = "生产企业";
            this.生产企业.ReadOnly = true;
            // 
            // 零售价
            // 
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.Blue;
            this.零售价.DefaultCellStyle = dataGridViewCellStyle11;
            this.零售价.HeaderText = "零售价";
            this.零售价.Name = "零售价";
            this.零售价.ReadOnly = true;
            this.零售价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 会员价
            // 
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.Blue;
            this.会员价.DefaultCellStyle = dataGridViewCellStyle12;
            this.会员价.HeaderText = "会员价";
            this.会员价.Name = "会员价";
            this.会员价.ReadOnly = true;
            this.会员价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 数量
            // 
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.Blue;
            this.数量.DefaultCellStyle = dataGridViewCellStyle13;
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.ReadOnly = true;
            this.数量.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 金额
            // 
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Red;
            this.金额.DefaultCellStyle = dataGridViewCellStyle14;
            this.金额.HeaderText = "金额";
            this.金额.Name = "金额";
            this.金额.ReadOnly = true;
            this.金额.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 会员金额
            // 
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Red;
            this.会员金额.DefaultCellStyle = dataGridViewCellStyle15;
            this.会员金额.HeaderText = "会员金额";
            this.会员金额.Name = "会员金额";
            this.会员金额.ReadOnly = true;
            // 
            // 药品批号
            // 
            this.药品批号.DataPropertyName = "BatchNumber";
            this.药品批号.HeaderText = "药品批号";
            this.药品批号.Name = "药品批号";
            this.药品批号.ReadOnly = true;
            // 
            // 药品有效期
            // 
            this.药品有效期.DataPropertyName = "DateExpiry";
            this.药品有效期.HeaderText = "药品有效期";
            this.药品有效期.Name = "药品有效期";
            this.药品有效期.ReadOnly = true;
            // 
            // 可退货数量
            // 
            this.可退货数量.HeaderText = "可退货数量";
            this.可退货数量.Name = "可退货数量";
            this.可退货数量.ReadOnly = true;
            // 
            // 退货数量
            // 
            this.退货数量.HeaderText = "退货数量";
            this.退货数量.Name = "退货数量";
            this.退货数量.ReadOnly = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Lab_HJcount,
            this.toolStripLabel2,
            this.toolStripLabel3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripLabel4});
            this.toolStrip1.Location = new System.Drawing.Point(0, 433);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1091, 25);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Lab_HJcount
            // 
            this.Lab_HJcount.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Lab_HJcount.Name = "Lab_HJcount";
            this.Lab_HJcount.Size = new System.Drawing.Size(39, 22);
            this.Lab_HJcount.Text = "共1笔";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Margin = new System.Windows.Forms.Padding(50, 1, 0, 2);
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(0, 22);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Margin = new System.Windows.Forms.Padding(50, 1, 0, 2);
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(80, 22);
            this.toolStripLabel3.Text = "制表人:libao ";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton1.Text = "保存";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton2.Text = "退货";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton4.Text = "导出";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(52, 22);
            this.toolStripButton5.Text = "打印";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Margin = new System.Windows.Forms.Padding(100, 1, 0, 2);
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(186, 22);
            this.toolStripLabel4.Text = "制表时间：2018-01-01 12:23:22";
            // 
            // 退货
            // 
            this.退货.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.全部退货,
            this.部分退货});
            this.退货.Name = "退货";
            this.退货.Size = new System.Drawing.Size(125, 48);
            // 
            // 全部退货
            // 
            this.全部退货.Name = "全部退货";
            this.全部退货.Size = new System.Drawing.Size(124, 22);
            this.全部退货.Text = "全部退货";
            this.全部退货.Click += new System.EventHandler(this.部分退货_Click);
            // 
            // 部分退货
            // 
            this.部分退货.Name = "部分退货";
            this.部分退货.Size = new System.Drawing.Size(124, 22);
            this.部分退货.Text = "部分退货";
            this.部分退货.Click += new System.EventHandler(this.部分退货_Click);
            // 
            // 药品退货管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1121, 561);
            this.ContextMenuStrip = this.退货;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "药品退货管理";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.采购入库管理_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.退货.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripAdd;
        private System.Windows.Forms.ToolStripDropDownButton toolStripCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgvSupplyList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lab_ddzt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lab_djbh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel Lab_HJcount;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LAB_id;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox lab_thbm;
        private System.Windows.Forms.ContextMenuStrip 退货;
        private System.Windows.Forms.ToolStripMenuItem 全部退货;
        private System.Windows.Forms.ToolStripMenuItem 部分退货;
        private System.Windows.Forms.Label lab_xszdID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 销售退货订单明细ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn 零售价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 会员金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品有效期;
        private System.Windows.Forms.DataGridViewTextBoxColumn 可退货数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退货数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退货订单编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退货订单状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 退货入库时间;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
    }
}