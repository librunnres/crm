﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.ToolHelper;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 采购统计 : Form
    {
        private readonly ISysUserRepository userRepository = new SysUserRepository();
        private readonly ISysSupplierRepository supplierRepository = new SysSupplierRepository();
        private readonly ISupplyOrderRepository supplyOrderRepository = new SupplyOrderRepository();

        public 采购统计()
        {
            InitializeComponent();
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        public string UserId { get; set; }
        public string UserName { get; set; }

        public 采购统计(string userId, string userName)
        {
            this.UserId = userId;
            this.UserName = userName;
            InitializeComponent();

            //获取所有的员工信息
            var userList = userRepository.GetList().ToList();
            userList.Insert(0, new SysUser() { UserName = "请选择业务员" });
            cboxUserId.DataSource = userList;
            cboxUserId.DisplayMember = nameof(SysUser.UserName);
            cboxUserId.ValueMember = nameof(SysUser.Id);

            var supplyList = supplierRepository.GetList().ToList();
            supplyList.Insert(0, new SysSupplier() { CompanyName = "请选择供应商" });
            cboxSupply.DataSource = supplyList;
            cboxSupply.DisplayMember = nameof(SysSupplier.CompanyName);
            cboxSupply.ValueMember = nameof(SysSupplier.Id);

            cboxUserId.SelectedValueChanged += btnQuery_Click;
            cboxSupply.SelectedValueChanged += btnQuery_Click;

            BindData();
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            StringBuilder where = new StringBuilder(" where 1=1 ");
            string userId = cboxUserId.SelectedValue.ToString();
            string supplyId = cboxSupply.SelectedValue.ToString();
            string beginDate = dtpStart.Text;
            string endDate = dtpEnd.Text;
            int uid = 0;
            if (!userId.IsNullOrEmpty())
            {
                if (int.TryParse(userId, out uid))
                {
                    if (uid > 0)
                    {
                        where.Append($" and CreateUserId = {uid}");
                    }
                }
            }

            int sid = 0;
            if (!supplyId.IsNullOrEmpty())
            {
                if (int.TryParse(supplyId, out sid))
                {
                    if (sid > 0)
                    {
                        where.Append($" and SupplierId = {sid}");
                    }
                }
            }
            if (!beginDate.IsNullOrEmpty())
            {
                where.Append($" and CONVERT(VARCHAR,CreateTime,23) >= '{DateTime.Parse(beginDate).ToString("yyyy-MM-dd")}'");
            }
            if (!endDate.IsNullOrEmpty())
            {
                where.Append($" and CONVERT(VARCHAR,CreateTime,23) <= '{DateTime.Parse(endDate).ToString("yyyy-MM-dd")}'");
            }

            string product = txtProduct.Text.Trim();
            if (!product.IsNullOrEmpty())
            {
                where.Append($" AND (ProductName LIKE '%{product}%' OR ProductCode LIKE '%{product}%')");
            }
            var list = supplyOrderRepository.GetSupplyReportList(where.ToString());

            dgvData.AutoGenerateColumns = false;
            dgvData.DataSource = list;
            var totalMoney = list.Sum(m => m.TotalMoney);
            lblTotalMoney.Text = totalMoney.ToString();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExport_Click(object sender, EventArgs e)
        {
            BindData();
            var path = ExcelHelperForCs.ExportToExcel(dgvData, "销售统计报表");
            MessageBox.Show($"导出成功，文件路径:{path}");
        }
    }
}