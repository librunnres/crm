﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Usido.CRM.IRepository;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 财务报表 : Form
    {
        public 财务报表()
        {
            InitializeComponent();
        }

        private readonly IStockProductBalanceRepository stockProductBalanceRepository = new StockProductBalanceRepository();

        private void 财务报表_Load(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            string begin = dtpStart.Text.Trim();
            string end = dtpEnd.Text.Trim();
            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            chart1.Series[0].Points.Clear();
            var list = stockProductBalanceRepository.GetReport(begin, end);
            chart1.Series[0].Palette = ChartColorPalette.Bright;
            chart1.Series[0].Points.AddXY("销总额", list[0]);

            chart1.Series[0].Points.AddXY("销退款金额", list[1]);
            chart1.Series[0].Points.AddXY("采总额", list[2]);
            chart1.Series[0].Points.AddXY("采退款金额", list[3]);
            chart1.Series[0].Points.AddXY("总利润", list[4]);
            chart1.Series[0].Label = "#VAL";   //设置显示X Y的值
            chart1.Series[0].ToolTip = "#VALX\r#VAL/￥";     //鼠标移动到对应点显示数值
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                chart1.Series[0].ChartType = SeriesChartType.Column;
            }
            else
            {
                chart1.Series[0].ChartType = SeriesChartType.Pie;
            }
        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            BindData();
        }
    }
}