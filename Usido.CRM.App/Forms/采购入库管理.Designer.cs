﻿namespace Usido.CRM.App.Forms
{
    partial class 采购入库管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(采购入库管理));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripAdd = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripBC = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripRK = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripCancel = new System.Windows.Forms.ToolStripDropDownButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvSupplyList = new System.Windows.Forms.DataGridView();
            this.订单编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.供应商 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.制单人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.订单状态 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.入库人 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.入库时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除此订单记录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Panel_RK = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.zje = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dgv2编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContextM_DelRK = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Lab_HJcount = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.药品编码 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.品名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.规格 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.生产企业 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单位 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.批号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.单价 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.数量 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金额 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.有效期至 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.Lab_ZDSS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Lab_ZDR = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cboxSupply = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Lab_DJZT = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Lab_DJBH = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCopy = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.Lab_OrderID = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_RK.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.ContextM_DelRK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Font = new System.Drawing.Font("Microsoft YaHei UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAdd,
            this.toolStripBC,
            this.toolStripRK,
            this.toolStripCancel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1142, 26);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripAdd
            // 
            this.toolStripAdd.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAdd.Image")));
            this.toolStripAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripAdd.Name = "toolStripAdd";
            this.toolStripAdd.Size = new System.Drawing.Size(66, 24);
            this.toolStripAdd.Text = "新增";
            this.toolStripAdd.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripAdd.Click += new System.EventHandler(this.toolStripAdd_Click);
            // 
            // toolStripBC
            // 
            this.toolStripBC.Enabled = false;
            this.toolStripBC.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripBC.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBC.Image")));
            this.toolStripBC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBC.Name = "toolStripBC";
            this.toolStripBC.Size = new System.Drawing.Size(66, 24);
            this.toolStripBC.Text = "保存";
            this.toolStripBC.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripBC.Click += new System.EventHandler(this.toolStripDelete_Click);
            // 
            // toolStripRK
            // 
            this.toolStripRK.Enabled = false;
            this.toolStripRK.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripRK.Image = ((System.Drawing.Image)(resources.GetObject("toolStripRK.Image")));
            this.toolStripRK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripRK.Name = "toolStripRK";
            this.toolStripRK.Size = new System.Drawing.Size(66, 24);
            this.toolStripRK.Text = "入库";
            this.toolStripRK.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripRK.Click += new System.EventHandler(this.toolStripEdit_Click);
            // 
            // toolStripCancel
            // 
            this.toolStripCancel.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripCancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripCancel.Image")));
            this.toolStripCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripCancel.Name = "toolStripCancel";
            this.toolStripCancel.Size = new System.Drawing.Size(66, 24);
            this.toolStripCancel.Text = "取消";
            this.toolStripCancel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.toolStripCancel.Click += new System.EventHandler(this.toolStripCancel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.30827F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 509F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1142, 511);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1132, 501);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1132, 501);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Wheat;
            this.tabPage1.Controls.Add(this.dgvSupplyList);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1124, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "列表";
            // 
            // dgvSupplyList
            // 
            this.dgvSupplyList.AllowUserToResizeColumns = false;
            this.dgvSupplyList.AllowUserToResizeRows = false;
            this.dgvSupplyList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSupplyList.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dgvSupplyList.CausesValidation = false;
            this.dgvSupplyList.ColumnHeadersHeight = 26;
            this.dgvSupplyList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvSupplyList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.订单编号,
            this.供应商,
            this.制单时间,
            this.制单人,
            this.订单状态,
            this.入库人,
            this.入库时间,
            this.Id});
            this.dgvSupplyList.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvSupplyList.Location = new System.Drawing.Point(3, 0);
            this.dgvSupplyList.Name = "dgvSupplyList";
            this.dgvSupplyList.RowHeadersWidth = 25;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgvSupplyList.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSupplyList.RowTemplate.Height = 23;
            this.dgvSupplyList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSupplyList.Size = new System.Drawing.Size(1115, 465);
            this.dgvSupplyList.TabIndex = 3;
            this.dgvSupplyList.DoubleClick += new System.EventHandler(this.dgvSupplyList_DoubleClick);
            // 
            // 订单编号
            // 
            this.订单编号.DataPropertyName = "OrderCode";
            this.订单编号.HeaderText = "订单编号";
            this.订单编号.Name = "订单编号";
            // 
            // 供应商
            // 
            this.供应商.DataPropertyName = "SupplierName";
            this.供应商.HeaderText = "供应商";
            this.供应商.Name = "供应商";
            this.供应商.ReadOnly = true;
            // 
            // 制单时间
            // 
            this.制单时间.DataPropertyName = "CreateTime";
            this.制单时间.HeaderText = "制单时间";
            this.制单时间.Name = "制单时间";
            this.制单时间.ReadOnly = true;
            // 
            // 制单人
            // 
            this.制单人.DataPropertyName = "CreateUserName";
            this.制单人.HeaderText = "制单人";
            this.制单人.Name = "制单人";
            // 
            // 订单状态
            // 
            this.订单状态.DataPropertyName = "StateText";
            this.订单状态.HeaderText = "订单状态";
            this.订单状态.Name = "订单状态";
            // 
            // 入库人
            // 
            this.入库人.DataPropertyName = "PutUserName";
            this.入库人.HeaderText = "入库人";
            this.入库人.Name = "入库人";
            // 
            // 入库时间
            // 
            this.入库时间.DataPropertyName = "PutTime";
            this.入库时间.HeaderText = "入库时间";
            this.入库时间.Name = "入库时间";
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除此订单记录ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 26);
            // 
            // 删除此订单记录ToolStripMenuItem
            // 
            this.删除此订单记录ToolStripMenuItem.Name = "删除此订单记录ToolStripMenuItem";
            this.删除此订单记录ToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.删除此订单记录ToolStripMenuItem.Text = "删除此订单记录";
            this.删除此订单记录ToolStripMenuItem.Click += new System.EventHandler(this.删除此订单记录ToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1124, 468);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "明细";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Panel_RK);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1124, 468);
            this.panel3.TabIndex = 0;
            // 
            // Panel_RK
            // 
            this.Panel_RK.BackColor = System.Drawing.Color.SkyBlue;
            this.Panel_RK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_RK.Controls.Add(this.tableLayoutPanel2);
            this.Panel_RK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_RK.Location = new System.Drawing.Point(0, 0);
            this.Panel_RK.Name = "Panel_RK";
            this.Panel_RK.Size = new System.Drawing.Size(1124, 468);
            this.Panel_RK.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel6, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1122, 466);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.zje);
            this.panel6.Controls.Add(this.panel1);
            this.panel6.Controls.Add(this.Lab_HJcount);
            this.panel6.Controls.Add(this.dataGridView1);
            this.panel6.Controls.Add(this.txtRemark);
            this.panel6.Controls.Add(this.Lab_ZDSS);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.Lab_ZDR);
            this.panel6.Location = new System.Drawing.Point(3, 78);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1116, 377);
            this.panel6.TabIndex = 2;
            // 
            // zje
            // 
            this.zje.AutoSize = true;
            this.zje.Location = new System.Drawing.Point(588, 357);
            this.zje.Name = "zje";
            this.zje.Size = new System.Drawing.Size(65, 20);
            this.zje.TabIndex = 10;
            this.zje.Text = "总金额：";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(378, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(574, 235);
            this.panel1.TabIndex = 8;
            this.panel1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(282, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "有效期至：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Location = new System.Drawing.Point(364, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 1;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.groupBox1.Controls.Add(this.dataGridView2);
            this.groupBox1.Location = new System.Drawing.Point(0, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(570, 204);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "药品列表";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.Color.Silver;
            this.dataGridView2.ColumnHeadersHeight = 26;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv2编号,
            this.dgv2品名,
            this.dgv2单位,
            this.dgv2规格,
            this.dgv2生产企业});
            this.dataGridView2.ContextMenuStrip = this.ContextM_DelRK;
            this.dataGridView2.Location = new System.Drawing.Point(3, 22);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 25;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView2.RowTemplate.Height = 23;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(564, 179);
            this.dataGridView2.TabIndex = 2;
            this.dataGridView2.DoubleClick += new System.EventHandler(this.dataGridView2_DoubleClick);
            this.dataGridView2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView2_KeyDown);
            // 
            // dgv2编号
            // 
            this.dgv2编号.DataPropertyName = "Id";
            this.dgv2编号.HeaderText = "编号";
            this.dgv2编号.Name = "dgv2编号";
            this.dgv2编号.ReadOnly = true;
            this.dgv2编号.Width = 80;
            // 
            // dgv2品名
            // 
            this.dgv2品名.DataPropertyName = "ProductName";
            this.dgv2品名.HeaderText = "品名";
            this.dgv2品名.Name = "dgv2品名";
            this.dgv2品名.ReadOnly = true;
            this.dgv2品名.Width = 140;
            // 
            // dgv2单位
            // 
            this.dgv2单位.DataPropertyName = "MarketingUnit";
            this.dgv2单位.HeaderText = "单位";
            this.dgv2单位.Name = "dgv2单位";
            this.dgv2单位.ReadOnly = true;
            // 
            // dgv2规格
            // 
            this.dgv2规格.DataPropertyName = "Specification";
            this.dgv2规格.HeaderText = "规格";
            this.dgv2规格.Name = "dgv2规格";
            this.dgv2规格.ReadOnly = true;
            this.dgv2规格.Width = 71;
            // 
            // dgv2生产企业
            // 
            this.dgv2生产企业.DataPropertyName = "ManufacturingEnterprise";
            this.dgv2生产企业.HeaderText = "生产企业";
            this.dgv2生产企业.Name = "dgv2生产企业";
            this.dgv2生产企业.ReadOnly = true;
            this.dgv2生产企业.Width = 140;
            // 
            // ContextM_DelRK
            // 
            this.ContextM_DelRK.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.ContextM_DelRK.Name = "contextMenuStrip1";
            this.ContextM_DelRK.Size = new System.Drawing.Size(125, 26);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.删除ToolStripMenuItem.Text = "删除此条";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // Lab_HJcount
            // 
            this.Lab_HJcount.AutoSize = true;
            this.Lab_HJcount.Location = new System.Drawing.Point(421, 357);
            this.Lab_HJcount.Name = "Lab_HJcount";
            this.Lab_HJcount.Size = new System.Drawing.Size(51, 20);
            this.Lab_HJcount.TabIndex = 9;
            this.Lab_HJcount.Text = "合计：";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            this.dataGridView1.ColumnHeadersHeight = 26;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.药品编码,
            this.品名,
            this.规格,
            this.生产企业,
            this.单位,
            this.批号,
            this.单价,
            this.数量,
            this.金额,
            this.有效期至});
            this.dataGridView1.ContextMenuStrip = this.ContextM_DelRK;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridView1.RowHeadersWidth = 25;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1110, 336);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dataGridView1_RowPostPaint_1);
            this.dataGridView1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dataGridView1_Scroll);
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // 药品编码
            // 
            this.药品编码.DataPropertyName = "ProductId";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.HotTrack;
            this.药品编码.DefaultCellStyle = dataGridViewCellStyle3;
            this.药品编码.HeaderText = "药品编码";
            this.药品编码.Name = "药品编码";
            this.药品编码.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 品名
            // 
            this.品名.DataPropertyName = "ProductName";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.品名.DefaultCellStyle = dataGridViewCellStyle4;
            this.品名.HeaderText = "品名";
            this.品名.Name = "品名";
            this.品名.ReadOnly = true;
            this.品名.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 规格
            // 
            this.规格.DataPropertyName = "Specification";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.规格.DefaultCellStyle = dataGridViewCellStyle5;
            this.规格.HeaderText = "规格";
            this.规格.Name = "规格";
            this.规格.ReadOnly = true;
            this.规格.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 生产企业
            // 
            this.生产企业.DataPropertyName = "ManufacturingEnterprise";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.生产企业.DefaultCellStyle = dataGridViewCellStyle6;
            this.生产企业.HeaderText = "生产企业";
            this.生产企业.Name = "生产企业";
            this.生产企业.ReadOnly = true;
            this.生产企业.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 单位
            // 
            this.单位.DataPropertyName = "MarketingUnit";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.单位.DefaultCellStyle = dataGridViewCellStyle7;
            this.单位.HeaderText = "单位";
            this.单位.Name = "单位";
            this.单位.ReadOnly = true;
            this.单位.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 批号
            // 
            this.批号.DataPropertyName = "BatchNumber";
            this.批号.HeaderText = "批号";
            this.批号.Name = "批号";
            this.批号.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 单价
            // 
            this.单价.DataPropertyName = "Price";
            this.单价.HeaderText = "单价";
            this.单价.Name = "单价";
            this.单价.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 数量
            // 
            this.数量.DataPropertyName = "Quantity";
            this.数量.HeaderText = "数量";
            this.数量.Name = "数量";
            this.数量.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 金额
            // 
            this.金额.DataPropertyName = "TotalMoney";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Red;
            this.金额.DefaultCellStyle = dataGridViewCellStyle8;
            this.金额.HeaderText = "金额";
            this.金额.Name = "金额";
            this.金额.ReadOnly = true;
            this.金额.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // 有效期至
            // 
            this.有效期至.DataPropertyName = "DateExpiry";
            this.有效期至.HeaderText = "有效期至";
            this.有效期至.Name = "有效期至";
            this.有效期至.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // txtRemark
            // 
            this.txtRemark.Location = new System.Drawing.Point(168, 354);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(191, 26);
            this.txtRemark.TabIndex = 8;
            // 
            // Lab_ZDSS
            // 
            this.Lab_ZDSS.AutoSize = true;
            this.Lab_ZDSS.ForeColor = System.Drawing.Color.Black;
            this.Lab_ZDSS.Location = new System.Drawing.Point(794, 355);
            this.Lab_ZDSS.Name = "Lab_ZDSS";
            this.Lab_ZDSS.Size = new System.Drawing.Size(157, 20);
            this.Lab_ZDSS.TabIndex = 5;
            this.Lab_ZDSS.Text = "2019年1月4日 16:44:22";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(720, 355);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "制单时间：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 356);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "备注：";
            // 
            // Lab_ZDR
            // 
            this.Lab_ZDR.AutoSize = true;
            this.Lab_ZDR.Location = new System.Drawing.Point(5, 357);
            this.Lab_ZDR.Name = "Lab_ZDR";
            this.Lab_ZDR.Size = new System.Drawing.Size(65, 20);
            this.Lab_ZDR.TabIndex = 6;
            this.Lab_ZDR.Text = "制单人：";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.textBox1);
            this.panel5.Controls.Add(this.cboxSupply);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.Lab_DJZT);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.Lab_DJBH);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 43);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1116, 29);
            this.panel5.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(634, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(158, 26);
            this.textBox1.TabIndex = 8;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // cboxSupply
            // 
            this.cboxSupply.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSupply.FormattingEnabled = true;
            this.cboxSupply.Location = new System.Drawing.Point(798, 0);
            this.cboxSupply.Name = "cboxSupply";
            this.cboxSupply.Size = new System.Drawing.Size(149, 28);
            this.cboxSupply.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(575, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "供应商：";
            // 
            // Lab_DJZT
            // 
            this.Lab_DJZT.AutoSize = true;
            this.Lab_DJZT.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.Lab_DJZT.Location = new System.Drawing.Point(367, 5);
            this.Lab_DJZT.Name = "Lab_DJZT";
            this.Lab_DJZT.Size = new System.Drawing.Size(37, 20);
            this.Lab_DJZT.TabIndex = 3;
            this.Lab_DJZT.Text = "制单";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(292, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "单据状态：";
            // 
            // Lab_DJBH
            // 
            this.Lab_DJBH.AutoSize = true;
            this.Lab_DJBH.Location = new System.Drawing.Point(81, 5);
            this.Lab_DJBH.Name = "Lab_DJBH";
            this.Lab_DJBH.Size = new System.Drawing.Size(0, 20);
            this.Lab_DJBH.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "单据编号：";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCopy);
            this.panel4.Controls.Add(this.linkLabel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1116, 34);
            this.panel4.TabIndex = 0;
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(3, 3);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(106, 31);
            this.btnCopy.TabIndex = 1;
            this.btnCopy.Text = "复制订单号";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.linkLabel1.Location = new System.Drawing.Point(366, 2);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(216, 29);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "药品采购入库单";
            // 
            // Lab_OrderID
            // 
            this.Lab_OrderID.AutoSize = true;
            this.Lab_OrderID.Location = new System.Drawing.Point(321, 0);
            this.Lab_OrderID.Name = "Lab_OrderID";
            this.Lab_OrderID.Size = new System.Drawing.Size(0, 12);
            this.Lab_OrderID.TabIndex = 1;
            this.Lab_OrderID.Visible = false;
            // 
            // 采购入库管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 537);
            this.Controls.Add(this.Lab_OrderID);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.Name = "采购入库管理";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "采购入库管理";
            this.Load += new System.EventHandler(this.采购入库管理_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSupplyList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.Panel_RK.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ContextM_DelRK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripAdd;
        private System.Windows.Forms.ToolStripDropDownButton toolStripBC;
        private System.Windows.Forms.ToolStripDropDownButton toolStripRK;
        private System.Windows.Forms.ToolStripDropDownButton toolStripCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ContextMenuStrip ContextM_DelRK;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel Panel_RK;
        private System.Windows.Forms.DataGridView dgvSupplyList;
        private System.Windows.Forms.Label Lab_OrderID;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 删除此订单记录ToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label zje;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2生产企业;
        private System.Windows.Forms.Label Lab_HJcount;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 药品编码;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名;
        private System.Windows.Forms.DataGridViewTextBoxColumn 规格;
        private System.Windows.Forms.DataGridViewTextBoxColumn 生产企业;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单位;
        private System.Windows.Forms.DataGridViewTextBoxColumn 批号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 单价;
        private System.Windows.Forms.DataGridViewTextBoxColumn 数量;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金额;
        private System.Windows.Forms.DataGridViewTextBoxColumn 有效期至;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label Lab_ZDSS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Lab_ZDR;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cboxSupply;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Lab_DJZT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Lab_DJBH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 供应商;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn 制单人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 订单状态;
        private System.Windows.Forms.DataGridViewTextBoxColumn 入库人;
        private System.Windows.Forms.DataGridViewTextBoxColumn 入库时间;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
    }
}