﻿using System;
using System.Windows.Forms;

namespace Usido.CRM.App.Forms
{
    public partial class 设置价格 : Form
    {
        public 设置价格()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (decimal.Parse(textBox1.Text.Trim()) == 0 || textBox1.Text.Trim().Length == 0)
                {
                    MessageBox.Show("数量必须大于0!");
                    this.DialogResult = DialogResult.No;
                    this.Tag = 0;
                    return;
                }
                this.Tag = textBox1.Text.Trim();
            }
            catch (Exception)
            {
                MessageBox.Show("数量格式有误!");
                this.DialogResult = DialogResult.No;
                return;
            }
            this.DialogResult = DialogResult.OK;
        }
        
        private void 设置价格_Load(object sender, EventArgs e)
        {
            this.textBox1.Select();
        }

        private void 设置价格_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
            }
            else
            {
                this.DialogResult = DialogResult.No;
                this.Tag = 0;
            }
        }

        private void textBox1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if (decimal.Parse(textBox1.Text.Trim()) == 0 || textBox1.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("价格必须大于0!");
                        this.DialogResult = DialogResult.No;
                        this.Tag = 0;
                        return;
                    }
                    this.Tag = textBox1.Text.Trim();
                }
                catch (Exception)
                {
                    MessageBox.Show("数量格式有误!");
                    this.DialogResult = DialogResult.No;
                    return;
                }
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}