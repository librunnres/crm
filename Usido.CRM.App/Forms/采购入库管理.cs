﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 采购入库管理 : Form
    {
        private readonly ILog logger = LogManager.GetLogger(nameof(采购入库管理));
        private readonly ISysProductRepository productRepository = new SysProductRepository();
        private readonly ISupplyInfoRepository supplyInfoRepository = new SupplyInfoRepository();
        private readonly ISupplyOrderRepository supplyOrderRepository = new SupplyOrderRepository();
        private readonly ISysSupplierRepository supplierRepository = new SysSupplierRepository();

        public 采购入库管理()
        {
            InitializeComponent();

            dataGridView1.ScrollBars = ScrollBars.Vertical;
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        public 采购入库管理(string userId, string userName)
        {
            InitializeComponent();
            this.userId = userId;
            this.userName = userName;
            Lab_ZDR.Text = this.userName;
            Lab_ZDSS.Text = DateTime.Now.ToString("yyyy年MM月dd日  hh:mm:ss");
        }

        private void 采购入库管理_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //加载采购订单信息
            BindData();
            tableLayoutPanel2.Enabled = false;
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = -1;
            try
            {
                index = dataGridView1.CurrentCell.RowIndex;
            }
            catch (Exception)
            {
                MessageBox.Show("您还未选中有效行！");
            }

            DialogResult RSS = MessageBox.Show(this, "确定要删除选中行数据码？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (RSS == DialogResult.Yes)
            {
                try
                {
                    dataGridView1.Rows.RemoveAt(index);
                    MessageBox.Show("成功删除选中行数据！");
                }
                catch (Exception)
                {
                    MessageBox.Show("您还未选中有效行！");
                }
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripEdit_Click(object sender, EventArgs e)
        {
            if (supplyOrderRepository.InStore(Lab_OrderID.Text.ToInt(), this.userId.ToInt()))
            {
                MessageBox.Show("入库成功！此订单已无法修改");
                BindData();
                toolStripBC.Enabled = false;
                toolStripRK.Enabled = false;
                toolStripAdd.Enabled = true;
            }
            else
            {
                MessageBox.Show("入库失败，请稍后重试！");
            }
        }

        /// <summary>
        /// 保存/入库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.Focus();
                if (cboxSupply.SelectedValue == null)
                {
                    MessageBox.Show("请选择供应商");
                    return;
                }
                var supplyId = cboxSupply.SelectedValue.ToString();
                if (supplyId.IsNullOrEmpty())
                {
                    MessageBox.Show("请选择供应商");
                    return;
                }
                int orderIdNew = 0;
                if (!Lab_OrderID.Text.Trim().IsNullOrEmpty())
                {
                    orderIdNew = Lab_OrderID.Text.Trim().ToInt();
                }
                SupplyOrder order = new SupplyOrder()
                {
                    CreateTime = DateTime.Now,
                    CreateUserId = this.userId.ToInt(),
                    Remark = txtRemark.Text.Trim(),
                    State = 0,
                    SupplierId = supplyId.ToInt()
                };

                List<SupplyInfo> infoList = new List<SupplyInfo>();

                for (int i = 0; i < Count; i++)
                {
                    var productId = dataGridView1.Rows[i].Cells["药品编码"].Value.ToString();
                    if (productId.IsNullOrEmpty())
                    {
                        continue;
                    }

                    string batchNumber;
                    if (dataGridView1.Rows[i].Cells["批号"].Value == null)
                    {
                        batchNumber = "";
                    }
                    else
                    {
                        batchNumber = dataGridView1.Rows[i].Cells["批号"].Value.ToString();
                    }

                    var price = dataGridView1.Rows[i].Cells["单价"].Value.ToString();
                    decimal priceNew = 0;
                    if (!decimal.TryParse(price, out priceNew))
                    {
                        MessageBox.Show($"第{i + 1}行单价格式不正确");
                        return;
                    }
                    if (priceNew < 0)
                    {
                        MessageBox.Show($"第{i + 1}行单价格式不正确");
                        return;
                    }
                    var quantity = dataGridView1.Rows[i].Cells["数量"].Value.ToString();
                    decimal quantityNew = 0;
                    if (!decimal.TryParse(quantity, out quantityNew))
                    {
                        MessageBox.Show($"第{i + 1}行数量格式不正确");
                        return;
                    }
                    if (quantityNew <= 0)
                    {
                        MessageBox.Show($"第{i + 1}行数量必须大于0");
                        return;
                    }
                    string dateExpiry = "";
                    DateTime dateExpiryNew;
                    if (dataGridView1.Rows[i].Cells["有效期至"].Value != null)
                    {
                        dateExpiry = dataGridView1.Rows[i].Cells["有效期至"].Value.ToString();
                    }

                    if (DateTime.TryParse(dateExpiry, out dateExpiryNew))
                    {
                    }
                    var info = new SupplyInfo()
                    {
                        BatchNumber = batchNumber,
                        Price = priceNew,
                        ProductId = productId.ToInt(),
                        Quantity = quantityNew,
                        OrderId = orderIdNew
                    };
                    if (DateTime.MinValue != dateExpiryNew)
                    {
                        info.DateExpiry = dateExpiryNew;
                    }

                    if (infoList.Any(m => m.ProductId == info.ProductId && m.BatchNumber == info.BatchNumber && m.DateExpiry == info.DateExpiry))
                    {
                        MessageBox.Show($"第{i + 1}行数据重复，请检查");
                        return;
                    }
                    infoList.Add(info);
                }

                if (orderIdNew <= 0)
                {
                    //add
                    var orderId = supplyOrderRepository.AddSupply(order, infoList);
                    if (orderId > 0)
                    {
                        this.Lab_OrderID.Text = orderId.ToString();
                        MessageBox.Show("保存成功");
                        toolStripRK.Enabled = true;
                        BindData();
                        BindSupplyInfo(orderId);
                    }
                    else
                    {
                        MessageBox.Show("保存失败，请稍后重试");
                    }
                }
                else
                {
                    order.Id = orderIdNew;
                    if (supplyOrderRepository.UpdateSupply(order, infoList))
                    {
                        MessageBox.Show("更新成功");
                        toolStripRK.Enabled = true;
                        BindData();
                        BindSupplyInfo(orderIdNew);
                    }
                    else
                    {
                        MessageBox.Show("更新失败，请稍后重试");
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("请让所选中的单元格失去焦点之后，再点击保存按钮");
            }
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripAdd_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows.Clear();
            }
            toolStripAdd.Enabled = false;
            toolStripBC.Enabled = true;
            toolStripRK.Enabled = false;
            tableLayoutPanel2.Enabled = true;
            this.Lab_OrderID.Text = "";
            this.textBox1.Text = "";
            this.Lab_DJZT.Text = "制单";
            cboxSupply.DataSource = new List<SysSupplier>();
            txtRemark.Text = "";
            Lab_DJBH.Text = "";
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //检测是被表示的控件还是DataGridViewTextBoxEditingControl
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridView dgv = (DataGridView)sender;
                //取得被表示的控件
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
                //事件处理器删除
                tb.TextChanged -= Tb_TextChanged1;
                tb.TextChanged -= Tb_TextChanged;
                tb.TextChanged -= Tb_TextChanged2;
                //检测对应列
                if (dgv.CurrentCell.OwningColumn.Name == "药品编码")
                {
                    // KeyPress事件处理器追加
                    tb.TextChanged += Tb_TextChanged;
                }
                if (dgv.CurrentCell.OwningColumn.Name == "数量")
                {
                    // KeyPress事件处理器追加
                    tb.TextChanged += Tb_TextChanged1; ;
                }
                if (dgv.CurrentCell.OwningColumn.Name == "单价")
                {
                    // KeyPress事件处理器追加
                    tb.TextChanged += Tb_TextChanged2; ; ;
                }
                if (dgv.CurrentCell.OwningColumn.Name == "有效期至")
                {
                    tb.Click += Tb_Click;
                }
            }
        }

        private void Tb_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell.OwningColumn.Name == "有效期至")
            {
                panel1.Visible = true;
            }
        }

        private void Tb_TextChanged2(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentCell.OwningColumn.Name == "有效期至")
                {
                    return;
                }
                TextBox TB = (TextBox)sender;
                double 单价;
                if (TB.Text.Trim() == "")
                {
                    单价 = 0;
                }
                else
                {
                    单价 = double.Parse(TB.Text.Trim());
                }
                double 数量 = double.Parse(dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["数量"].Value.ToString());
                dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["金额"].Value = "￥" + (单价 * 数量).ToString();
            }
            catch (Exception ex)
            {
            }
        }

        //计算总金额
        private void Tb_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentCell.OwningColumn.Name == "有效期至")
                {
                    return;
                }
                TextBox TB = (TextBox)sender;
                double 数量;
                if (TB.Text.Trim() == "")
                {
                    数量 = 0;
                }
                else
                {
                    数量 = double.Parse(TB.Text.Trim());
                }
                double 单价 = double.Parse(dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["单价"].Value.ToString());
                dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["金额"].Value = "￥" + (单价 * 数量).ToString();
            }
            catch (Exception ex)
            {
            }
        }

        //检索药品
        private int index = -1;

        private void Tb_TextChanged(object sender, EventArgs e)
        {
            TextBox TB = (TextBox)sender;
            if (TB.Text.Trim() == "")
            {
                panel1.Visible = false;
            }
            else
            {
                index = dataGridView1.CurrentCell.RowIndex;
                string code = TB.Text.Trim();
                string sql = $" where (ProductName LIKE '%{code}%' OR ProductCode LIKE '%{code}%') AND State = 1";
                var productList = productRepository.GetList(sql, new { });
                if (productList.ToList().Count > 0)
                {
                    dataGridView2.AutoGenerateColumns = false;
                    dataGridView2.DataSource = productList;
                    dataGridView2.ClearSelection();
                    panel1.Visible = true;
                }
                else
                {
                    panel1.Visible = false;
                }
            }
        }

        /// <summary>
        /// 查询结果双击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string Id = dataGridView2.SelectedRows[0].Cells["dgv2编号"].Value.ToString();
                string name = dataGridView2.SelectedRows[0].Cells["dgv2品名"].Value.ToString();
                string gg = dataGridView2.SelectedRows[0].Cells["dgv2规格"].Value.ToString();
                string scqy = dataGridView2.SelectedRows[0].Cells["dgv2生产企业"].Value.ToString();
                string dw = dataGridView2.SelectedRows[0].Cells["dgv2单位"].Value.ToString();
                //int row = dataGridView1.CurrentRow.Index;
                dataGridView1.Rows[index].Cells["药品编码"].Value = Id;
                dataGridView1.Rows[index].Cells["品名"].Value = name;
                dataGridView1.Rows[index].Cells["规格"].Value = gg;
                dataGridView1.Rows[index].Cells["生产企业"].Value = scqy;
                dataGridView1.Rows[index].Cells["单位"].Value = dw;
                this.panel1.Visible = false;
                dataGridView1.Rows[index].Cells["药品编码"].ReadOnly = true;
                dataGridView1.Focus();
            }
            catch (Exception E)
            {
            }
        }

        private bool IsoK = true;

        //新增一行
        private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dataGridView1.ScrollBars = ScrollBars.Vertical;
                IsoK = true;
                int Count = dataGridView1.Rows.Count;
                if (Count > 0)
                {
                    for (int i = 0; i < dataGridView1.Rows[Count - 1].Cells.Count; i++)
                    {
                        if (dataGridView1.Rows[Count - 1].Cells[i].Value == null || dataGridView1.Rows[Count - 1].Cells[i].Value.ToString() == "")
                        {
                            if (i != 5 && i != 9)
                            {
                                IsoK = false;
                            }
                        }
                    }
                    if (IsoK)
                    {
                        dataGridView1.Rows.Add();
                        dataGridView1.CurrentCell = this.dataGridView1.Rows[Count].Cells[0];
                        //dataGridView1.Rows[Count].Cells[0].Selected = true;
                    }
                }
                else
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[Count].Cells[0].Selected = true;
                }
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.Rows[index].Cells["有效期至"].Value = dateTimePicker1.Value.ToString("yyyy-MM-dd");
            this.panel1.Visible = false;
            //dataGridView1.Rows[index].Cells["有效期至"].ReadOnly = true;
            dataGridView1.Focus();
        }

        //药品检索结果
        private void dataGridView2_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    string Id = dataGridView2.SelectedRows[0].Cells["dgv2编号"].Value.ToString();
                    string name = dataGridView2.SelectedRows[0].Cells["dgv2品名"].Value.ToString();
                    string gg = dataGridView2.SelectedRows[0].Cells["dgv2规格"].Value.ToString();
                    string scqy = dataGridView2.SelectedRows[0].Cells["dgv2生产企业"].Value.ToString();
                    string dw = dataGridView2.SelectedRows[0].Cells["dgv2单位"].Value.ToString();
                    //int row = dataGridView1.CurrentRow.Index;
                    dataGridView1.Rows[index].Cells["药品编码"].Value = Id;
                    dataGridView1.Rows[index].Cells["品名"].Value = name;
                    dataGridView1.Rows[index].Cells["规格"].Value = gg;
                    dataGridView1.Rows[index].Cells["生产企业"].Value = scqy;
                    dataGridView1.Rows[index].Cells["单位"].Value = dw;
                    this.panel1.Visible = false;
                    dataGridView1.Rows[index].Cells["药品编码"].ReadOnly = true;
                    dataGridView1.Focus();
                }
            }
            catch (Exception E)
            {
            }
        }

        private double Money = 0.00;
        private int Count = 0;
        private string userId;
        private string userName;

        private void dataGridView1_RowPostPaint_1(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Money = 0.00;
            Count = 0;
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
          e.RowBounds.Location.Y,
          dataGridView1.RowHeadersWidth - 4,
          e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   dataGridView1.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            try
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells["金额"].Value != null)
                    {
                        Count++;
                        double num = double.Parse(dataGridView1.Rows[i].Cells["金额"].Value.ToString().Replace("￥", ""));
                        Money += num;
                    }
                }
                Lab_HJcount.Text = "合计：" + Count.ToString() + "笔 ";
                zje.Text = "总金额:￥" + Money.ToString();
            }
            catch (Exception ex)
            {
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string code = textBox1.Text;
            string sql = $" WHERE (CompanyName LIKE '%{code}%' OR CompanyCode LIKE '%{code}%') AND State = 1";
            var supplyLit = supplierRepository.GetList(sql, new { });
            cboxSupply.DataSource = supplyLit;
            if (supplyLit.ToList().Count > 0)
            {
                cboxSupply.DroppedDown = true;
            }
            cboxSupply.ValueMember = nameof(SysSupplier.Id);
            cboxSupply.DisplayMember = nameof(SysSupplier.CompanyName);
        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void dgvSupplyList_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string id = dgvSupplyList.SelectedRows[0].Cells["Id"].Value.ToString();
                BindSupplyInfo(id.ToInt());

                this.tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                MessageBox.Show("请选择采购订单!");
            }
        }

        /// <summary>
        /// 绑定订单详细信息
        /// </summary>
        /// <param name="orderId"></param>
        private void BindSupplyInfo(int orderId)
        {
            dataGridView1.Rows.Clear();
            //绑定主表
            var model = supplyOrderRepository.GetModel(orderId);
            Lab_DJBH.Text = model.OrderCode;
            txtRemark.Text = model.Remark;
            Lab_ZDR.Text = model.CreateUserName;
            Lab_ZDSS.Text = model.CreateTime.ToString("yyyy年MM月dd日 HH:mm:ss");
            textBox1.Text = model.SupplierName;
            cboxSupply.DroppedDown = false;
            //绑定明细表
            var list = supplyOrderRepository.GetSupplyInfoList(orderId);
            dataGridView1.AutoGenerateColumns = false;

            this.Lab_OrderID.Text = model.Id.ToString();
            //dataGridView1.DataSource = list;

            for (int i = 0; i < list.Count; i++)
            {
                int RowsIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[RowsIndex].Cells["药品编码"].Value = list[i].ProductId;
                dataGridView1.Rows[RowsIndex].Cells["品名"].Value = list[i].ProductName;
                dataGridView1.Rows[RowsIndex].Cells["规格"].Value = list[i].Specification;
                dataGridView1.Rows[RowsIndex].Cells["生产企业"].Value = list[i].ManufacturingEnterprise;
                dataGridView1.Rows[RowsIndex].Cells["单位"].Value = list[i].MarketingUnit;
                dataGridView1.Rows[RowsIndex].Cells["批号"].Value = list[i].BatchNumber;
                dataGridView1.Rows[RowsIndex].Cells["单价"].Value = list[i].Price;
                dataGridView1.Rows[RowsIndex].Cells["数量"].Value = list[i].Quantity;
                dataGridView1.Rows[RowsIndex].Cells["金额"].Value = list[i].TotalMoney;
                dataGridView1.Rows[RowsIndex].Cells["有效期至"].Value = list[i].DateExpiry;
            }
            tableLayoutPanel2.Enabled = true;
            if (model.State == 0)
            {
                Lab_DJZT.Text = "已保存";
                //dataGridView1.Columns["药品编码"].ReadOnly = true;
                toolStripRK.Enabled = true;
                toolStripBC.Enabled = true;
            }
            else if (model.State == 1)
            {
                Lab_DJZT.Text = "已入库";
                toolStripRK.Enabled = false;
                toolStripBC.Enabled = false;
            }
            else
            {
                toolStripRK.Enabled = false;
                toolStripBC.Enabled = true;
                Lab_DJZT.Text = "制单";
                //dataGridView1.Columns["药品编码"].ReadOnly = true;
            }
        }

        private void 删除此订单记录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string orderId = dgvSupplyList.SelectedRows[0].Cells["Id"].Value.ToString();
                string OrderState = dgvSupplyList.SelectedRows[0].Cells["订单状态"].Value.ToString();
                if (OrderState == "已保存")
                {
                    DialogResult RSS = MessageBox.Show(this, "确定要删除选中行数据吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (RSS == DialogResult.Yes)
                    {
                        if (supplyOrderRepository.DeleteSupply(orderId.ToInt()))
                        {
                            MessageBox.Show("删除成功");
                            BindData();
                        }
                        else
                        {
                            MessageBox.Show("删除失败");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("该订单已入库！无法删除！");
                }
            }
            catch (Exception)
            {
            }
        }

        private void BindData()
        {
            var list = supplyOrderRepository.GetSupplyList();
            dgvSupplyList.AutoGenerateColumns = false;
            list.ForEach(m => m.StateText = m.State == 0 ? "已保存" : "已完成");
            dgvSupplyList.DataSource = list;
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (Lab_DJBH.Text.Trim().IsNullOrEmpty())
            {
                MessageBox.Show("暂无订单号");
            }
            else
            {
                Clipboard.SetDataObject(Lab_DJBH.Text.Trim());
                MessageBox.Show("订单号复制成功");
            }
        }
        int VerticalScrollIndex = 0;

        int HorizontalOffset = 0;

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
        }
    }
}