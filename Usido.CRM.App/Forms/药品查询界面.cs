﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 药品查询界面 : Form
    {
        private readonly ISysProductRepository ProductReposioty = new SysProductRepository();
        //错误日志
        private readonly ILog logger = LogManager.GetLogger(typeof(药品查询界面));
        //操作日志
        private readonly ISysLogRepository LogReposioty = new SysLogRepository();
        private readonly string UserId = "";
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        public 药品查询界面(string userId)
        {
            this.UserId = userId;
            InitializeComponent();
        }

        private void 药品查询界面_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            BindData("");
        }
        public void BindData(string where)
        {
            var productList = ProductReposioty.GetProductList(where);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = productList;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string seacher = textBox1.Text.Trim();
            BindData($" ProductCode like '%{seacher}%'  or ProductName like '%{seacher}%'  ");
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
             e.RowBounds.Location.Y,
             dataGridView1.RowHeadersWidth - 4,
             e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   dataGridView1.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            ypcount.Text = "药品总数：" + (dataGridView1.Rows.Count).ToString() + " ";
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string Id = dataGridView1.SelectedRows[0].Cells["药品编号"].Value.ToString();
                var m = ProductReposioty.GetProductList(" a.Id="+ Convert.ToInt32(Id) + "");
                txtCreateTime.Text = m.ToList()[0].CreateTime.ToString();
                txtDW.Text = m.ToList()[0].MarketingUnit;
                txtGG.Text = m.ToList()[0].Specification;
                txtHYJ.Text = m.ToList()[0].MemberDiscount.ToString();
                txtJM.Text = m.ToList()[0].ProductCode;
                txtLSDJ.Text = m.ToList()[0].RetailPrice.ToString();
                txtSCQY.Text = m.ToList()[0].ManufacturingEnterprise;
                txtUserName.Text = m.ToList()[0].UserName;
                txtYPBM.Text = m.ToList()[0].Id.ToString();
                txtYPMC.Text = m.ToList()[0].ProductName;
                this.tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("请选择药品!");
            }
        }
    }
}
