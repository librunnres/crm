﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using log4net;
using Usido.CRM.IRepository;
using Usido.CRM.Repository;
using System.Windows.Forms;
using Usido.CRM.Models;
using Usido.CRM.Core.Extensions;

namespace Usido.CRM.App.Forms
{
    public partial class Frm_Logincs : Form
    {
        private readonly ISysUserRepository userReposioty = new SysUserRepository();
        private readonly ISysDepartmentRepository departmentReposit = new SysDepartmentRepository();
        private readonly ISysLogRepository logRepository = new SysLogRepository();
        private readonly ILog logger = LogManager.GetLogger(typeof(Frm_Logincs));
        public Frm_Logincs()
        {
            InitializeComponent();
            this.Tag = "";
        }
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Environment.Exit(1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Frm_Logincs_Load(object sender, EventArgs e)
        {
            var List_SysDepartment = departmentReposit.GetList();
            CoB_bm.DataSource = List_SysDepartment;
            CoB_bm.ValueMember = nameof(SysDepartment.Id);
            CoB_bm.DisplayMember = nameof(SysDepartment.DeptName);

        }

        private void CoB_bm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string va1 = CoB_bm.SelectedValue.ToString(); 
                var List_SysUser = userReposioty.GetList(new { DeptId = va1 });
                cboxUserName.DataSource = List_SysUser;
                cboxUserName.ValueMember = nameof(SysUser.Id);
                cboxUserName.DisplayMember = nameof(SysUser.UserName);
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
            }
        }

        private void txtPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login();
            }
        }


        private void Login()
        {
            string val = cboxUserName.SelectedValue.ToString();
            string psd = txtPwd.Text.Trim();

            if (val != "" && psd != "")
            {
                var List_SysUser = userReposioty.GetList(new { Id = val, Password = psd.ToMd5String() }).ToList();
                if (List_SysUser.Count() > 0)
                {
                    if (!List_SysUser[0].State)
                    {
                        MessageBox.Show("该账号已被禁用");
                        return;
                    }
                    logRepository.AddLog(val.ToInt(), "登录系统");
                    this.Tag = List_SysUser.ToList()[0].Id.ToString() + "|" + List_SysUser.ToList()[0].UserName;
                    this.DialogResult = DialogResult.OK;

                }
                else
                {
                    this.Tag = "";
                    //this.DialogResult = DialogResult.No;
                    MessageBox.Show("密码错误");
                }
            }
            else
            {
                MessageBox.Show("请完善登录信息");
            }
        }
    }
}
