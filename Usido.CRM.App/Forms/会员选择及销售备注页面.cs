﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 会员选择及销售备注页面 : Form
    {
        public 会员选择及销售备注页面()
        {
            InitializeComponent();
            this.Tag = "";
        }

        public 会员选择及销售备注页面(int? memberId, string remark)
        {
            InitializeComponent();
            this.Tag = "";
            this.memberId = memberId;
            this.remark = remark;
        }

        private readonly IMemberInfoRepository MemberInfoReposioty = new MemberInfoRepository();
        #region 检索会员绑定下拉框
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string seacher = textBox1.Text.Trim();
            if (seacher == "")
            {
                cboxHY.DataSource = null;
                return;
            }
            var productList = MemberInfoReposioty.GetMemberInfoList($" MemberCard like '%{seacher}%' or LLCard like '%{seacher}%' or Phone like '%{seacher}%'  ");
            cboxHY.DataSource = productList;
            if (productList.ToList().Count > 0)
            {
                cboxHY.DroppedDown = true;
            }
            cboxHY.ValueMember = nameof(MemberInfo.Id);
            cboxHY.DisplayMember = nameof(MemberInfo.UserName);
        }
        #endregion

        private void 会员选择及销售备注页面_Load(object sender, EventArgs e)
        {
            if (memberId!=null)
            {
                var productList = MemberInfoReposioty.GetMemberInfoList($"id = {memberId}");
                cboxHY.DataSource = productList;
                cboxHY.ValueMember = nameof(MemberInfo.Id);
                cboxHY.DisplayMember = nameof(MemberInfo.UserName);
            }
            if (remark!="")
            {
                textBox2.Text = remark;
            }
        }

        string ID;
        private int? memberId;
        private string remark;

        private void button1_Click(object sender, EventArgs e)
        {
            string Txt = textBox2.Text.Trim();
            if (cboxHY.SelectedValue == null)
            {
                ID = "0";
            }
            else
            {
                ID = cboxHY.SelectedValue.ToString();
            }
            if (Txt == "")
            {
                Txt = " ";
            }
            this.Tag = $"{ID}&{Txt}";
            this.DialogResult = DialogResult.OK;
        }

        private void 会员选择及销售备注页面_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Tag.ToString() == "")
            {
                this.Tag = $"0& ";
            }
            this.DialogResult = DialogResult.OK;
        }

       
    }
}
