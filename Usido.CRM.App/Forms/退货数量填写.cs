﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Usido.CRM.App.Forms
{
    public partial class 退货数量填写 : Form
    {
        public 退货数量填写()
        {
            InitializeComponent();
            this.DialogResult = DialogResult.No;
            this.Tag = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(textBox1.Text.Trim()) > 0)
                {
                    this.Tag = textBox1.Text.Trim();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception)
            {
                this.DialogResult = DialogResult.No;
                MessageBox.Show("输入的数量格式不正确！");
            } 
        }


    }
}
