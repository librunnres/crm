﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.IRepository;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class UpdateUserPassWord : Form
    {
        private string userId;
        private readonly ISysUserRepository userReposioty = new SysUserRepository();
        public UpdateUserPassWord()
        {
            InitializeComponent();
        }
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        public UpdateUserPassWord(string userId)
        {
            InitializeComponent();
            this.userId = userId;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("信息不全，无法修改！");
                return;
            }
            string oldpwd = textBox1.Text.Trim();
            string newpwd = textBox2.Text.Trim();
            string newpwd1 = textBox3.Text.Trim();
            var user = userReposioty.Get(int.Parse(userId));
            if (user.Password != oldpwd.ToMd5String())
            {
                MessageBox.Show("旧密码错误！");
                return;
            }
            if (newpwd!= newpwd1)
            {
                MessageBox.Show("两次新密码不一致！");
                return;
            }
            if (userReposioty.UpdatePassword(userId.ToInt(), newpwd.ToMd5String()))
            {
                MessageBox.Show("密码修改成功！请牢记您的新密码");
                this.Close();            } 

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
