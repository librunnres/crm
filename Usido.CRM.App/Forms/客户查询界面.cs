﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 客户查询界面 : Form
    {
        private readonly ISysSupplierRepository SupplierReposioty = new SysSupplierRepository();
        //错误日志
        private readonly ILog logger = LogManager.GetLogger(typeof(客户设置界面));
        //操作日志
        private readonly ISysLogRepository LogReposioty = new SysLogRepository();
        public 客户查询界面()
        {
            InitializeComponent();
        }
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        private void 客户查询界面_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            BindData("");
        }
        public void BindData(string where)
        {
            var productList = SupplierReposioty.GetSupplierList(where);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = productList;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string Id = dataGridView1.SelectedRows[0].Cells["企业编号"].Value.ToString();
                var m = SupplierReposioty.GetSupplierList(" a.Id=" + Convert.ToInt32(Id) + "").ToList();
                TB_QYBM.Text = m[0].Id.ToString();
                TB_QYDZ.Text = m[0].CompanyAddress;
                TB_QYFR.Text = m[0].LegalPerson;
                TB_QYJM.Text = m[0].CompanyCode;
                TB_QYMC.Text = m[0].CompanyName;
                TB_TEL.Text = m[0].Phone;
                TB_lxr.Text = m[0].Contacts;
                TB_CreateTime.Text = m[0].CreateTime.ToString();
                TB_TJR.Text = m[0].UserName;
                this.tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("请选择供应商!");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            string seacher = textBox1.Text.Trim();
            BindData($" CompanyCode like '%{seacher}%'  or CompanyName like '%{seacher}%'  ");
        }
    }
}
