﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 会员充值 : Form
    {
        private readonly IMemberInfoRepository MemberInfoReposioty = new MemberInfoRepository();
        private readonly IMoneyFlowRepository MoneyFlowReposioty = new MoneyFlowRepostitory();

        //错误日志
        private readonly ILog logger = LogManager.GetLogger(typeof(会员充值));

        //操作日志
        private readonly ISysLogRepository LogReposioty = new SysLogRepository();
        //充值账号
        private readonly string VipCard = "";
        private readonly int VipId = 0;
        private readonly int type = 0;  //0 普通会员充值   1理疗充值
        private readonly int UserId = 0;  //当前用户ID
        public 会员充值(string card, int VipId, int type,int userId)
        {
            this.VipCard = card;
            this.VipId = VipId;
            this.type = type;
            this.UserId = userId;
            InitializeComponent();
        }

        private void txtMones_KeyDown(object sender, KeyEventArgs e)
        {
            //回车
            if (e.KeyCode == Keys.Enter)
            {
                string money = txtMones.Text.Trim();  //充值金额
                string type = labtype.Text;  //充值类别
                int id = Convert.ToInt32(labVipId.Text.Trim());  //id
                if (GetNum(money))
                {
                    if (Convert.ToInt32(money)>0)
                    {
                        if (MemberInfoReposioty.UpdateAccountMon(id.ToString(), type, Convert.ToDecimal(money)) > 0)
                        {
                            MoneyFlow m = new MoneyFlow();
                            m.CreateTime = DateTime.Now;
                            m.CreateUserId = UserId;
                            m.MemberCard = txtCard.Text.Trim();
                            m.MemberId = id;
                            m.Moneys = Convert.ToDecimal(money);
                            m.Type = type == "0" ? "普通会员充值" : "理疗充值";
                            MoneyFlowReposioty.Insert(m);
                            MessageBox.Show("充值成功!");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("充值失败!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("充值金额格式错误!");
                    }
                }
                else
                {
                    MessageBox.Show("充值金额格式错误");
                }


            }
        }

        private void 会员充值_Load(object sender, EventArgs e)
        {
            txtCard.Text = VipCard;
            labVipId.Text = VipId.ToString();
            labtype.Text = type.ToString();
        }

        #region 验证文本框输入为数字
        /// <summary>
        /// 验证是不是数字（包含整数和小数）
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool GetNum(string str)
        {
            return Regex.IsMatch(str, @"^[-]?\d+[.]?\d*$");
        }
        #endregion
    }
}
