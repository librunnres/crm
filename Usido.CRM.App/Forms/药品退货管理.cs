﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 药品退货管理 : Form
    {
        private readonly IStockProductBalanceRepository productRepository = new StockProductBalanceRepository();
        private readonly ISalesOrderRepository SalesOrderRepository = new SalesOrderRepository();
        private readonly ISalesReturnOrderRepository SalesReturnOrder = new SalesReturnOrderRepository();
        private readonly ISysUserRepository userReposioty = new SysUserRepository();
        private readonly IMemberInfoRepository MemberInfoReposioty = new MemberInfoRepository();//会员
        private readonly ISalesInfoRepository SalesInfoRepository = new SalesInfoRepository();
        private readonly ISysLogRepository logRepository = new SysLogRepository();
        private readonly ILog logger = LogManager.GetLogger(nameof(药品退货管理));
        ///单据状态： 制单  已保存

        public 药品退货管理()
        {
            InitializeComponent();
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        private string userId;
        private string userName;

        public 药品退货管理(string userId, string userName)
        {
            InitializeComponent();
            this.userId = userId;
            this.userName = userName;
        }

        #region 取消按钮，默认退出

        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion 取消按钮，默认退出

        #region 新增按钮，跳转页面。初始化 订单编号与订单状态

        /// <summary>
        /// 新增按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripAdd_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            if (dataGridView1.Rows.Count > 0)
            {
                DialogResult RSS = MessageBox.Show(this, "请确保当前订单已经保存成功？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (RSS == DialogResult.Yes)
                {
                    dataGridView1.Rows.Clear();
                }
                else
                {
                    return;
                }
            }
            lab_djbh.Text = "";
            lab_ddzt.Text = "制单";
            toolStripLabel2.Text = "";
            toolStripLabel3.Text = userName;
            Lab_HJcount.Text = "";
            toolStrip1.Text = "";
            toolStripLabel3.Text = "";
            toolStripLabel4.Text = "";
            lab_thbm.Text = "";
            LAB_id.Text = "";
            lab_xszdID.Text = "";
            lab_thbm.Select();
        }

        #endregion 新增按钮，跳转页面。初始化 订单编号与订单状态

        #region Load

        private void 采购入库管理_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            lab_djbh.Text = "";
            lab_ddzt.Text = "";
            toolStripLabel3.Text = userName;
            Lab_HJcount.Text = "";
            toolStrip1.Text = "";
            toolStripLabel3.Text = "";
            //toolStripLabel4.Text = "";
            LAB_id.Text = "";
            lab_thbm.Text = "";
            this.toolStripLabel4.Margin = new System.Windows.Forms.Padding(this.Width - 700, 1, 0, 2);
            BingOrder();
        }

        #endregion Load

        #region 绑定退货订单

        /// <summary>
        /// 绑定退货订单
        /// </summary>
        private void BingOrder()
        {
            dgvSupplyList.AutoGenerateColumns = false;
            var SalesRrturnOrderModel = SalesReturnOrder.GetSalesReturnList();
            dgvSupplyList.Rows.Clear();
            for (int i = 0; i < SalesRrturnOrderModel.ToList().Count; i++)
            {
                dgvSupplyList.Rows.Add();
                dgvSupplyList.Rows[i].Cells["退货订单编号"].Value = SalesRrturnOrderModel.ToList()[i].OrderCode;
                dgvSupplyList.Rows[i].Cells["制单人"].Value = SalesRrturnOrderModel.ToList()[i].UserName;
                dgvSupplyList.Rows[i].Cells["制单时间"].Value = SalesRrturnOrderModel.ToList()[i].CreateTime;
                dgvSupplyList.Rows[i].Cells["退货订单状态"].Value = SalesRrturnOrderModel.ToList()[i].State;
                dgvSupplyList.Rows[i].Cells["退货入库时间"].Value = SalesRrturnOrderModel.ToList()[i].PutTime;
                dgvSupplyList.Rows[i].Cells["Id"].Value = SalesRrturnOrderModel.ToList()[i].Id;
            }
            for (int i = 0; i < dgvSupplyList.Rows.Count; i++)
            {
                //绑定订单状态
                if (dgvSupplyList.Rows[i].Cells["退货订单状态"].Value.ToString() == "0")
                {
                    dgvSupplyList.Rows[i].Cells["退货订单状态"].Value = "已保存";
                    dgvSupplyList.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                }
                if (dgvSupplyList.Rows[i].Cells["退货订单状态"].Value.ToString() == "1")
                {
                    dgvSupplyList.Rows[i].Cells["退货订单状态"].Value = "已完成";
                    dgvSupplyList.Rows[i].DefaultCellStyle.BackColor = Color.Gray;
                }
                dgvSupplyList.ClearSelection();
            }
        }

        #endregion 绑定退货订单

        #region 绘制订单序号，并且统计总数量与总金额

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            double Money = 0.00;
            double Count = 0;
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
          e.RowBounds.Location.Y,
          dataGridView1.RowHeadersWidth - 4,
          e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   dataGridView1.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            try
            {
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].Cells["金额"].Value != null)
                    {
                        Count++;
                        double num = double.Parse(dataGridView1.Rows[i].Cells["金额"].Value.ToString().Replace("￥", ""));
                        Money += num;
                    }
                }
                Lab_HJcount.Text = "合计：" + Count.ToString() + "笔 ";
                toolStripLabel2.Text = "总金额:￥" + Money.ToString();
            }
            catch (Exception ex)
            {
            }
        }

        #endregion 绘制订单序号，并且统计总数量与总金额

        #region 入库

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (lab_ddzt.Text.Contains("制单") || lab_ddzt.Text.Trim() == "")
            {
                MessageBox.Show("请先保存订单！确认无误后再入库！");
                return;
            }
            if (lab_ddzt.Text.Contains("已完成"))
            {
                MessageBox.Show("该订单已完成！");
                return;
            }
            int orderID = int.Parse(lab_xszdID.Text);
            if (SalesReturnOrder.OutStore(orderID, int.Parse(this.userId)))
            {
                MessageBox.Show("商品退货成功！");
                var SalesReturnOrderMOdel = SalesReturnOrder.Get(orderID);
                {
                    // 读取销售单号与状态
                    if (SalesReturnOrderMOdel.State == 0)
                    {
                        lab_ddzt.Text = "已保存";
                    }
                    else
                    {
                        lab_ddzt.Text = "已完成";
                    }
                    BingOrder();
                }
            }
        }

        #endregion 入库

        #region 绑定订单明细

        private void dgvSupplyList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int index = dgvSupplyList.CurrentCell.RowIndex;
                string Id = dgvSupplyList.Rows[index].Cells["Id"].Value.ToString();
                lab_thbm.Text = dgvSupplyList.Rows[index].Cells["退货订单编号"].Value.ToString();
                lab_ddzt.Text = dgvSupplyList.Rows[index].Cells["退货订单状态"].Value.ToString();
                lab_xszdID.Text = Id;
                toolStripLabel4.Text = $"制表时间：{dgvSupplyList.Rows[index].Cells["制单时间"].Value.ToString()}";
                var SaleReturnOderInfo = SalesReturnOrder.GetSalesReturnInfoList(int.Parse(Id));
                tabControl1.SelectedIndex = 1;
                dataGridView1.Rows.Clear();
                for (int i = 0; i < SaleReturnOderInfo.Count; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells["药品编码"].Value = SaleReturnOderInfo[i].ProductId;
                    dataGridView1.Rows[i].Cells["药品名称"].Value = SaleReturnOderInfo[i].ProductName;
                    dataGridView1.Rows[i].Cells["生产企业"].Value = SaleReturnOderInfo[i].ManufacturingEnterprise;
                    dataGridView1.Rows[i].Cells["零售价"].Value = SaleReturnOderInfo[i].RetailPrice;
                    dataGridView1.Rows[i].Cells["会员价"].Value = SaleReturnOderInfo[i].MemberPrice;
                    dataGridView1.Rows[i].Cells["数量"].Value = SaleReturnOderInfo[i].Quantity;
                    dataGridView1.Rows[i].Cells["金额"].Value = SaleReturnOderInfo[i].Price;
                    dataGridView1.Rows[i].Cells["会员金额"].Value = (decimal.Parse(SaleReturnOderInfo[i].Quantity.ToString()) * decimal.Parse(SaleReturnOderInfo[i].MemberPrice.ToString())).ToString();
                    dataGridView1.Rows[i].Cells["药品批号"].Value = SaleReturnOderInfo[i].BatchNumber;
                    dataGridView1.Rows[i].Cells["药品有效期"].Value = SaleReturnOderInfo[i].DateExpiry;
                    dataGridView1.Rows[i].Cells["可退货数量"].Value = SaleReturnOderInfo[i].QuantityReturned;
                    dataGridView1.Rows[i].Cells["退货数量"].Value = SaleReturnOderInfo[i].Quantity;
                    dataGridView1.Rows[i].Cells["销售退货订单明细ID"].Value = SaleReturnOderInfo[i].OriginalSalesInforId;
                }
                this.dataGridView1.Refresh();
            }
            catch (Exception w)
            {
            }
        }

        #endregion 绑定订单明细

        #region 解决切换卡顿重影

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = tabControl1.SelectedIndex;
            if (page == 0)
            {
                BingOrder();
            }
        }

        #endregion 解决切换卡顿重影

        #region 保存退货订单  (inert  与  update)

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count <= 0)
            {
                MessageBox.Show("无数据可保存");
                return;
            }
            SalesReturnOrder order = new SalesReturnOrder()
            {
                State = 0,
                UserId = int.Parse(this.userId)
            };

            List<SalesReturnInfo> SalesReturnList = new List<SalesReturnInfo>();
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (dataGridView1.Rows[i].Cells["退货数量"].Value == null ||
                    dataGridView1.Rows[i].Cells["退货数量"].Value.ToString() == "" ||
                    decimal.Parse(dataGridView1.Rows[i].Cells["退货数量"].Value.ToString()) <= 0)
                {
                    continue;
                }
                var info = new SalesReturnInfo();
                info.BatchNumber = dataGridView1.Rows[i].Cells["药品批号"].Value.ToString();
                if (dataGridView1.Rows[i].Cells["药品有效期"].Value != null)
                {
                    info.DateExpiry = DateTime.Parse(dataGridView1.Rows[i].Cells["药品有效期"].Value.ToString());
                }
                info.OriginalSalesInforId = dataGridView1.Rows[i].Cells["销售退货订单明细ID"].Value.ToString().ToInt();
                info.Price = decimal.Parse(dataGridView1.Rows[i].Cells["零售价"].Value.ToString());
                info.ProductId = dataGridView1.Rows[i].Cells["药品编码"].Value.ToString().ToInt();
                info.Quantity = decimal.Parse(dataGridView1.Rows[i].Cells["退货数量"].Value.ToString());
                SalesReturnList.Add(info);
            }
            if (SalesReturnList.Count <= 0)
            {
                MessageBox.Show("无数据");
                return;
            }
            if (!lab_thbm.Text.IsNullOrEmpty()) //修改
            {
                order.Id = lab_xszdID.Text.ToInt();
                if (SalesReturnOrder.UpdateSalesReturn(order, SalesReturnList))
                {
                    MessageBox.Show("修改成功");
                }
                else
                {
                    MessageBox.Show("修改失败");
                }
            }
            else
            {
                order.OriginalSalesOrderId = int.Parse(LAB_id.Text);
                int SalesReturnOrderId = SalesReturnOrder.AddSalesReturn(order, SalesReturnList);
                if (SalesReturnOrderId > 0)
                {
                    lab_xszdID.Text = SalesReturnOrderId.ToString();
                    var SalesReturnModel = SalesReturnOrder.Get(SalesReturnOrderId);
                    {
                        // 读取退货单号与状态
                        lab_thbm.Text = SalesReturnModel.OrderCode.ToString();
                        if (SalesReturnModel.State == 0)
                        {
                            lab_ddzt.Text = "已保存";
                        }
                        else
                        {
                            lab_ddzt.Text = "已完成";
                        }
                        MessageBox.Show("保存成功");
                        var SalesReturnInfo = SalesReturnOrder.GetSalesReturnInfoList(SalesReturnOrderId);
                        dataGridView1.Rows.Clear();
                        for (int i = 0; i < SalesReturnInfo.Count; i++)
                        {
                            dataGridView1.Rows.Add();
                            dataGridView1.Rows[i].Cells["销售退货订单明细ID"].Value = SalesReturnInfo[i].OriginalSalesInforId;
                            dataGridView1.Rows[i].Cells["药品编码"].Value = SalesReturnInfo[i].ProductId;
                            dataGridView1.Rows[i].Cells["药品名称"].Value = SalesReturnInfo[i].ProductName;
                            dataGridView1.Rows[i].Cells["生产企业"].Value = SalesReturnInfo[i].ManufacturingEnterprise;
                            dataGridView1.Rows[i].Cells["零售价"].Value = SalesReturnInfo[i].RetailPrice;
                            dataGridView1.Rows[i].Cells["会员价"].Value = SalesReturnInfo[i].MemberPrice;
                            dataGridView1.Rows[i].Cells["数量"].Value = SalesReturnInfo[i].Quantity;
                            dataGridView1.Rows[i].Cells["金额"].Value = SalesReturnInfo[i].Price;
                            dataGridView1.Rows[i].Cells["会员金额"].Value = (decimal.Parse(SalesReturnInfo[i].Quantity.ToString()) * decimal.Parse(SalesReturnInfo[i].MemberPrice.ToString())).ToString();
                            dataGridView1.Rows[i].Cells["药品批号"].Value = SalesReturnInfo[i].BatchNumber;
                            dataGridView1.Rows[i].Cells["药品有效期"].Value = SalesReturnInfo[i].DateExpiry;
                            dataGridView1.Rows[i].Cells["可退货数量"].Value = SalesReturnInfo[i].QuantityReturned;
                        }
                    }
                }
            }
        }

        #endregion 保存退货订单  (inert  与  update)

        #region 根据销售订单编号查询明细

        private void button1_Click(object sender, EventArgs e)
        {
            if (lab_djbh.Text.Trim().Length != 20)
            {
                MessageBox.Show("请仔细核对订单编号！");
                dataGridView1.Rows.Clear();
                return;
            }
            lab_ddzt.Text = "制表";
            string 销售单据编号 = lab_djbh.Text.Trim();
            int 销售订单ID;
            var SalesOrderModel = SalesOrderRepository.GetList($" where  OrderCode='{销售单据编号}'", new { });
            if (SalesOrderModel.Count() > 0)
            {
                销售订单ID = SalesOrderModel.ToList()[0].Id;
                LAB_id.Text = 销售订单ID.ToString();
                var SaleOderInfo = SalesOrderRepository.GetSalesInfoReturnQuantityList(销售订单ID);
                dataGridView1.Rows.Clear();
                for (int i = 0; i < SaleOderInfo.Count; i++)
                {
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells["销售退货订单明细ID"].Value = SaleOderInfo[i].Id;
                    dataGridView1.Rows[i].Cells["药品编码"].Value = SaleOderInfo[i].ProductId;
                    dataGridView1.Rows[i].Cells["药品名称"].Value = SaleOderInfo[i].ProductName;
                    dataGridView1.Rows[i].Cells["生产企业"].Value = SaleOderInfo[i].ManufacturingEnterprise;
                    dataGridView1.Rows[i].Cells["零售价"].Value = SaleOderInfo[i].RetailPrice;
                    dataGridView1.Rows[i].Cells["会员价"].Value = SaleOderInfo[i].MemberPrice;
                    dataGridView1.Rows[i].Cells["数量"].Value = SaleOderInfo[i].Quantity;
                    dataGridView1.Rows[i].Cells["金额"].Value = SaleOderInfo[i].Price;
                    dataGridView1.Rows[i].Cells["会员金额"].Value = (decimal.Parse(SaleOderInfo[i].Quantity.ToString()) * decimal.Parse(SaleOderInfo[i].MemberPrice.ToString())).ToString();
                    dataGridView1.Rows[i].Cells["药品批号"].Value = SaleOderInfo[i].BatchNumber;
                    dataGridView1.Rows[i].Cells["药品有效期"].Value = SaleOderInfo[i].DateExpiry;
                    dataGridView1.Rows[i].Cells["可退货数量"].Value = SaleOderInfo[i].QuantityReturned;
                }
            }
            else
            {
                MessageBox.Show("查无此单!");
                return;
            }
        }

        #endregion 根据销售订单编号查询明细

        #region 部分退货

        private void 部分退货_Click(object sender, EventArgs e)
        {
            int index;
            decimal THcount = 0;
            try
            {
                if (lab_ddzt.Text == "已完成")
                {
                    MessageBox.Show("该订单已完成！不能修改");
                    return;
                }
                index = dataGridView1.CurrentCell.RowIndex;
            }
            catch (Exception)
            {
                return;
            }
            ToolStripMenuItem TSM = (ToolStripMenuItem)sender;
            if (TSM.Name == "全部退货")
            {
                THcount = decimal.Parse(dataGridView1.Rows[index].Cells["可退货数量"].Value.ToString());
            }
            else
            {
                退货数量填写 thsltx = new 退货数量填写();
                thsltx.ShowDialog();
                if (thsltx.DialogResult == DialogResult.OK)
                {
                    if (decimal.Parse(dataGridView1.Rows[index].Cells["可退货数量"].Value.ToString()) >= decimal.Parse(thsltx.Tag.ToString()))
                    {
                        THcount = decimal.Parse(thsltx.Tag.ToString());
                    }
                    else
                    {
                        MessageBox.Show("退货数量超出可退数量！");
                    }
                }
            }

            dataGridView1.Rows[index].Cells["退货数量"].Value = THcount.ToString();
        }

        #endregion 部分退货

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int index = dgvSupplyList.CurrentCell.RowIndex;
                //判断退货订单状态
                if (dgvSupplyList.Rows[index].Cells["退货订单状态"].Value.ToString() == "已完成")
                {
                    MessageBox.Show("该订单已完成，不能删除");
                    return;
                }
                var code = dgvSupplyList.Rows[index].Cells["退货订单编号"].Value.ToString();
                DialogResult RSS = MessageBox.Show(this, $"确定要删除销售退货订单【{code}】？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (RSS == DialogResult.Yes)
                {
                    var id = dgvSupplyList.Rows[index].Cells["Id"].Value.ToString();

                    if (SalesReturnOrder.DeleteSalesReturn(id.ToInt()))
                    {
                        string LogStr = $"删除{code}成功";
                        logRepository.AddLog(int.Parse(userId), LogStr);
                        MessageBox.Show(LogStr);
                        BingOrder();
                    }
                    else
                    {
                        MessageBox.Show("删除失败，请稍后重试");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}