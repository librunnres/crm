﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Usido.CRM.App.Forms
{
    public partial class 数量 : Form
    {
        public 数量()
        {
            InitializeComponent();
        }
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (decimal.Parse(textBox1.Text.Trim()) == 0 || textBox1.Text.Trim().Length == 0)
                {
                    MessageBox.Show("数量必须大于0!");
                    this.DialogResult = DialogResult.No;
                    this.Tag = 0;
                    return;
                }
                this.Tag = textBox1.Text.Trim();
            }
            catch (Exception)
            {
                MessageBox.Show("数量格式有误!");
                this.DialogResult = DialogResult.No;
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void 数量_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
            }
            else
            {
                this.DialogResult = DialogResult.No;
                this.Tag = 0;
            }

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                try
                {
                    if (decimal.Parse(textBox1.Text.Trim()) == 0 || textBox1.Text.Trim().Length == 0)
                    {
                        MessageBox.Show("数量必须大于0!");
                        this.DialogResult = DialogResult.No;
                        this.Tag = 0;
                        return;
                    }
                    this.Tag = textBox1.Text.Trim();
                }
                catch (Exception)
                {
                    MessageBox.Show("数量格式有误!");
                    this.DialogResult = DialogResult.No;
                    return;
                }
                this.DialogResult = DialogResult.OK;
            } 
        }

        private void 数量_Load(object sender, EventArgs e)
        {

            this.textBox1.Select();
        }
    }
}
