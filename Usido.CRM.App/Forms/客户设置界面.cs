﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Usido.CRM.Core.ToolHelper;
using Usido.CRM.IRepository;
using Usido.CRM.Models;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 客户设置界面 : Form
    {
        private readonly ISysSupplierRepository SupplierReposioty = new SysSupplierRepository();
        //错误日志
        private readonly ILog logger = LogManager.GetLogger(typeof(客户设置界面));
        //操作日志
        private readonly ISysLogRepository LogReposioty = new SysLogRepository();
        private readonly string UserId = "";
        #region 窗体绘制
        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁 
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
        #endregion
        public 客户设置界面(string userid)
        {
            this.UserId = userid;
            InitializeComponent();
        }

        private void 客户设置界面_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            BindData("");
        }

        public void BindData(string where)
        {
            var productList = SupplierReposioty.GetSupplierList(where);
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = productList;
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            TB_QYBM.Text = "";
            TB_QYDZ.Text = "";
            TB_QYFR.Text = "";
            TB_QYJM.Text = "";
            TB_QYMC.Text = "";
            TB_TEL.Text = "";
            TB_lxr.Text = "";
            this.tabControl1.SelectedIndex = 1;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string Id = dataGridView1.SelectedRows[0].Cells["企业编号"].Value.ToString();
                SysSupplier m = SupplierReposioty.Get(Convert.ToInt32(Id));
                TB_QYBM.Text = m.Id.ToString();
                TB_QYDZ.Text = m.CompanyAddress;
                TB_QYFR.Text = m.LegalPerson;
                TB_QYJM.Text = m.CompanyCode;
                TB_QYMC.Text = m.CompanyName;
                TB_TEL.Text = m.Phone;
                TB_lxr.Text = m.Contacts;
                this.tabControl1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("请选择药品!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SysSupplier m = new SysSupplier();
            if (string.IsNullOrEmpty(TB_QYDZ.Text.Trim()) || string.IsNullOrEmpty(TB_QYFR.Text.Trim()) || string.IsNullOrEmpty(TB_QYJM.Text.Trim()) || string.IsNullOrEmpty(TB_QYMC.Text.Trim()) || string.IsNullOrEmpty(TB_TEL.Text.Trim()))
            {
                MessageBox.Show("请完善信息再保存!");
            }
            else
            {

                m.CompanyAddress = TB_QYDZ.Text.Trim();
                m.CompanyCode = TB_QYJM.Text.Trim();
                m.CompanyName = TB_QYMC.Text.Trim();
                m.Contacts = TB_lxr.Text.Trim();
                m.LastUpdateTime = DateTime.Now;
                m.LastUpdateUserId = Convert.ToInt32(UserId);
                m.LegalPerson = TB_QYFR.Text.Trim();
                m.Phone = TB_TEL.Text.Trim();
                m.State = true;
                if (TB_QYBM.Text.Trim().Length > 0)
                {
                    string Id = dataGridView1.SelectedRows[0].Cells["企业编号"].Value.ToString();
                    SysSupplier p = SupplierReposioty.Get(Convert.ToInt32(Id));
                    m.CreateTime = p.CreateTime;
                    m.CreateUserId = p.CreateUserId;
                    m.Id = p.Id;
                    //编辑
                    if (SupplierReposioty.Update(m) > 0)
                    {
                        LogReposioty.AddLog(Convert.ToInt32(UserId), "药品信息修改完成", m);
                        MessageBox.Show("修改成功!");
                        this.tabControl1.SelectedIndex = 0;
                        BindData("");
                    }
                    else
                    {
                        MessageBox.Show("修改失败!");
                    }
                }
                else
                {
                    m.CreateUserId = Convert.ToInt32(UserId);
                    m.CreateTime = DateTime.Now;
                    //新增
                    if (SupplierReposioty.Insert(m) > 0)
                    {
                        LogReposioty.AddLog(Convert.ToInt32(UserId), "供应商添加完成", m);
                        MessageBox.Show("保存成功!");
                        this.tabControl1.SelectedIndex = 0;
                        BindData("");

                    }
                    else
                    {
                        MessageBox.Show("保存失败!");
                    }
                }
            }
        }


        private void TB_QYMC_Leave(object sender, EventArgs e)
        {
            if (TB_QYMC.Text.Trim().Length > 0)
            {
                TB_QYJM.Text = "";
                for (int i = 0; i < TB_QYMC.Text.Trim().Length; i++)
                {

                    TB_QYJM.Text += Tools.GetPYChar(TB_QYMC.Text.Trim().Substring(i, 1).Trim());
                }
            }
            else
            {
                TB_QYJM.Text = "";
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolStripDropDownButton4_Click(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 0)
            {
                string Id = dataGridView1.SelectedRows[0].Cells["企业编号"].Value.ToString();
                string Tym = dataGridView1.SelectedRows[0].Cells["企业名称"].Value.ToString();
                DialogResult RSS = MessageBox.Show(this, $"确定要删除企业{Tym}？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (RSS == DialogResult.Yes)
                {
                    int res = SupplierReposioty.Delete(int.Parse(Id));
                    if (res > 0)
                    {
                        string LogStr = $"删除{Tym}成功";
                        MessageBox.Show("删除成功");
                        LogReposioty.AddLog(Convert.ToInt32(UserId), LogStr);
                        BindData("");
                    }
                }
            }
        }

        private void toolStripDropDownButton3_Click(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 0)
            {
                string Id = dataGridView1.SelectedRows[0].Cells["企业编号"].Value.ToString();
                SysSupplier m = SupplierReposioty.Get(Convert.ToInt32(Id));
                TB_QYBM.Text = m.Id.ToString();
                TB_QYDZ.Text = m.CompanyAddress;
                TB_QYFR.Text = m.LegalPerson;
                TB_QYJM.Text = m.CompanyCode;
                TB_QYMC.Text = m.CompanyName;
                TB_TEL.Text = m.Phone;
                TB_lxr.Text = m.Contacts;
                this.tabControl1.SelectedIndex = 1;
            }
        }

        private void toolStripDropDownButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string seacher = textBox1.Text.Trim();
            BindData($" CompanyCode like '%{seacher}%'  or CompanyName like '%{seacher}%'  ");
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            Rectangle rectangle = new Rectangle(e.RowBounds.Location.X,
            e.RowBounds.Location.Y,
            dataGridView1.RowHeadersWidth - 4,
            e.RowBounds.Height);
            TextRenderer.DrawText(e.Graphics,
                  (e.RowIndex + 1).ToString(),
                   dataGridView1.RowHeadersDefaultCellStyle.Font,
                   rectangle,
                   dataGridView1.RowHeadersDefaultCellStyle.ForeColor,
                   TextFormatFlags.VerticalCenter | TextFormatFlags.Right);
            listCount.Text = "供应商总数：" + (dataGridView1.Rows.Count).ToString() + " ";
        }
    }
}
