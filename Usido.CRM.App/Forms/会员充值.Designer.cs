﻿namespace Usido.CRM.App.Forms
{
    partial class 会员充值
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCard = new System.Windows.Forms.TextBox();
            this.txtMones = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labVipId = new System.Windows.Forms.Label();
            this.labtype = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "会员账号:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "充值金额:";
            // 
            // txtCard
            // 
            this.txtCard.Enabled = false;
            this.txtCard.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtCard.Location = new System.Drawing.Point(135, 12);
            this.txtCard.Name = "txtCard";
            this.txtCard.ReadOnly = true;
            this.txtCard.Size = new System.Drawing.Size(209, 35);
            this.txtCard.TabIndex = 2;
            // 
            // txtMones
            // 
            this.txtMones.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtMones.Location = new System.Drawing.Point(135, 57);
            this.txtMones.Name = "txtMones";
            this.txtMones.Size = new System.Drawing.Size(188, 35);
            this.txtMones.TabIndex = 3;
            this.txtMones.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMones_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(323, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "￥";
            // 
            // labVipId
            // 
            this.labVipId.AutoSize = true;
            this.labVipId.Location = new System.Drawing.Point(15, 88);
            this.labVipId.Name = "labVipId";
            this.labVipId.Size = new System.Drawing.Size(41, 12);
            this.labVipId.TabIndex = 5;
            this.labVipId.Text = "label4";
            this.labVipId.Visible = false;
            // 
            // labtype
            // 
            this.labtype.AutoSize = true;
            this.labtype.Location = new System.Drawing.Point(77, 88);
            this.labtype.Name = "labtype";
            this.labtype.Size = new System.Drawing.Size(41, 12);
            this.labtype.TabIndex = 6;
            this.labtype.Text = "label4";
            this.labtype.Visible = false;
            // 
            // 会员充值
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 109);
            this.Controls.Add(this.labtype);
            this.Controls.Add(this.labVipId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMones);
            this.Controls.Add(this.txtCard);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "会员充值";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "会员充值";
            this.Load += new System.EventHandler(this.会员充值_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCard;
        private System.Windows.Forms.TextBox txtMones;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labVipId;
        private System.Windows.Forms.Label labtype;
    }
}