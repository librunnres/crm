﻿using System;
using System.Windows.Forms;
using Usido.CRM.Core.Extensions;
using Usido.CRM.IRepository;
using Usido.CRM.Repository;

namespace Usido.CRM.App.Forms
{
    public partial class 电子处方 : Form
    {
        private readonly ISalesOrderRepository SalesOrderRepository = new SalesOrderRepository();
        private readonly IStockProductBalanceRepository stockProductBalanceRepository = new StockProductBalanceRepository();

        public 电子处方()
        {
            InitializeComponent();
        }

        #region 窗体绘制

        /// <summary>
        /// 解决加载窗体多控件所造成的闪烁
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }

        #endregion 窗体绘制

        private string userId;
        private string userName;

        public 电子处方(string userId, string userName)
        {
            InitializeComponent();
            this.userId = userId;
            this.userName = userName;
        }

        #region 解决切换卡顿重影

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = tabControl1.SelectedIndex;
            if (page == 0)
            {
                BindData();
            }
            if (page == 2)
            {
                this.Close();
            }
        }
        #endregion 解决切换卡顿重影

        private void BindData()
        {

        }

        private void txtProductQuery_TextChanged(object sender, EventArgs e)
        {
            var code = txtProductQuery.Text.Trim();

            if (code.IsNullOrEmpty())
            {
                return;
            }

            var productList = stockProductBalanceRepository.GetStockInfoList(code);

            dgvProductList.AutoGenerateColumns = false;
            dgvProductList.DataSource = productList;
            dgvProductList.ClearSelection();
        }

        private void txtProductQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtProductQuery.Text.Length > 0)
                {
                    if (dgvProductList.Rows.Count > 0)
                    {
                        dgvProductList.Focus();
                        dgvProductList.Rows[0].Selected = true;
                    }
                }
            }
        }

        private void dgvProductList_KeyDown(object sender, KeyEventArgs e)
        {
            //回车
            if (e.KeyCode == Keys.Enter)
            {
                if (lblOrderState.Text == "已完成")
                {
                    MessageBox.Show("此订单已完成,无法更改!");
                    return;
                }
                if (dgvProductList.SelectedRows.Count > 0)
                {
                    string Count;
                    数量 frmNum = new Forms.数量();
                    frmNum.ShowDialog();
                    if (frmNum.DialogResult.Equals(DialogResult.OK))
                    {
                        frmNum.Close();
                        Count = frmNum.Tag.ToString();
                    }
                    else
                    {
                        return;
                    }
                    try
                    {
                        string Id = dgvProductList.SelectedRows[0].Cells["dgv2编号"].Value.ToString();
                        for (int i = 0; i < dgvSalesOrderInfo.Rows.Count; i++)
                        {
                            if (dgvSalesOrderInfo.Rows[i].Cells["药品编码"].Value.ToString() == Id)
                            {
                                MessageBox.Show("已存在该药品");
                                return;
                            }
                        }

                        //string name = dgvSalesOrderInfo.SelectedRows[0].Cells["dgv2药品名称"].Value.ToString();
                        //string gg = dgvSalesOrderInfo.SelectedRows[0].Cells["dgv2规格"].Value.ToString();
                        //string scqy = dgvSalesOrderInfo.SelectedRows[0].Cells["dgv2生产企业"].Value.ToString();
                        //string kc = dataGridView2.SelectedRows[0].Cells["dgv2库存"].Value.ToString();
                        //string ph = dataGridView2.SelectedRows[0].Cells["批号"].Value.ToString();
                        //string rq = "";
                        //if (dataGridView2.SelectedRows[0].Cells["过期时间"].Value != null)
                        //{
                        //    rq = dataGridView2.SelectedRows[0].Cells["过期时间"].Value.ToString();
                        //}
                        //if (decimal.Parse(Count) > decimal.Parse(kc))
                        //{
                        //    MessageBox.Show($"该药品库存{kc}，无法出售{Count}");
                        //    return;
                        //}
                        //if (decimal.Parse(kc) <= 0)
                        //{
                        //    MessageBox.Show("该药品库存不足！无法出售");
                        //    return;
                        //}
                        //string dw = dataGridView2.SelectedRows[0].Cells["dgv2单位"].Value.ToString();
                        //string lsj = dataGridView2.SelectedRows[0].Cells["dgv2零售价"].Value.ToString();
                        //string hyjzk = dataGridView2.SelectedRows[0].Cells["dgv2会员折扣"].Value.ToString();
                        //int index = dataGridView1.Rows.Add();
                        //dataGridView1.Rows[index].Cells["药品编码"].Value = Id;
                        //dataGridView1.Rows[index].Cells["药品名称"].Value = name;
                        //dataGridView1.Rows[index].Cells["规格"].Value = gg;
                        //dataGridView1.Rows[index].Cells["生产企业"].Value = scqy;
                        //dataGridView1.Rows[index].Cells["库存"].Value = kc;
                        //dataGridView1.Rows[index].Cells["单位"].Value = dw;
                        //dataGridView1.Rows[index].Cells["药品批号"].Value = ph;
                        //dataGridView1.Rows[index].Cells["药品有效期"].Value = rq;
                        //dataGridView1.Rows[index].Cells["零售价"].Value = lsj;
                        //string hyj = String.Format("{0:F}", Convert.ToDouble(lsj) * Convert.ToDouble(hyjzk) / 100);
                        //dataGridView1.Rows[index].Cells["会员价"].Value = hyj;
                        //dataGridView1.Rows[index].Cells["数量"].Value = decimal.Parse(Count);
                        //dataGridView1.Rows[index].Cells["金额"].Value = (decimal.Parse(Count) * decimal.Parse(lsj)).ToString();
                        //dataGridView1.Rows[index].Cells["会员金额"].Value = (decimal.Parse(Count) * decimal.Parse(hyj)).ToString();
                        //dataGridView1.ClearSelection();
                        //textBox1.Focus();
                        //textBox1.Text = "";
                    }
                    catch (Exception E)
                    {
                    }
                }
            }
        }
    }
}