﻿using Dapper;
using Usido.CRM.Core.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Usido.CRM.Core.Repository
{
    public class BaseRepository<T, TKey> : IBaseRepository<T, TKey> where T : class
    {
        protected IDbConnection _dbConnection;

        #region 同步

        public int Delete(T entity)
        {
            return _dbConnection.Delete<T>(entity);
        }

        public int Delete(TKey id)
        {
            return _dbConnection.Delete<T>(id);
        }

        public int DeleteList(object whereConditions, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return _dbConnection.DeleteList<T>(whereConditions, transaction, commandTimeout);
        }

        public int DeleteList(string conditions, object parameters = null, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return _dbConnection.DeleteList<T>(conditions, parameters, transaction, commandTimeout);
        }

        public T Get(TKey id)
        {
            return _dbConnection.Get<T>(id);
        }

        public int Update(T entity)
        {
            return _dbConnection.Update<T>(entity);
        }

        public int RecordCount(string conditions = "", object parameters = null)
        {
            return _dbConnection.RecordCount<T>(conditions, parameters);
        }

        public int? Insert(T entity)
        {
            return _dbConnection.Insert<T>(entity);
        }

        public IEnumerable<T> GetList()
        {
            return _dbConnection.GetList<T>();
        }

        public IEnumerable<T> GetList(object whereConditions)
        {
            return _dbConnection.GetList<T>(whereConditions);
        }

        public IEnumerable<T> GetList(string conditions, object parameters)
        {
            return _dbConnection.GetList<T>(conditions, parameters);
        }

        public IEnumerable<T> GetListPaged(int pageNumber, int rowsPerPage, string conditions, string orderby, object parameters = null)
        {
            return _dbConnection.GetListPaged<T>(pageNumber, rowsPerPage, conditions, orderby, parameters);
        }

        #endregion 同步

        #region 异步

        public async Task<int> DeleteAsync(T entity)
        {
            return await _dbConnection.DeleteAsync<T>(entity);
        }

        public async Task<int> DeleteAsync(TKey id)
        {
            return await _dbConnection.DeleteAsync<T>(id);
        }

        public async Task<int> DeleteListAsync(object whereConditions, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return await _dbConnection.DeleteListAsync<T>(whereConditions, transaction, commandTimeout);
        }

        public async Task<int> DeleteListAsync(string conditions, object parameters = null, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            return await _dbConnection.DeleteListAsync<T>(conditions, parameters, transaction, commandTimeout);
        }

        public async Task<T> GetAsync(TKey id)
        {
            return await _dbConnection.GetAsync<T>(id);
        }

        public async Task<IEnumerable<T>> GetListAsync()
        {
            return await _dbConnection.GetListAsync<T>();
        }

        public async Task<IEnumerable<T>> GetListAsync(object whereConditions)
        {
            return await _dbConnection.GetListAsync<T>(whereConditions);
        }

        public async Task<IEnumerable<T>> GetListAsync(string conditions, object parameters)
        {
            return await _dbConnection.GetListAsync<T>(conditions, parameters);
        }

        public async Task<IEnumerable<T>> GetListPagedAsync(int pageNumber, int rowsPerPage, string conditions, string orderby, object parameters = null)
        {
            return await _dbConnection.GetListPagedAsync<T>(pageNumber, rowsPerPage, conditions, orderby, parameters);
        }

        public async Task<int?> InsertAsync(T entity)
        {
            return await _dbConnection.InsertAsync<T>(entity);
        }

        public async Task<int> RecordCountAsync(string conditions = "", object parameters = null)
        {
            return await _dbConnection.RecordCountAsync<T>(conditions, parameters);
        }

        public async Task<int> UpdateAsync(T entity)
        {
            return await _dbConnection.UpdateAsync<T>(entity);
        }

        #endregion 异步

        public void Dispose()
        {
            //TODO
        }
    }
}