﻿using System;
using System.Net;
using System.Windows.Forms;

namespace Usido.CRM.Core.ToolHelper
{
    public static class Tools
    {
        public static string GetPYChar(string c)
        {
            try
            {
                byte[] array = new byte[2];
                array = System.Text.Encoding.Default.GetBytes(c);
                int i = (short)(array[0] - '\0') * 256 + ((short)(array[1] - '\0'));
                if (i < 0xB0A1) return "*";
                if (i < 0xB0C5) return "A";
                if (i < 0xB2C1) return "B";
                if (i < 0xB4EE) return "C";
                if (i < 0xB6EA) return "D";
                if (i < 0xB7A2) return "E";
                if (i < 0xB8C1) return "F";
                if (i < 0xB9FE) return "G";
                if (i < 0xBBF7) return "H";
                if (i < 0xBFA6) return "J";
                if (i < 0xC0AC) return "K";
                if (i < 0xC2E8) return "L";
                if (i < 0xC4C3) return "M";
                if (i < 0xC5B6) return "N";
                if (i < 0xC5BE) return "O";
                if (i < 0xC6DA) return "P";
                if (i < 0xC8BB) return "Q";
                if (i < 0xC8F6) return "R";
                if (i < 0xCBFA) return "S";
                if (i < 0xCDDA) return "T";
                if (i < 0xCEF4) return "W";
                if (i < 0xD1B9) return "X";
                if (i < 0xD4D1) return "Y";
                if (i < 0xD7FA) return "Z";
                return "*";
            }
            catch (Exception)
            {
                return c.ToUpper();
            }
        }

        public static string GetIp()
        {
            try
            {
                //string hostName = Dns.GetHostName();
                //IPHostEntry localhost = "";// Dns.GetHostByName(hostName);
                //IPAddress localaddr = localhost.AddressList[0];
                //return localaddr.ToString();
            }
            catch
            {
                return "";
            }

            return "";
        }


        /// <summary>
        /// 获取订单的编号 根据类型
        /// </summary>
        /// <param name="type">订单类型 1销售订单 2销售退货 3采购订单 4采购退货 5库存盘点</param>
        /// <returns></returns>
        public static string GetOrderCode(int type)
        {
            string code = string.Empty;
            switch (type)
            {
                case 1:
                    code = "XSDD";
                    break;
                case 2:
                    code = "XSTH";
                    break;
                case 3:
                    code = "CGDD";
                    break;
                case 4:
                    code = "CGTH";
                    break;
                case 5:
                    code = "KCPD";
                    break;
                default:
                    break;
            }

            return code + DateTime.Now.ToString("yyyyMMddHHmmssff");
        }
         
    }
}