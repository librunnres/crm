/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2019/1/5 10:32:29                            */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('MemberInfo')
            and   type = 'U')
   drop table MemberInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SalesInfo')
            and   type = 'U')
   drop table SalesInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SalesOrder')
            and   type = 'U')
   drop table SalesOrder
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockInventory')
            and   type = 'U')
   drop table StockInventory
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockInventoryDetail')
            and   type = 'U')
   drop table StockInventoryDetail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockInventoryDetails')
            and   type = 'U')
   drop table StockInventoryDetails
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockProductBalance')
            and   type = 'U')
   drop table StockProductBalance
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockPutStorage')
            and   type = 'U')
   drop table StockPutStorage
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockPutStorageDetail')
            and   type = 'U')
   drop table StockPutStorageDetail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockShipment')
            and   type = 'U')
   drop table StockShipment
go

if exists (select 1
            from  sysobjects
           where  id = object_id('StockShipmentDetail')
            and   type = 'U')
   drop table StockShipmentDetail
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SupplyInfo')
            and   type = 'U')
   drop table SupplyInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SupplyOrder')
            and   type = 'U')
   drop table SupplyOrder
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysDepartment')
            and   type = 'U')
   drop table SysDepartment
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysDict')
            and   type = 'U')
   drop table SysDict
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysDictType')
            and   type = 'U')
   drop table SysDictType
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysLog')
            and   type = 'U')
   drop table SysLog
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysMenu')
            and   type = 'U')
   drop table SysMenu
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysProduct')
            and   type = 'U')
   drop table SysProduct
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysRole')
            and   type = 'U')
   drop table SysRole
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysRoleRight')
            and   type = 'U')
   drop table SysRoleRight
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysSupplier')
            and   type = 'U')
   drop table SysSupplier
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SysUser')
            and   type = 'U')
   drop table SysUser
go

/*==============================================================*/
/* Table: MemberInfo                                            */
/*==============================================================*/
create table MemberInfo (
   Id                   int                  null
      constraint CKC_ID_MEMBERIN check (Id is null or (Id >= 10000)),
   姓名                   varchar(50)          null,
   手机号                  varchar(50)          null,
   IdNumber             varchar(50)          null,
   Sex                  bit                  not null,
   Birthdate            datetime             null,
   MemberCard           varchar(500)         null
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('MemberInfo') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'MemberInfo' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '会员信息', 
   'user', @CurrentUser, 'table', 'MemberInfo'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '姓名')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', '姓名'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '姓名',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', '姓名'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '手机号')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', '手机号'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '手机号',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', '手机号'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IdNumber')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'IdNumber'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '身份证号',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'IdNumber'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sex')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Sex'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '性别',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Sex'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Birthdate')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Birthdate'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '出生日期',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'Birthdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('MemberInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'MemberCard')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'MemberCard'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '会员卡号',
   'user', @CurrentUser, 'table', 'MemberInfo', 'column', 'MemberCard'
go

/*==============================================================*/
/* Table: SalesInfo                                             */
/*==============================================================*/
create table SalesInfo (
   Id                   int                  identity,
   constraint PK_SALESINFO primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SalesInfo') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SalesInfo' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '销售明细表', 
   'user', @CurrentUser, 'table', 'SalesInfo'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SalesInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SalesInfo', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SalesInfo', 'column', 'Id'
go

/*==============================================================*/
/* Table: SalesOrder                                            */
/*==============================================================*/
create table SalesOrder (
   Id                   int                  identity,
   constraint PK_SALESORDER primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SalesOrder') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SalesOrder' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '销售主表', 
   'user', @CurrentUser, 'table', 'SalesOrder'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SalesOrder')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SalesOrder', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SalesOrder', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockInventory                                        */
/*==============================================================*/
create table StockInventory (
   Id                   int                  identity,
   constraint PK_STOCKINVENTORY primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockInventory') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockInventory' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '库存盘点主表', 
   'user', @CurrentUser, 'table', 'StockInventory'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockInventory')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockInventory', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockInventory', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockInventoryDetail                                  */
/*==============================================================*/
create table StockInventoryDetail (
   Id                   int                  identity,
   constraint PK_STOCKINVENTORYDETAIL primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockInventoryDetail') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockInventoryDetail' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '库存盘点明细表', 
   'user', @CurrentUser, 'table', 'StockInventoryDetail'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockInventoryDetail')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockInventoryDetail', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockInventoryDetail', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockInventoryDetails                                 */
/*==============================================================*/
create table StockInventoryDetails (
   Id                   int                  identity,
   constraint PK_STOCKINVENTORYDETAILS primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockInventoryDetails') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockInventoryDetails' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '实物明细账', 
   'user', @CurrentUser, 'table', 'StockInventoryDetails'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockInventoryDetails')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockInventoryDetails', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockInventoryDetails', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockProductBalance                                   */
/*==============================================================*/
create table StockProductBalance (
   Id                   int                  identity,
   constraint PK_STOCKPRODUCTBALANCE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockProductBalance') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockProductBalance' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '实物结存表', 
   'user', @CurrentUser, 'table', 'StockProductBalance'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockProductBalance')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockProductBalance', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockProductBalance', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockPutStorage                                       */
/*==============================================================*/
create table StockPutStorage (
   Id                   int                  identity,
   constraint PK_STOCKPUTSTORAGE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockPutStorage') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockPutStorage' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '入库主表', 
   'user', @CurrentUser, 'table', 'StockPutStorage'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockPutStorage')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockPutStorage', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockPutStorage', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockPutStorageDetail                                 */
/*==============================================================*/
create table StockPutStorageDetail (
   Id                   int                  identity,
   constraint PK_STOCKPUTSTORAGEDETAIL primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockPutStorageDetail') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockPutStorageDetail' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '入库明细表', 
   'user', @CurrentUser, 'table', 'StockPutStorageDetail'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockPutStorageDetail')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockPutStorageDetail', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockPutStorageDetail', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockShipment                                         */
/*==============================================================*/
create table StockShipment (
   Id                   int                  identity,
   constraint PK_STOCKSHIPMENT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockShipment') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockShipment' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '出库主表', 
   'user', @CurrentUser, 'table', 'StockShipment'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockShipment')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockShipment', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockShipment', 'column', 'Id'
go

/*==============================================================*/
/* Table: StockShipmentDetail                                   */
/*==============================================================*/
create table StockShipmentDetail (
   Id                   int                  identity,
   constraint PK_STOCKSHIPMENTDETAIL primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('StockShipmentDetail') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'StockShipmentDetail' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '出库明细表', 
   'user', @CurrentUser, 'table', 'StockShipmentDetail'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('StockShipmentDetail')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'StockShipmentDetail', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'StockShipmentDetail', 'column', 'Id'
go

/*==============================================================*/
/* Table: SupplyInfo                                            */
/*==============================================================*/
create table SupplyInfo (
   Id                   int                  identity,
   constraint PK_SUPPLYINFO primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SupplyInfo') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SupplyInfo' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '采购明细表', 
   'user', @CurrentUser, 'table', 'SupplyInfo'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SupplyInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SupplyInfo', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SupplyInfo', 'column', 'Id'
go

/*==============================================================*/
/* Table: SupplyOrder                                           */
/*==============================================================*/
create table SupplyOrder (
   Id                   int                  identity,
   constraint PK_SUPPLYORDER primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SupplyOrder') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SupplyOrder' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '采购主表', 
   'user', @CurrentUser, 'table', 'SupplyOrder'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SupplyOrder')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SupplyOrder', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SupplyOrder', 'column', 'Id'
go

/*==============================================================*/
/* Table: SysDepartment                                         */
/*==============================================================*/
create table SysDepartment (
   Id                   int                  identity
      constraint CKC_ID_SYSDEPAR check (Id >= 10000),
   DeptName             varchar(50)          null,
   Remark               varchar(50)          null,
   State                bit                  not null default 1,
   constraint PK_SYSDEPARTMENT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysDepartment') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysDepartment' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '部门表', 
   'user', @CurrentUser, 'table', 'SysDepartment'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDepartment')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDepartment')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'DeptName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'DeptName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '部门名称',
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'DeptName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDepartment')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'Remark'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDepartment')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态',
   'user', @CurrentUser, 'table', 'SysDepartment', 'column', 'State'
go

/*==============================================================*/
/* Table: SysDict                                               */
/*==============================================================*/
create table SysDict (
   Id                   int                  identity,
   DictName             varchar(50)          null,
   DictTypeId           int                  not null,
   Remark               varchar(50)          null,
   OrderBy              int                  not null default 0,
   State                bit                  not null default 1,
   constraint PK_SYSDICT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysDict') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysDict' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '数据字典信息', 
   'user', @CurrentUser, 'table', 'SysDict'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'DictName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'DictName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '字典名称',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'DictName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'DictTypeId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'DictTypeId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '所属类型',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'DictTypeId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'Remark'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OrderBy')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'OrderBy'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '排序字段',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'OrderBy'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDict')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态 正常 禁用',
   'user', @CurrentUser, 'table', 'SysDict', 'column', 'State'
go

/*==============================================================*/
/* Table: SysDictType                                           */
/*==============================================================*/
create table SysDictType (
   Id                   int                  identity,
   TypeName             varchar(50)          null,
   Remark               varchar(200)         null,
   constraint PK_SYSDICTTYPE primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysDictType') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysDictType' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '数据字典类型', 
   'user', @CurrentUser, 'table', 'SysDictType'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDictType')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDictType')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TypeName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'TypeName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '字典类型名称',
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'TypeName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysDictType')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysDictType', 'column', 'Remark'
go

/*==============================================================*/
/* Table: SysLog                                                */
/*==============================================================*/
create table SysLog (
   Id                   int                  identity,
   UserId               int                  not null,
   CreateTime           datetime             null default getdate(),
   OperContent          varchar(max)         null,
   OperIp               varchar(50)          null,
   Remark               varchar(200)         null,
   constraint PK_SYSLOG primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysLog') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysLog' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '操作日志表', 
   'user', @CurrentUser, 'table', 'SysLog'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'UserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户Id',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'UserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '操作时间',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperContent')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'OperContent'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '操作内容',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'OperContent'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperIp')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'OperIp'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '操作ip',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'OperIp'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysLog')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysLog', 'column', 'Remark'
go

/*==============================================================*/
/* Table: SysMenu                                               */
/*==============================================================*/
create table SysMenu (
   Id                   int                  identity,
   MenuName             varchar(50)          null,
   Icon                 varchar(50)          null,
   ParentId             int                  not null default 0,
   State                bit                  not null default 1,
   OrderBy              int                  not null default 0,
   constraint PK_SYSMENU primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysMenu') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysMenu' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '菜单信息', 
   'user', @CurrentUser, 'table', 'SysMenu'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '菜单Id',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'MenuName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'MenuName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '菜单名称',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'MenuName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Icon')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'Icon'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '菜单图标',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'Icon'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ParentId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'ParentId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '上级菜单Id',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'ParentId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态 正常禁用',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'State'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysMenu')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OrderBy')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'OrderBy'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '排序字段',
   'user', @CurrentUser, 'table', 'SysMenu', 'column', 'OrderBy'
go

/*==============================================================*/
/* Table: SysProduct                                            */
/*==============================================================*/
create table SysProduct (
   Id                   int                  identity
      constraint CKC_ID_SYSPRODU check (Id >= 10000),
   ProductName          varchar(200)         null,
   ProductCode          varchar(200)         null,
   Specification        varchar(200)         null,
   MarketingUnit        varchar(200)         null,
   ManufacturingEnterprise varchar(200)         null,
   CreateTime           datetime             not null,
   CreateUserId         int                  not null,
   LastUpdateTime       datetime             null,
   LastUpdateUserId     int                  null,
   State                bit                  not null default 1,
   constraint PK_SYSPRODUCT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysProduct') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysProduct' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '商品表', 
   'user', @CurrentUser, 'table', 'SysProduct'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ProductName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ProductName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '商品名称',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ProductName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ProductCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ProductCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '商品简码',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ProductCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Specification')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'Specification'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '商品规格',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'Specification'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'MarketingUnit')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'MarketingUnit'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '销售单位',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'MarketingUnit'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ManufacturingEnterprise')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ManufacturingEnterprise'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '生产企业',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'ManufacturingEnterprise'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '添加时间',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '添加人',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastUpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'LastUpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '最后一次修改时间',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'LastUpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastUpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'LastUpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'LastUpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysProduct')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态',
   'user', @CurrentUser, 'table', 'SysProduct', 'column', 'State'
go

/*==============================================================*/
/* Table: SysRole                                               */
/*==============================================================*/
create table SysRole (
   角色Id                 int                  identity,
   RoleName             varchar(50)          null,
   Remark               varchar(50)          null,
   State                bit                  not null default 1,
   constraint PK_SYSROLE primary key (角色Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysRole') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysRole' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '角色表', 
   'user', @CurrentUser, 'table', 'SysRole'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '角色Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRole', 'column', '角色Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '角色Id',
   'user', @CurrentUser, 'table', 'SysRole', 'column', '角色Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RoleName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'RoleName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '名称',
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'RoleName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'Remark'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRole')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态',
   'user', @CurrentUser, 'table', 'SysRole', 'column', 'State'
go

/*==============================================================*/
/* Table: SysRoleRight                                          */
/*==============================================================*/
create table SysRoleRight (
   Id                   int                  identity,
   RoleId               int                  not null,
   MenuId               int                  not null,
   constraint PK_SYSROLERIGHT primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysRoleRight') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysRoleRight' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '角色权限', 
   'user', @CurrentUser, 'table', 'SysRoleRight'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRoleRight')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRoleRight')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RoleId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'RoleId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '角色id',
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'RoleId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysRoleRight')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'MenuId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'MenuId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '菜单Id',
   'user', @CurrentUser, 'table', 'SysRoleRight', 'column', 'MenuId'
go

/*==============================================================*/
/* Table: SysSupplier                                           */
/*==============================================================*/
create table SysSupplier (
   Id                   int                  identity
      constraint CKC_ID_SYSSUPPL check (Id >= 10000),
   CompanyName          varchar(200)         null,
   CompanyCode          varchar(200)         null,
   LegalPerson          varchar(200)         null,
   Contacts             varchar(200)         null,
   Phone                varchar(200)         null,
   CompanyAddress       varchar(200)         null,
   CreateTime           datetime             not null,
   CreateUserId         int                  not null,
   LastUpdateTime       datetime             null,
   LastUpdateUserId     int                  null,
   State                bit                  not null default 1,
   constraint PK_SYSSUPPLIER primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysSupplier') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysSupplier' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '供应商', 
   'user', @CurrentUser, 'table', 'SysSupplier'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CompanyName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '企业名称',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CompanyCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '企业简码',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LegalPerson')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LegalPerson'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '企业法人',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LegalPerson'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Contacts')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Contacts'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '联系人',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Contacts'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Phone')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Phone'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '电话',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'Phone'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CompanyAddress')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyAddress'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '企业地址',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CompanyAddress'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '添加时间',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CreateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '添加人',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'CreateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastUpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LastUpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '最后一次修改时间',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LastUpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastUpdateUserId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LastUpdateUserId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '修改人',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'LastUpdateUserId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysSupplier')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态',
   'user', @CurrentUser, 'table', 'SysSupplier', 'column', 'State'
go

/*==============================================================*/
/* Table: SysUser                                               */
/*==============================================================*/
create table SysUser (
   Id                   int                  identity
      constraint CKC_ID_SYSUSER check (Id >= 10000),
   UserName             varchar(50)          null,
   Password             varchar(50)          null,
   UserLogogramCode     varchar(50)          null,
   IdNumber             varchar(50)          null,
   Sex                  bit                  not null,
   Birthdate            datetime             null,
   DeptId               int                  not null,
   TakingWorkTime       datetime             null,
   EqucationId          int                  not null,
   Contact              varchar(50)          null,
   CreateTime           datetime             null,
   UpdateTime           datetime             null,
   LastLoginTime        datetime             null,
   State                bit                  not null default 1,
   RoleId               int                  not null,
   Remark               varchar(50)          null,
   constraint PK_SYSUSER primary key (Id)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('SysUser') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'SysUser' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '用户表', 
   'user', @CurrentUser, 'table', 'SysUser'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Id')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Id'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'Id',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Id'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UserName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UserName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户名',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UserName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Password')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Password'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '登录密码',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Password'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UserLogogramCode')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UserLogogramCode'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户简码',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UserLogogramCode'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IdNumber')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'IdNumber'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '身份证号',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'IdNumber'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Sex')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Sex'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '性别',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Sex'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Birthdate')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Birthdate'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '出生日期',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Birthdate'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'DeptId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'DeptId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '部门',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'DeptId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'TakingWorkTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'TakingWorkTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '入职日期',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'TakingWorkTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'EqucationId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'EqucationId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '学历',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'EqucationId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Contact')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Contact'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '联系方式',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Contact'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'CreateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'CreateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '创建时间',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'CreateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '最后一次修改时间',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'UpdateTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastLoginTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'LastLoginTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '最后一次登录时间',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'LastLoginTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'State')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'State'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '状态',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'State'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RoleId')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'RoleId'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '角色',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'RoleId'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SysUser')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Remark')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Remark'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '备注',
   'user', @CurrentUser, 'table', 'SysUser', 'column', 'Remark'
go

