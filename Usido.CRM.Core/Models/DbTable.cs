﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usido.CRM.Core.Models
{
    public class DbTable
    {
        public string TableName { get; set; }

        public string TableComment { get; set; }

        public virtual List<DbTableColumn> Columns { get; set; } = new List<DbTableColumn>();
    }
}
