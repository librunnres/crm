﻿using System;

namespace Usido.CRM.Core.Options
{
    /// <summary>
    /// 代码生成配置项
    /// </summary>
    public class CodeGenerateOption : DbOption
    {
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 代码生成时间
        /// </summary>
        public string GeneratorTime { get; set; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        /// <summary>
        /// 文件输出路径
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// 实体命名空间
        /// </summary>
        public string ModelsNamespace { get; set; }

        /// <summary>
        /// 仓储层接口命名空间
        /// </summary>
        public string IRepositoryNamespace { get; set; }

        /// <summary>
        /// 仓储层命名空间
        /// </summary>
        public string RepositoryNamespace { get; set; }

        /// <summary>
        /// 项目命名空间
        /// </summary>
        public string ProjectNamespace { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }
    }
}