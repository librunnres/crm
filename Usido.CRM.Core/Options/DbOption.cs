﻿using Usido.CRM.Core.Models;

namespace Usido.CRM.Core.Options
{
    /// <summary>
    /// 数据库连接选项
    /// </summary>
    public class DbOption
    {
        /// <summary>
        /// 数据连接字符串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 数据库类型
        /// </summary>
        public string DbType { get; set; }
    }

    public static class DbOptionData
    {
        public static string DbType = DatabaseType.SqlServer.ToString();

        //public static string
        //    ConnectionString =
        //        "Data Source=123.207.60.155;Initial Catalog=UsidoCRM;User ID=sa;Password=radial_soft1;Persist Security Info=True;Max Pool Size=50;Min Pool Size=0;Connection Lifetime=300;";   
        public static string
           ConnectionString =
               "Data Source=123.207.60.155;Initial Catalog=UsidoCRMTest;User ID=sa;Password=radial_soft1;Persist Security Info=True;Max Pool Size=50;Min Pool Size=0;Connection Lifetime=300;";
    }
         
}