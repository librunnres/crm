﻿using Usido.CRM.Core.DbHelper;
using Usido.CRM.Core.Extensions;
using Usido.CRM.Core.Models;
using Usido.CRM.Core.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Usido.CRM.Core.CodeGenerator
{
    /// <summary>
    /// 代码生成类
    /// </summary>
    public class CodeGenerator
    {
        private readonly string Delimiter = "\\";

        private static CodeGenerateOption _options;

        public CodeGenerator(CodeGenerateOption options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            _options = options;
            if (_options.ConnectionString.IsNullOrWhiteSpace())
                throw new ArgumentNullException("未指定数据库连接字符串");
            if (_options.DbType.IsNullOrWhiteSpace())
                throw new ArgumentNullException("未指定数据库类型");

            var path = AppDomain.CurrentDomain.BaseDirectory;

            if (_options.OutputPath.IsNullOrWhiteSpace())
                _options.OutputPath = path;

            var flag = path.IndexOf("/bin");
            if (flag > 0)
                Delimiter = "/";//如果可以取到值，修改分割符
        }

        /// <summary>
        /// 通过数据库生成代码
        /// </summary>
        /// <param name="isCoveredExsited"></param>
        public void GenerateTemplateCodesFromDatabase(bool isCoveredExsited = true)
        {
            DatabaseType dbType = ConnectionFactory.GetDataBaseType(_options.DbType);

            List<DbTable> tables = new List<DbTable>();

            using (var db = ConnectionFactory.CreateConnection(dbType, _options.ConnectionString))
            {
                tables = db.GetCurrentDatabaseTableList(dbType);
            }

            if (tables != null && tables.Any())
            {
                foreach (var table in tables)
                {
                    //生成实体
                    GenerateEntity(table, isCoveredExsited);

                    if (table.Columns.Any(c => c.IsPrimaryKey))
                    {
                        var primaryKeyTypeName = table.Columns.First(m => m.IsPrimaryKey).CSharpType;
                        //生成仓储接口
                        GenerateIRepository(table, primaryKeyTypeName, isCoveredExsited);
                        //生成仓储
                        GenerateRepository(table, primaryKeyTypeName, isCoveredExsited);
                    }
                }
            }
        }

        /// <summary>
        /// 生成仓储
        /// </summary>
        /// <param name="table"></param>
        /// <param name="primaryKeyTypeName"></param>
        /// <param name="isCoveredExsited"></param>
        private void GenerateRepository(DbTable table, string primaryKeyTypeName, bool isCoveredExsited)
        {
            var repositoryPath = _options.OutputPath + Delimiter + $"{_options.ProjectNamespace}.Repository";
            if (!Directory.Exists(repositoryPath))
            {
                Directory.CreateDirectory(repositoryPath);
            }
            var fullPath = repositoryPath + Delimiter + table.TableName + "Repository.cs";
            if (File.Exists(fullPath) && !isCoveredExsited)
                return;
            var content = ReadTemplate("RepositoryTemplate.txt");
            content = content.Replace("{Comment}", table.TableComment)
                .Replace("{Author}", _options.Author)
                .Replace("{GeneratorTime}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                .Replace("{RepositoryNamespace}", _options.RepositoryNamespace)
                .Replace("{ModelName}", table.TableName)
                .Replace("{ProjectNamespance}", _options.ProjectNamespace)
                .Replace("{ProjectName}", _options.ProjectName)
                .Replace("{KeyTypeName}", primaryKeyTypeName)
                .Replace("\r\n\r\n\r\n", "\r\n");
            WriteAndSave(fullPath, content);
        }

        /// <summary>
        /// 生成仓储接口
        /// </summary>
        /// <param name="table"></param>
        /// <param name="primaryKeyTypeName"></param>
        /// <param name="isCoveredExsited"></param>
        private void GenerateIRepository(DbTable table, string primaryKeyTypeName, bool isCoveredExsited)
        {
            var iRepositoryPath = _options.OutputPath + Delimiter + $"{_options.ProjectNamespace}.IRepository";
            if (!Directory.Exists(iRepositoryPath))
            {
                Directory.CreateDirectory(iRepositoryPath);
            }
            var fullPath = iRepositoryPath + Delimiter + "I" + table.TableName + "Repository.cs";
            if (File.Exists(fullPath) && !isCoveredExsited)
                return;
            var content = ReadTemplate("IRepositoryTemplate.txt");
            content = content.Replace("{Comment}", table.TableComment)
                .Replace("{Author}", _options.Author)
                .Replace("{GeneratorTime}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                .Replace("{IRepositoryNamespace}", _options.IRepositoryNamespace)
                .Replace("{ModelName}", table.TableName)
                .Replace("{ProjectNamespance}", _options.ProjectNamespace)
                .Replace("{ProjectName}", _options.ProjectName)
                .Replace("{KeyTypeName}", primaryKeyTypeName)
                .Replace("\r\n\r\n\r\n", "\r\n");
            WriteAndSave(fullPath, content);
        }

        /// <summary>
        /// 生成实体
        /// </summary>
        /// <param name="table"></param>
        /// <param name="isCoveredExsited"></param>
        private void GenerateEntity(DbTable table, bool isCoveredExsited)
        {
            var modelPath = _options.OutputPath + Delimiter + $"{_options.ProjectNamespace}.Models";
            if (!Directory.Exists(modelPath))
            {
                Directory.CreateDirectory(modelPath);
            }

            var fullPath = modelPath + Delimiter + table.TableName + ".cs";
            if (File.Exists(fullPath) && !isCoveredExsited)
                return;

            var pkTypeName = table.Columns.First(m => m.IsPrimaryKey).CSharpType;
            var sb = new StringBuilder();
            foreach (var column in table.Columns)
            {
                var tmp = GenerateEntityProperty(table.TableName, column);
                sb.AppendLine(tmp);
            }
            var content = ReadTemplate("ModelTemplate.txt");
            content = content.Replace("{GeneratorTime}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                .Replace("{ModelsNamespace}", _options.ModelsNamespace)
                .Replace("{Author}", _options.Author)
                .Replace("{Comment}", table.TableComment)
                .Replace("{ModelName}", table.TableName)
                .Replace("{ProjectNamespance}", _options.ProjectNamespace)
                .Replace("{ProjectName}", _options.ProjectName)
                .Replace("{ModelProperties}", sb.ToString())
                .Replace("\r\n\r\n\r\n", "\r\n");
            WriteAndSave(fullPath, content);
        }

        /// <summary>
        /// 生成属性
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="column">列</param>
        /// <returns></returns>
        private static string GenerateEntityProperty(string tableName, DbTableColumn column)
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(column.Comment))
            {
                sb.AppendLine("        /// <summary>");
                sb.AppendLine("        /// " + column.Comment);
                sb.AppendLine("        /// </summary>");
            }
            if (column.IsPrimaryKey)
            {
                sb.AppendLine("        [Key]");
                sb.AppendLine($"        public {column.CSharpType} Id " + "{get;set;}");
            }
            else
            {
                if (!column.IsNullable)
                {
                    sb.AppendLine("        [Required]");
                }

                var colType = column.CSharpType;
                if (colType.ToLower() != "string" && colType.ToLower() != "byte[]" && colType.ToLower() != "object" &&
                    column.IsNullable)
                {
                    colType = colType + "?";
                }

                sb.AppendLine($"        public {colType} {column.ColName} " + "{get;set;}");
            }

            return sb.ToString();
        }

        /// <summary>
        /// 从代码模板中读取内容
        /// </summary>
        /// <param name="templateName">模板名称，应包括文件扩展名称。比如：template.txt</param>
        /// <returns></returns>
        private string ReadTemplate(string templateName)
        {
            var currentAssembly = Assembly.GetExecutingAssembly();
            var content = string.Empty;
            using (var stream = currentAssembly.GetManifestResourceStream($"{currentAssembly.GetName().Name}.CodeTemplate.{templateName}"))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream))
                    {
                        content = reader.ReadToEnd();
                    }
                }
            }
            return content;
        }

        /// <summary>
        /// 写文件
        /// </summary>
        /// <param name="fileName">文件完整路径</param>
        /// <param name="content">内容</param>
        private static void WriteAndSave(string fileName, string content)
        {
            //实例化一个文件流--->与写入文件相关联
            using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                //实例化一个StreamWriter-->与fs相关联
                using (var sw = new StreamWriter(fs))
                {
                    //开始写入
                    sw.Write(content);
                    //清空缓冲区
                    sw.Flush();
                    //关闭流
                    sw.Close();
                    fs.Close();
                }
            }
        }
    }
}