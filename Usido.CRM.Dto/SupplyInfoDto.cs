﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    public class SupplyInfoDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 销售主表Id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 药品到期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }

        #region 扩展属性

        /// <summary>
        /// 品名
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public decimal TotalMoney { get; set; }

        /// <summary>
        /// 可退货数量
        /// </summary>
        public decimal QuantityReturned { get; set; }
        #endregion 扩展属性
    }
}