﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    public class SupplyReportDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public decimal Quantity { get; set; }

        /// <summary>
        /// 有效期至
        /// </summary>
        [Required]
        public DateTime? DateExpiry { get; set; }

        /// <summary>
        /// 采购主表id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品简码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 下单时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 药品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 供应商
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public decimal TotalMoney { get; set; }
    }
}