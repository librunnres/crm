﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 实物结存表
    /// </summary>
    public class StockProductBalanceDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 结存数量
        /// </summary>
        [Required]
        public decimal StockQuantity { get; set; }

        /// <summary>
        /// 成本价
        /// </summary>
        [Required]
        public decimal CostPrice { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [Required]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 预留库存数
        /// </summary>
        public decimal? ReservedStock { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 总金额
        /// </summary>
        public decimal TotalMoney { get; set; }
    }
}