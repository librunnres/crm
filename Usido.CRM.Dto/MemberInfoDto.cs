﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usido.CRM.Dto
{
    [Table("MemberInfo")]
    public class MemberInfoDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string IdNumber { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? Birthdate { get; set; }

        /// <summary>
        /// 会员卡号
        /// </summary>
        public string MemberCard { get; set; }

        /// <summary>
        /// 理疗卡号
        /// </summary>
        public string LLCard { get; set; }

        /// <summary>
        /// 账户余额
        /// </summary>

        public decimal AccountMoney { get; set; }

        /// <summary>
        /// 理疗账户余额
        /// </summary>

        public decimal LLAccountMoney { get; set; }

        /// <summary>
        /// 添加日期
        /// </summary>

        public DateTime CreateTime { get; set; }
    }
}
