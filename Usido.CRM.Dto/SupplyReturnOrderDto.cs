﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 采购退货主表
    /// </summary>
    public class SupplyReturnOrderDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        [Required]
        public int CreateUserId { get; set; }

        /// <summary>
        /// 出库人
        /// </summary>
        public int? PutUserId { get; set; }

        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? PutTime { get; set; }

        /// <summary>
        /// 状态 0未出库 1已出库
        /// </summary>
        [Required]
        public int State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 原始采购订单号
        /// </summary>
        [Required]
        public int SupplyOrderId { get; set; }

        #region 扩展属性

        /// <summary>
        /// 制单人姓名
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 入库人姓名
        /// </summary>
        public string PutUserName { get; set; }

        #endregion 扩展属性
    }
}