﻿using System;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 库存信息
    /// </summary>
    public class StockInfoDto
    {
        /// <summary>
        /// 实物结存表Id 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// 商品简码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 可用库存
        /// </summary>
        public decimal AvailableQuantity { get; set; }

        /// <summary>
        /// 零售价
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// 会员折扣
        /// </summary>
        public decimal MemberDiscount { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 过期日期
        /// </summary>
        public DateTime? DateExpiry { get; set; }

    }
}