﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 销售订单列表实体
    /// </summary>
    public class SalesOrderDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 会员id
        /// </summary>
        public int? MemberId { get; set; }

        /// <summary>
        /// 业务员Id
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 订单类型 0默认是业务员打的销售订单 1表示处方转的订单
        /// </summary>
        [Required]
        public int OrderType { get; set; }

        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? ShipTime { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Required]
        public int State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 会员姓名
        /// </summary>
        public string MemberName { get; set; }

        /// <summary>
        /// 业务员姓名
        /// </summary>
        public string UserName { get; set; }
    }
}