﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售退货明细表
    /// </summary>
    public class SalesReturnInfoDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// 原始销售订单明细id
        /// </summary>
        public int? OriginalSalesInforId { get; set; }

        /// <summary>
        /// 销售退货主表id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 药品到期时间
        /// </summary>
        public DateTime? DateExpiry { get; set; }

        #region 扩展属性

        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品简码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 零售价
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// 会员折扣
        /// </summary>
        public decimal MemberDiscount { get; set; }

        /// <summary>
        /// 会员价格
        /// </summary>
        public decimal MemberPrice { get; set; }

        /// <summary>
        /// 可退货数量
        /// </summary>
        public decimal QuantityReturned { get; set; }

        #endregion 扩展属性
    }
}