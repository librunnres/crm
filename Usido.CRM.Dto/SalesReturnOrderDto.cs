﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 销售退货主表
    /// </summary>
    public class SalesReturnOrderDto
    { /// <summary>
      /// Id
      /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 业务员Id
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTime? PutTime { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public int? State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 原始销售订单号
        /// </summary>
        public int? OriginalSalesOrderId { get; set; }

        /// <summary>
        /// 业务员姓名
        /// </summary>
        public string UserName { get; set; }
    }
}