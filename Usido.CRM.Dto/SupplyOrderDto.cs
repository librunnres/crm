﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 李宝
    /// 采购主表
    /// </summary>
    public class SupplyOrderDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 供应商Id
        /// </summary>
        [Required]
        public int SupplierId { get; set; }

        /// <summary>
        /// 制单时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 制单人
        /// </summary>
        [Required]
        public int CreateUserId { get; set; }

        /// <summary>
        /// 入库人
        /// </summary>
        public int? PutUserId { get; set; }

        /// <summary>
        /// 入库时间
        /// </summary>
        public DateTime? PutTime { get; set; }

        /// <summary>
        /// 状态 0未入库 1已入库
        /// </summary>
        [Required]
        public int State { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        #region 扩展属性

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// 制单人姓名
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 入库人姓名
        /// </summary>
        public string PutUserName { get; set; }

        public string StateText { get; set; }

        #endregion 扩展属性
    }
}