﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 库存盘点明细表
    /// </summary>
    public class StockInventoryDetailDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        /// 主表id
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 库存数
        /// </summary>
        public decimal? StockQuantity { get; set; }

        /// <summary>
        /// 实盘数
        /// </summary>
        public decimal? ActualQuantity { get; set; }

        /// <summary>
        /// 盘盈数量
        /// </summary>
        public decimal? InventorySurplusQuantity { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public DateTime? DateExpiry { get; set; }

        /// <summary>
        /// 品名
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }
    }
}