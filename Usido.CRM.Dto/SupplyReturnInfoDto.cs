﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Usido.CRM.Dto
{
    /// <summary>
    /// 李宝
    /// 2019-01-06 14:31:59
    /// 采购退货明细表
    /// </summary>
    public class SupplyReturnInfoDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// 批号
        /// </summary>
        public string BatchNumber { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Required]
        public decimal Quantity { get; set; }

        /// <summary>
        /// 原始采购订单号
        /// </summary>
        [Required]
        public int OriginalSupplyInforId { get; set; }

        /// <summary>
        /// 采购主表id
        /// </summary>
        public int? OrderId { get; set; }

        /// <summary>
        /// 有效期至
        /// </summary>
        [Required]
        public DateTime? DateExpiry { get; set; }

        #region 扩展属性

        /// <summary>
        /// 品名
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 合计金额
        /// </summary>
        public decimal TotalMoney { get; set; }

        /// <summary>
        /// 可退货数量
        /// </summary>
        public decimal QuantityReturned { get; set; }
        #endregion 扩展属性
    }
}