﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usido.CRM.Dto
{
    [Table("SysProduct")]
    public class SysProductDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// 商品简码
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// 商品规格
        /// </summary>
        public string Specification { get; set; }

        /// <summary>
        /// 销售单位
        /// </summary>
        public string MarketingUnit { get; set; }

        /// <summary>
        /// 生产企业
        /// </summary>
        public string ManufacturingEnterprise { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        [Required]
        public int CreateUserId { get; set; }

        /// <summary>
        /// 最后一次修改时间
        /// </summary>
        public DateTime? LastUpdateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public int? LastUpdateUserId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public bool State { get; set; }

        /// <summary>
        /// 批注问号
        /// </summary>

        public string Notation { get; set; }

        #region 扩展属性-添加人姓名
        public string UserName { get; set; }

        /// <summary>
        /// 零售价
        /// </summary>
        public decimal RetailPrice { get; set; }

        /// <summary>
        /// 会员折扣
        /// </summary>
        public decimal MemberDiscount { get; set; }
        #endregion
    }
}
