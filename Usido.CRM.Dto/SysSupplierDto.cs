﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Usido.CRM.Dto
{
    [Table("SysSupplier")]
    public class SysSupplierDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 企业简码
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// 企业法人
        /// </summary>
        public string LegalPerson { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 企业地址
        /// </summary>
        public string CompanyAddress { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 添加人
        /// </summary>
        [Required]
        public int CreateUserId { get; set; }

        /// <summary>
        /// 最后一次修改时间
        /// </summary>
        public DateTime? LastUpdateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public int? LastUpdateUserId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Required]
        public bool State { get; set; }

        #region 扩展属性-添加人
        public string UserName { get; set; }
        #endregion
    }
}
