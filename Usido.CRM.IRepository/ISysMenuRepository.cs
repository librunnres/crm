/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：菜单信息
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISysMenuRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;

namespace Usido.CRM.IRepository
{
    public interface ISysMenuRepository : IBaseRepository<SysMenu, int>
    {
        /// <summary>
        /// 通过角色id获取用户的菜单权限
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        IEnumerable<SysMenu> GetMenuListByRoleId(int roleId);
    }
}