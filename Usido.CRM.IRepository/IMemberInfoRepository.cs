/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：会员信息                                                    
*│　作    者：李宝                                              
*│　版    本：1.0 模板代码自动生成                                              
*│　创建时间：2019-01-05 10:49:56                           
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository                                   
*│　接口名称： IMemberInfoRepository                                      
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using Usido.CRM.Dto;
using System.Collections.Generic;

namespace Usido.CRM.IRepository
{
    public interface IMemberInfoRepository : IBaseRepository<MemberInfo, int>
    {
        IEnumerable<MemberInfoDto> GetMemberInfoList(string where);
        int UpdateAccountMon(string id,string type,decimal money);

        decimal GetMoney(string where);
    }

}