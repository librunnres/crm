/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：用户表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISysUserRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;

namespace Usido.CRM.IRepository
{
    public interface ISysUserRepository : IBaseRepository<SysUser, int>
    {
        /// <summary>
        /// 获取用户信息，带部门和角色
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IEnumerable<SysUserDto> GetUserList(string where);

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="password">新密码</param>
        /// <returns></returns>
        bool UpdatePassword(int userId, string password);
    }
}