using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售退货主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISalesReturnOrderRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Dto;
using Usido.CRM.Models;

namespace Usido.CRM.IRepository
{
    public interface ISalesReturnOrderRepository : IBaseRepository<SalesReturnOrder, int>
    {
        /// <summary>
        /// 添加销售退货订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        int AddSalesReturn(SalesReturnOrder order, List<SalesReturnInfo> orderInfoList);

        /// <summary>
        /// 修改销售退货订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        bool UpdateSalesReturn(SalesReturnOrder order, List<SalesReturnInfo> orderInfoList);

        /// <summary>
        /// 删除销售退货订单
        /// </summary>
        /// <param name="id">订单主表id</param>
        bool DeleteSalesReturn(int id);

        /// <summary>
        /// 销售退货入库
        /// </summary>
        /// <param name="orderId">销售订退货单id</param>
        /// <param name="userId">当前登陆人id</param>
        /// <returns></returns>
        bool OutStore(int orderId, int userId);

        /// <summary>
        /// 获取销售退货订单列表
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<SalesReturnOrderDto> GetSalesReturnList(string where = "");

        /// <summary>
        /// 获取销售退货订单明细列表
        /// </summary>
        /// <param name="orderId">退货订单id</param>
        /// <returns></returns>
        List<SalesReturnInfoDto> GetSalesReturnInfoList(int orderId);
    }
}