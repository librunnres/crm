/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：实物明细账                                                    
*│　作    者：李宝                                              
*│　版    本：1.0 模板代码自动生成                                              
*│　创建时间：2019-01-05 16:55:24                           
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository                                   
*│　接口名称： IStockProductDetailsRepository                                      
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;

namespace Usido.CRM.IRepository
{
    public interface IStockProductDetailsRepository : IBaseRepository<StockProductDetails, int>
    {
        IEnumerable<StockProductDetailsDto> GetAllList(string where);
    }
}