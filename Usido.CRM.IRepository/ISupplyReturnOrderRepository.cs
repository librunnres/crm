using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：采购退货主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISupplyReturnOrderRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Dto;
using Usido.CRM.Models;

namespace Usido.CRM.IRepository
{
    public interface ISupplyReturnOrderRepository : IBaseRepository<SupplyReturnOrder, int>
    {
        /// <summary>
        /// 添加采购退货订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        int AddSupplyReturn(SupplyReturnOrder order, List<SupplyReturnInfo> orderInfoList);

        /// <summary>
        /// 修改采购退货订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        bool UpdateSupplyReturn(SupplyReturnOrder order, List<SupplyReturnInfo> orderInfoList);

        /// <summary>
        /// 删除采购退货订单
        /// </summary>
        /// <param name="id">订单主表id</param>
        bool DeleteSupplyReturn(int id);

        /// <summary>
        /// 采购退货入库
        /// </summary>
        /// <param name="orderId">采购订退货单id</param>
        /// <param name="userId">当前登陆人id</param>
        /// <returns></returns>
        bool OutStore(int orderId, int userId);

        /// <summary>
        /// 获取采购退货订单列表
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<SupplyReturnOrderDto> GetSupplyReturnList(string where = "");

        /// <summary>
        /// 获取采购退货订单明细列表
        /// </summary>
        /// <param name="orderId">退货订单id</param>
        /// <returns></returns>
        List<SupplyReturnInfoDto> GetSupplyReturnInfoList(int orderId);
    }
}