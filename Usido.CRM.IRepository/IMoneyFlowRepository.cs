﻿using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using Usido.CRM.Dto;
using System.Collections.Generic;
namespace Usido.CRM.IRepository
{
    public interface IMoneyFlowRepository : IBaseRepository<MoneyFlow, int>
    {
        double GetMoneyFlowList(string where);
    }
}
