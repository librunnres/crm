/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：数据字典信息                                                    
*│　作    者：李宝                                              
*│　版    本：1.0 模板代码自动生成                                              
*│　创建时间：2019-01-05 10:49:57                           
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository                                   
*│　接口名称： ISysDictRepository                                      
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;

namespace Usido.CRM.IRepository
{
    public interface ISysDictRepository : IBaseRepository<SysDict, int>
    {
    }
}