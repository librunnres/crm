using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：商品表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 10:49:57
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISysProductRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Dto;
using Usido.CRM.Models;

namespace Usido.CRM.IRepository
{
    public interface ISysProductRepository : IBaseRepository<SysProduct, int>
    {
        IEnumerable<SysProductDto> GetProductList(string where);

        /// <summary>
        /// 修改商品的零售价和折扣率
        /// </summary>
        /// <param name="productId">商品id</param>
        /// <param name="price">零售价</param>
        /// <param name="discount">折扣率</param>
        /// <returns></returns>
        bool UpdatePrice(int productId, decimal price, decimal discount);
    }
}