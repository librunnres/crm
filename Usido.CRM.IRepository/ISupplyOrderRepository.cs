using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：采购主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISupplyOrderRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Dto;
using Usido.CRM.Models;

namespace Usido.CRM.IRepository
{
    public interface ISupplyOrderRepository : IBaseRepository<SupplyOrder, int>
    {
        int AddSupply(SupplyOrder order, List<SupplyInfo> orderInfoList);

        bool UpdateSupply(SupplyOrder order, List<SupplyInfo> orderInfoList);

        bool DeleteSupply(int id);

        bool InStore(int orderId, int userId);

        List<SupplyOrderDto> GetSupplyList(string where = "");

        List<SupplyInfoDto> GetSupplyInfoList(int orderId);

        SupplyOrderDto GetModel(int id);

        List<SupplyReportDto> GetSupplyReportList(string where);

        List<SupplyInfoDto> GetSupplyInfoReturnQuantityList(int orderId);
    }
}