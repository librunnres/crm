/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：库存盘点主表                                                    
*│　作    者：李宝                                              
*│　版    本：1.0 模板代码自动生成                                              
*│　创建时间：2019-01-05 10:49:56                           
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository                                   
*│　接口名称： IStockInventoryRepository                                      
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;
using Usido.CRM.Dto;

namespace Usido.CRM.IRepository
{
    public interface IStockInventoryRepository : IBaseRepository<StockInventory, int>
    {
        /// <summary>
        /// 添加库存盘点信息
        /// </summary>
        /// <param name="inventory">主表</param>
        /// <param name="list">明细表</param>
        /// <returns></returns>
        int AddInventory(StockInventory inventory, List<StockInventoryDetail> list);

        /// <summary>
        /// 修改库存盘点信息
        /// </summary>
        /// <param name="inventory">主表</param>
        /// <param name="list">明细表</param>
        /// <returns></returns>
        bool UpdateInventory(StockInventory inventory, List<StockInventoryDetail> list);

        /// <summary>
        /// 删除库存盘点
        /// </summary>
        /// <param name="id">主表id</param>
        /// <returns></returns>
        bool DeleteInventory(int id);

        /// <summary>
        /// 库存盘点出入库
        /// </summary>
        /// <param name="orderId">盘点单Id</param>
        /// <param name="userId">当前登陆人id</param>
        /// <returns></returns>
        bool InStore(int orderId, int userId);

        /// <summary>
        /// 获取库存盘点列表数据
        /// </summary>
        /// <param name="where">where条件</param>
        /// <returns></returns>
        List<StockInventoryDto> GetInventoryList(string where = "");

        /// <summary>
        /// 获取库存盘点明细数据
        /// </summary>
        /// <param name="orderId">盘点单据id</param>
        /// <returns></returns>
        List<StockInventoryDetailDto> GetInventoryInfoList(int orderId);

        /// <summary>
        /// 获取盘点单信息
        /// </summary>
        /// <param name="id">盘点单id</param>
        /// <returns></returns>
        StockInventoryDto GetModel(int id);
    }
}