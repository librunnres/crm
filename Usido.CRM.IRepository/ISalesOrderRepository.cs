using System.Collections.Generic;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：销售主表
*│　作    者：李宝
*│　版    本：1.0 模板代码自动生成
*│　创建时间：2019-01-05 16:55:24
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository
*│　接口名称： ISalesOrderRepository
*└──────────────────────────────────────────────────────────────┘
*/

using Usido.CRM.Core.Repository;
using Usido.CRM.Dto;
using Usido.CRM.Models;

namespace Usido.CRM.IRepository
{
    public interface ISalesOrderRepository : IBaseRepository<SalesOrder, int>
    {
        /// <summary>
        /// 添加销售订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        int AddSales(SalesOrder order, List<SalesInfo> orderInfoList);

        /// <summary>
        /// 修改销售订单
        /// </summary>
        /// <param name="order">主表</param>
        /// <param name="orderInfoList">明细表集合</param>
        /// <returns></returns>
        bool UpdateSales(SalesOrder order, List<SalesInfo> orderInfoList);

        /// <summary>
        /// 删除销售订单
        /// </summary>
        /// <param name="id">订单主表id</param>
        /// <returns></returns>
        bool DeleteSales(int id);

        /// <summary>
        /// 销售出库
        /// </summary>
        /// <param name="orderId">销售订单id</param>
        /// <param name="userId">当前登陆人id</param>
        /// <returns></returns>
        bool OutStore(int orderId, int userId);

        /// <summary>
        /// 获取销售订单列表
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<SalesOrderDto> GetSalesList(string where = "");

        /// <summary>
        /// 获取销售订单明细列表
        /// </summary>
        /// <param name="orderId">订单id</param>
        /// <returns></returns>
        List<SalesInfoDto> GetSalesInfoList(int orderId);

        /// <summary>
        /// 获取销售统计报表数据
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        List<SalesReportDto> GetSalesReportList(string where = "");

        /// <summary>
        /// 获取销售订单明细列表 带可退货数量
        /// </summary>
        /// <param name="orderId">订单id</param>
        /// <returns></returns>
        List<SalesInfoDto> GetSalesInfoReturnQuantityList(int orderId);

        string GetMoney(int id);
    }
}