/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：实物结存表                                                    
*│　作    者：李宝                                              
*│　版    本：1.0 模板代码自动生成                                              
*│　创建时间：2019-01-05 16:55:24                           
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Usido.CRM.IRepository                                   
*│　接口名称： IStockProductBalanceRepository                                      
*└──────────────────────────────────────────────────────────────┘
*/
using Usido.CRM.Core.Repository;
using Usido.CRM.Models;
using System;
using System.Collections.Generic;
using Usido.CRM.Dto;

namespace Usido.CRM.IRepository
{
    public interface IStockProductBalanceRepository : IBaseRepository<StockProductBalance, int>
    {
        /// <summary>
        /// 获取商品的可用库存
        /// </summary>
        /// <param name="productId">商品id</param>
        /// <param name="dateExpiry">过期时间</param>
        /// <param name="batchNumber">商品批号</param>
        /// <returns>可用数量</returns>
        decimal GetProductStock(int productId, DateTime dateExpiry,string batchNumber);

        /// <summary>
        /// 查询库存信息
        /// </summary>
        /// <param name="code">筛选条件</param>
        /// <returns>库存信息</returns>
        IEnumerable<StockInfoDto> GetStockInfoList(string code);

        /// <summary>
        /// 获取实物结存表信息
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        IEnumerable<StockProductBalanceDto> GetStockBalanceList(string where);

        /// <summary>
        /// 设置预留数量
        /// </summary>
        /// <param name="productId">实物结存表id</param>
        /// <param name="reservedStock">预留数量</param>
        /// <returns></returns>
        bool UpdateReservedStock(int id, decimal reservedStock);

        /// <summary>
        /// 查询报表
        /// </summary>
        /// <param name="begin">开始时间</param>
        /// <param name="end">结束时间</param>
        /// <returns></returns>
        List<decimal> GetReport(string begin, string end);
    }
}